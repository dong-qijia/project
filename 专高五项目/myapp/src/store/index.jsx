import {configureStore} from '@reduxjs/toolkit'
import storeList from './modules/store'

const store = configureStore({
    reducer: {
        storeList
    }
})
export default store