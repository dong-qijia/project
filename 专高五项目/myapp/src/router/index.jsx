
import React from "react";
import { createBrowserRouter, Navigate } from 'react-router-dom';
// import Xiang from "../component/LPX/Xiang";

// 路由文件导入---------------------------------------
// 徐晓坤---------------------------------------
let XXK_home = React.lazy(() => import('../component/XXK/XXK_home'))
let XXK_Personal = React.lazy(() => import('../component/XXK/XXK_Personal'))
let XXK_name = React.lazy(() => import('../component/XXK/XXK_name'))
let XXK_field = React.lazy(() => import('../component/XXK/XXK_field'))
let XXK_experience = React.lazy(() => import('../component/XXK/XXK_experience'))
let XXK_medical = React.lazy(() => import('../component/XXK/XXK_medical'))
let XXK_prove = React.lazy(() => import('../component/XXK/XXK_prove'))
let XXK_upprove = React.lazy(() => import('../component/XXK/XXK_upprove'))
let XXK_income = React.lazy(() => import('../component/XXK/XXK_income'))
let XXK_bank = React.lazy(() => import('../component/XXK/XXK_bank'))
let XXK_setup = React.lazy(() => import('../component/XXK/XXK_setup'))
let XXK_help = React.lazy(() => import('../component/XXK/XXK_help'))
let XXK_upphone = React.lazy(() => import('../component/XXK/XXK_upphone.jsx'))
let XXK_uppsd = React.lazy(() => import('../component/XXK/XXK_uppsd.jsx'))
let XXK_helpname = React.lazy(() => import('../component/XXK/XXK_helpname.jsx'))
let XXK_img = React.lazy(() => import('../component/XXK/XXK_img.jsx'))
let XXK_renlian= React.lazy(() => import('../component/XXK/XXK_renlian.jsx'))

// 董起嘉---------------------------------------
let Dqj_Home = React.lazy(() => import('../component/DQJ/Dqj_Home'))
let Dqj_homepage = React.lazy(() => import('../component/DQJ/Dqj_homepage'))
let Dqj_My_consultation = React.lazy(() => import('../component/DQJ/Dqj_My_consultation'))
let Dqj_patient = React.lazy(() => import('../component/DQJ/Dqj_patient'))
let Dqj_My_pharmacy = React.lazy(() => import('../component/DQJ/Dqj_My_pharmacy'))
let Commonly_Used_Medicines = React.lazy(() => import('../component/DQJ/Commonly_Used_Medicines'))
let Commonly_Prescribed_Prescriptions = React.lazy(() => import('../component/DQJ/Commonly_Prescribed_Prescriptions'))
let Historical_Prescriptions = React.lazy(() => import('../component/DQJ/Historical_Prescriptions'))
let Consultation_chat = React.lazy(() => import('../component/DQJ/Consultation_chat'))
let Patient_details = React.lazy(() => import('../component/DQJ/Patient_details'))
let Details_of_the_drug = React.lazy(() => import('../component/DQJ/Details_of_the_drug'))
let Prescription_details = React.lazy(() => import('../component/DQJ/Prescription_details'))
let Historical_prescription_details = React.lazy(() => import('../component/DQJ/Historical_prescription_details'))
let Video_calls = React.lazy(() => import('../component/DQJ/Video_calls'))
let Article_Center = React.lazy(() => import('../component/DQJ/Article_Center'))
let Write_articles = React.lazy(() => import('../component/DQJ/Write_articles'))
let Wodezhuangzuo = React.lazy(() => import('../component/DQJ/Wodezhuangzuo.jsx'))
let Article_details = React.lazy(() => import('../component/DQJ/Article_details'))
let Shipin = React.lazy(() => import('../component/DQJ/shipin.jsx'))
let Wenzhang = React.lazy(() => import('../component/DQJ/wenzhang.jsx'))
let Post_the_video = React.lazy(() => import('../component/DQJ/Post_the_video'))

// 张海涛---------------------------------------
let Mys = React.lazy(() => import('../component/ZHT/Mys'))
let Logins = React.lazy(() => import('../component/ZHT/Logoins'))
let My_order = React.lazy(() => import('../component/ZHT/My_order'))
let Blood_d = React.lazy(() => import('../component/ZHT/Blood_d'))
let Use_drug = React.lazy(() => import('../component/ZHT/Use_drug'))
let Use_rec = React.lazy(() => import('../component/ZHT/Use_rec'))
let Diet = React.lazy(() => import('../component/ZHT/Diet'))
let Integral = React.lazy(() => import('../component/ZHT/Integral'))
let History = React.lazy(() => import('../component/ZHT/History'))
let Add_drug = React.lazy(() => import('../component/ZHT/Add_drug'))
let Perhome = React.lazy(() => import('../component/ZHT/Perhome'))
let My_set = React.lazy(() => import('../component/ZHT/My_set'))
let Personage = React.lazy(() => import('../component/ZHT/Personage.jsx'))
let Zhanghao = React.lazy(() => import('../component/ZHT/Zhanghao.jsx'))
let Aichart = React.lazy(() => import('../component/ZHT/Aichart.jsx'))
let Chatserve = React.lazy(() => import('../component/ZHT/Chatserve.jsx'))
let Chatserve2 = React.lazy(() => import('../component/ZHT/ChatServer2.jsx'))





// 郝丹丹----------------------------------------------
let H_Home = React.lazy(() => import('../component/HDD/Home'))
let H_Community = React.lazy(() => import('../component/HDD/Community'))
let H_Consultation = React.lazy(() => import('../component/HDD/Consultation'))
let H_Add_title = React.lazy(() => import('../component/HDD/Add_title'))
let H_userPage = React.lazy(() => import('../component/HDD/UserPage'))
let Item = React.lazy(()=>import('../component/HDD/Item'))
let MapContainer = React.lazy(()=>import('../component/HDD/MapContainer'))
let DocList = React.lazy(()=>import('../component/HDD/DocList.jsx'))


// 邢馨月---------------------------------------
let Homexxy = React.lazy(() => import('../component/XXY/Home'))
let Detailsxxy = React.lazy(() => import('../component/XXY/Detail'))
let Cartxxy = React.lazy(() => import('../component/XXY/Cart'))
let Payxxy = React.lazy(() => import('../component/XXY/Pay'))
let Orderxxy = React.lazy(() => import('../component/XXY/Orders.jsx'))

//李鹏翔---------------------------------------
let Login = React.lazy(() => import('../component/LPX/Login'))
let Phone = React.lazy(() => import('../component/LPX/Phone'))
let Yans = React.lazy(() => import('../component/LPX/Yans'))
let Resigater = React.lazy(() => import('../component/LPX/Resigater'))
let Main=React.lazy(()=>import('../component/LPX/Main'))
let Expert=React.lazy(()=>import('../component/LPX/Expert'))
let Know=React.lazy(()=>import('../component/LPX/Know.jsx'))
let Health = React.lazy(() => import('../component/LPX/Health.jsx'))
let Complication=React.lazy(()=>import('../component/LPX/Complication'))
let Xiang=React.lazy(()=>import('../component/LPX/Xiang'))
let Xi=React.lazy(()=>import('../component/LPX/Xi'))





// 路由文件配置---------------------------------------
const router = createBrowserRouter([

    //徐晓坤---------------------------------------
    { path: '/XXK_personal', element: <XXK_Personal /> },
    { path: '/XXK_name', element: <XXK_name /> },
    { path: '/XXK_field', element: <XXK_field /> },
    { path: '/XXK_experience', element: <XXK_experience /> },
    { path: '/XXK_medical', element: <XXK_medical /> },
    { path: '/XXK_prove', element: <XXK_prove /> },
    { path: '/XXK_upprove', element: <XXK_upprove /> },
    { path: '/XXK_income', element: <XXK_income /> },
    { path: '/XXK_bank', element: <XXK_bank /> },
    { path: '/XXK_setup', element: <XXK_setup /> },
    { path:'XXK_help',element:<XXK_help/>},
    { path:'XXK_upphone',element:<XXK_upphone/>},
    { path:'XXK_uppsd',element:<XXK_uppsd/>},
    { path:'XXK_helpname',element:<XXK_helpname/>},
    { path:'XXK_img',element:<XXK_img/>},
    { path:'XXK_renlian',element:<XXK_renlian/>},



    // 郝丹丹-------------------------------------
    {
        path: '/User_Home', element: <H_Home />, children: [ //用户端一级路由
            //用户端 首页
            {path:'shou',element:<Main></Main>},
            { path: 'Community', element: <H_Community /> },// 用户端 社区
            { path: 'Consultation', element: <H_Consultation /> },//用户端 问诊
            { path: 'Shop', element: <Homexxy /> }, //用户端  商城
            //用户端  我的
            { path: 'Mys', element: <Mys /> },//用户端我的
        ]
    },
    { path: '/', element: <Navigate to='/login'></Navigate> },//重定向
    { path: '/H_add', element: <H_Add_title /> }, //发布页
    { path: '/H_userPage', element: <H_userPage /> }, //用户主页
    { path: '/item', element: <Item /> }, //帖子详情
    { path: '/MapContainer', element: <MapContainer /> }, //城市
    { path: '/docList', element: <DocList /> }, //医生聊天列表


    // 邢馨月---------------------------------------
    { path: '/Detailsxxy', element: <Detailsxxy /> },//商品详情
    { path: '/Cartxxy', element: <Cartxxy /> },//购物车
    { path: '/Payxxy', element: <Payxxy /> },//支付
    { path: '/Orderxxy', element: <Orderxxy /> },//订单



    //董起嘉--------------------------------------- 
    {
        path: '/Dqj_Home', element: <Dqj_Home />, children: [
            { path: 'Dqj_homepage', element: <Dqj_homepage /> },//医生端首页
            { path: 'Dqj_patient', element: <Dqj_patient /> },//医生端患者
            { path: 'XXK_home', element: <XXK_home /> },//医生端我的
        ]
    },
    { path: '/Dqj_My_consultation', element: <Dqj_My_consultation /> },//医生端我的问诊
    {
        path: "/Dqj_My_pharmacy", element: <Dqj_My_pharmacy />, children: [
            { path: 'Commonly_Used_Medicines', element: <Commonly_Used_Medicines /> },//医生端常用药物
            { path: 'Commonly_Prescribed_Prescriptions', element: <Commonly_Prescribed_Prescriptions /> },//医生端常用处方
            { path: 'Historical_Prescriptions', element: <Historical_Prescriptions /> },//医生端历史处方
        ]
    },//医生端我的药房
    { path: '/', element: <Navigate to='/Dqj_Home/Dqj_homepage'></Navigate> },//重定向
    { path: '/Consultation_chat', element: <Consultation_chat /> },//医生端咨询聊天
    { path: '/Patient_details', element: <Patient_details /> },//医生端患者详情
    { path: '/Details_of_the_drug', element: <Details_of_the_drug /> },//医生端药品详情
    { path: '/Prescription_details', element: <Prescription_details /> },//医生端常用处方详情
    { path: '/Historical_prescription_details', element: <Historical_prescription_details /> },//医生端历史处方详情
    { path: '/Video_calls', element: <Video_calls /> },//医生端视频聊天
    { path: '/Article_Center', element: <Article_Center /> },//医生端文章中心
    { path: '/Write_articles', element: <Write_articles /> },//医生端书写文章
    { path: '/Post_the_video', element: <Post_the_video /> },//医生端上传视频
    { path: '/wodechuangzuo', element: <Wodezhuangzuo />,children:[
        {path:'Shipin',element:<Shipin></Shipin>},
        {path:'Wenzhang',element:<Wenzhang></Wenzhang>}
    ]},//医生端我的文章
    { path: '/Article_details', element: <Article_details /> },//医生端文章详情



    //张海涛---------------------------------------
    
    { path: '/logins', element: <Logins /> },//登录测试
    { path: '/ym_order', element: <My_order /> },//我的订单
    { path: '/blood_d', element: <Blood_d /> },//血糖检测
    { path: '/use_drug', element: <Use_drug /> },//用药提醒
    { path: '/add_drug', element: <Add_drug /> },//添加用药提醒
    { path: '/diet', element: <Diet /> },//饮食监测
    { path: '/integral', element: <Integral /> },//积分兑换
    { path: '/history', element: <History /> },//血糖监测结果列表
    { path: '/perhome', element: <Perhome /> },//我的详情页
    { path: '/my_set', element: <My_set />},//我的设置页
    { path: '/personage', element: <Personage /> },//个人资料页
    { path: '/zhanghao', element: <Zhanghao /> },//账号与安全
    { path: '/aichart', element: <Aichart /> },//账号与安全
    { path: '/chatserve', element: <Chatserve /> },//账号与安全
    { path: '/chatserve2', element: <Chatserve2 /> },//账号与安全






    //李鹏翔
    { path: '/login', element: <Login /> },
    { path: '/phone', element: <Phone /> },
    { path: '/yans', element: <Yans /> },
    { path: '/resigater', element: <Resigater /> },
    {path:"/health",element:<Health></Health>},
    {path:'/expert',element:<Expert></Expert>},
    {path:'/complic',element:<Complication></Complication>},
    {path:'/know',element:<Know></Know>},
    {path:'/xiang',element:<Xiang></Xiang>},
    {path:"/xi",element:<Xi></Xi>}

])
export default router