import { StrictMode, Suspense } from 'react'
import { createRoot } from 'react-dom/client'

import router from './router'
import { RouterProvider } from 'react-router-dom'

import store from './store'
import { Provider } from 'react-redux'

import axios from 'axios'

axios.defaults.baseURL = 'http://localhost:3000'

// import App from './App.jsx'
import './index.css'


createRoot(document.getElementById('root')).render(
  // <StrictMode>
  <Suspense fallback={<div>Loding……</div>}>
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
  </Suspense>
  // </StrictMode>,
)
