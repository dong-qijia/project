import React from 'react'
import { NavBar, Space, Toast } from 'antd-mobile'
import { useNavigate } from 'react-router-dom'
export default function Expert() {
    const navigate = useNavigate()
    const back = () => navigate('/User_Home/shou')
    return (
        <div>
            <NavBar onBack={back}>专家讲座</NavBar>
        </div>
    )
}
