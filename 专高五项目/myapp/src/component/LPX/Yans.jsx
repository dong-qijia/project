import React, { useEffect, useRef, useState } from 'react'
import './Login.css'
import { NavBar, Space, Toast } from 'antd-mobile'
import { useNavigate } from 'react-router-dom'
export default function Yans() {
  const sjh = localStorage.getItem('sjh')
  const [str, setstr] = useState('')
  const [num, setnum] = useState(60)
  const iptRef = useRef([])
  const [flag, setflag] = useState(true)
  let [prv, setprv] = useState('')
  const navigate = useNavigate()
  const timer = () => {
    let time = num
    if (time > 0) {
      let times = setInterval(() => {
        time--
        setnum(time)
        if (time == 0) {
          setnum(60)
          clearInterval(times)
          setflag(false)
        }
      }, 1000);
    }
  }

  const cxhq = () => {
    setflag(true)
    timer()
  }

  useEffect(() => {
    timer()
  }, [])

  const gb = (idx, e) => {
    let value = e.value
    setprv(prv = prv + value)
    if (value != '' && idx < iptRef.current.length - 1) {
      iptRef.current[idx + 1].focus()
    }
    if (prv == localStorage.getItem('code')) {
      Toast.show({
        icon: 'success',
        content: '验证成功',
      })
      navigate('/User_Home/shou')
    }

  }

  const back = () => {
    navigate('/login')
  }
  return (
    <div id='yan'>
      <NavBar onBack={back}>医疗系统</NavBar>
      <div className='yan-top'></div>
      <div className='yan-bottom'>
        <div className='yan'>
          <p style={{ marginTop: '10px', fontSize: '20px', marginBottom: '15px' }}>获取验证码</p>
          <p>验证码已经发送到您的手机</p>
          <p style={{ marginTop: '10px', fontSize: '16px', marginBottom: '10px' }}>+86&nbsp;{sjh}</p>
          <div style={{ display: 'flex', justifyContent: 'space-between', width: '330px', marginLeft: '40px' }}>
            <p>6位数字验证码</p>
            {flag ? <p>{num}秒</p> : <p onClick={() => {
              cxhq()
            }}>重新获取验证码</p>}
          </div>

          <div>
            {[...Array(6)].map((_, index) => {
              return <input key={index}
                maxLength={1}
                ref={(idx) => iptRef.current[index] = idx}
                style={{ width: '40px', height: '40px', borderRadius: '15px', margin: '8px', textAlign: 'center', lineHeight: '40px' }}
                onChange={(e) => {
                  gb(index, e.target)
                }}>
              </input>
            })}
          </div>
          <p style={{ textAlign: 'right', marginRight: '40px', marginTop: '10px' }} onClick={() => {
            navigate('/login')
          }}>使用账号密码登录</p>
        </div>
      </div>
    </div>
  )
}

