import React, { useEffect, useRef, useState } from 'react'
import { Button, SearchBar, Space, Toast } from 'antd-mobile'
import { InfiniteScroll, List, DotLoading } from 'antd-mobile'
import { useNavigate } from 'react-router-dom'
import './main.css'
import { Tabs } from 'antd-mobile'
import axios from 'axios'
axios.defaults.baseURL = 'http://localhost:3000'


export default function Main() {
  const navigate = useNavigate()
  const [list, setlist] = useState([] || JSON.parse(sessionStorage.getItem('list')))
  const getlist = async () => {
    let { data: { data } } = await axios.get('/LPX/getjkz')
    setlist(data)
    sessionStorage.setItem('list', JSON.stringify(data))
    console.log('11111111111');
  }
  const [hot, sethot] = useState([])
  const [title, settitle] = useState('')
  const gethot = async () => {
    let { data: { data } } = await axios.get('/LPX/gethot')
    sethot(data)
    sessionStorage.setItem('hot', JSON.stringify(data))

  }
  const [video, setvideo] = useState([])
  const getvideo = async () => {
    let { data: { data } } = await axios.get('/LPX/getvideo')
    console.log(data);
    setvideo(data)
  }


  useEffect(() => {
    gethot()
  }, [title])
  useEffect(() => {
    getlist()

    getvideo()
  }, [])


  const InfiniteScrollContent = ({ hasMore }) => {
    return (
      <>
        {hasMore ? (
          <>
            <span>Loading</span>
            <DotLoading />
          </>
        ) : (
          <span>--- 我是有底线的 ---</span>
        )}
      </>
    )
  }

  const [hasMore, setHasMore] = useState(true)
  const [num, setnum] = useState(6)
  async function loadMore() {
    setTimeout(() => {
      setnum(num + 3)
      setHasMore(num < list.length)
    }, 2000);
  }

  return (
    <div>
      <div className='top'>
        <div className='sea' style={{ width: '80%', margin: '0 auto' }}>
          <SearchBar
            placeholder='请输入内容'
            onSearch={(value) => {
              sethot(hot.filter(item => item.title.includes(value)))
            }}
            showCancelButton
            style={{
              '--border-radius': '100px',
              '--background': '#ffffff',
              '--height': '32px',
              '--padding-left': '12px',
            }}
          />
        </div>
      </div>
      <div className='nav'>
        <Tabs>
          <Tabs.Tab title='热门科普' key='hot'>
            <div>
              {hot.slice(0, num).map(item => {
                return <div key={item._id} onClick={() => {
                  sessionStorage.setItem('id', item._id)
                  navigate('/xiang')
                }} style={{
                  margin: '10px',
                  display: 'flex',
                  justifyContent: "space-around",
                  alignItems: 'center',
                  padding: '10px',
                  fontSize: '20px',
                  boxShadow: '0 0 5px #ccc'
                }}>
                  <div>
                    <p>{item.title}</p>
                  </div>
                  <div>
                    <img src={item.img} width={70} height={70}></img>
                  </div>
                </div>
              })}
              <InfiniteScroll loadMore={loadMore} hasMore={hasMore}>
                <InfiniteScrollContent hasMore={hasMore} />
              </InfiniteScroll>
            </div>
          </Tabs.Tab>
          <Tabs.Tab title='科普文章' key='essay'>
            {list.slice(0, num).map(item => {
              return <div style={{
                margin: '10px',
                display: 'flex',
                padding: '10px',
                fontSize: '18px',
                boxShadow: '0 0 5px #ccc'
              }} onClick={()=>{
                sessionStorage.setItem('zid', item._id)
                  navigate('/xi')
              }}>
                <div>
                  <p>{item.title}</p>
                </div>
                <div>
                  <img src={item.img} width={90} height={90}></img>
                </div>
              </div>
            })}
            <InfiniteScroll loadMore={loadMore} hasMore={hasMore}>
              <InfiniteScrollContent hasMore={hasMore} />
            </InfiniteScroll>
          </Tabs.Tab>
          <Tabs.Tab title='科普视频' key='video'>
            <div>
              {video.map(item => {
                return <div>
                  <video src={item.src} controls width={400} height={200}></video>
                  <h2>{item.title}</h2>
                </div>
              })}
            </div>
          </Tabs.Tab>
        </Tabs>
      </div>

    </div>
  )
}
