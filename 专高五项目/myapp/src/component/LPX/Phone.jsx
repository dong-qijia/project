import React, { useState } from 'react'
import './Login.css'
import { useNavigate } from 'react-router-dom'
import { NavBar, Space, Toast } from 'antd-mobile'
import axios from 'axios'
axios.defaults.baseURL = 'http://localhost:3000/LPX'
export default function Phone() {
  const [flag, setflag] = useState(false)
  const [sjh, setsjh] = useState('')
  const navigate = useNavigate()
  const gotoyan = async () => {

    if (sjh.length == 11) {
      if (flag == false) {
        Toast.show({
          icon: 'fail',
          content: '请勾选协议',
        })
      } else {
        localStorage.setItem('sjh', sjh)
        let res = await axios.get('/send_code', {
          params: {
            phone: sjh
          }
        })
        if (res.data.code == 200) {
          navigate('/yans')
          localStorage.setItem('code', res.data.sms)
        }
      }
    } else {
      Toast.show({
        icon: 'fail',
        content: '手机号格式不正确',
      })
    }
  }
  const back = () => {
    navigate('/login')
  }
  return (
    <div id='sjhdl'>
      <NavBar onBack={back}>手机号验证登录</NavBar>
      <div className='sjhdl-top'></div>
      <div className='sjhdl-bottom'>
        <div className='sjhdl'>
          <span>为了安全进行联系，请输入您的常用手机号码</span>
          <div className='sjh' style={{ marginTop: '50px' }}>
            <div className='sjhl'>
              <p>+86 &nbsp;|</p>
            </div>
            <input value={sjh} onChange={(e) => {
              setsjh(e.target.value)
            }} placeholder='请输入手机号' maxLength={11}></input>
          </div>
          <p style={{ fontSize: '12px', display: 'flex', alignItems: 'center', marginTop: '20px', marginLeft: '20px' }}>
            <span><input type='checkbox' checked={flag} onChange={() => {
              setflag(!flag)
            }} ></input>&nbsp;</span>同意服务协议和《儿童个人信息处理规则》和《个人信息处理规则》</p>
          <button onClick={() => {
            gotoyan()
            localStorage.setItem('sjh', sjh)
          }} className='xia'>下一步</button>
        </div>
      </div>


    </div>
  )
}
