import React from 'react'
import { NavBar, Space, Toast } from 'antd-mobile'
import { useNavigate } from 'react-router-dom'

export default function Know() {
    const navigate = useNavigate()
    const back = () => navigate('/User_Home/shou')
    return (
        <div>
            <NavBar onBack={back}>知识竞答</NavBar>
        </div>
    )
}
