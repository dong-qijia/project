import SliderCaptcha from 'rc-slider-captcha'; // 导入滑动验证码组件
import React, { useRef } from 'react'; // 导入React和useRef钩子
import { sleep } from 'ut2'; // 导入sleep函数
import createPuzzle from 'create-puzzle'; // 导入createPuzzle函数
import { RubyOutlined, MehOutlined, SmileOutlined, RedoOutlined, LoadingOutlined } from '@ant-design/icons' // 导入Ant Design图标
// 这里是你要自己准备的图片
import pic from '../../assets/login.jpg' // 导入登录页面的背景图片
import { useNavigate } from 'react-router-dom'; // 导入useNavigate钩子

const SoildCaptcha = (params) => {
    const navigate = useNavigate()
    const offsetXRef = useRef(0); // x 轴偏移值
    // 查看是否在安全距离
    const verifyCaptcha = async (data) => {
        await sleep();
        if (data.x >= offsetXRef.current - 5 && data.x < offsetXRef.current + 5) {
            navigate('/login')
            return Promise.resolve();
        }
        alert('验证失败')
        return Promise.reject();

    };
    return (
        <div className='container'>
            <SliderCaptcha
                request={() =>
                    createPuzzle(pic, {
                        format: 'blob'
                    }).then((res) => {
                        offsetXRef.current = res.x
                        return {
                            // 背景图片
                            bgUrl: res.bgUrl,
                            // 核验区域
                            puzzleUrl: res.puzzleUrl
                        };
                    })
                }
                onVerify={(data) => {
                    return verifyCaptcha(data);
                }}
                // bgSize必须和原图片的尺寸一样！！！！！！！！！！！！！！！！！！
                bgSize={{ width: 450, height: 300 }}
                tipIcon={{
                    default: <RubyOutlined />,
                    loading: <LoadingOutlined />,
                    success: <SmileOutlined />,
                    error: <MehOutlined />,
                    refresh: <RedoOutlined />
                }}
                tipText={{
                    default: '向右👉拖动完成拼图',
                    loading: '👩🏻‍💻🧑‍💻努力中...',
                    moving: '向右拖动至拼图位置',
                    verifying: '验证中...',
                    error: '验证失败'
                }}

            />
        </div>
    );
}

// 导出默认的SoildCaptcha
export default SoildCaptcha;