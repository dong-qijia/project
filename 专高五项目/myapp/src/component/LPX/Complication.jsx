import React, { useEffect, useState } from 'react'
import { NavBar, Space, Toast } from 'antd-mobile'
import { useNavigate } from 'react-router-dom'
import './main.css'
import axios from 'axios'
axios.defaults.baseURL = 'http://localhost:3000/LPX'
export default function Complication() {
    const navigate = useNavigate()
    const [idx, setidx] = useState(0)
    const back = () => {
        window.history.go(-1)
    }
    const [list, setlist] = useState([])
    const getsj = async () => {
        let { data: { data } } = await axios.get('/getbfz')
        setlist(data)
        console.log('3333333');
        
    }

    useEffect(() => {
        getsj()
    }, [])
    return (
        <div>
            <NavBar onBack={back}>并发症科普</NavBar>
            <div style={{ margin: "10px 0", display: 'flex' }}>
                <p style={{ width: "90px", height: "30px", textAlign: 'center', lineHeight: "30px", border: "2px solid gray", borderRadius: "50px", marginLeft: "20px" }} className={idx == 0 ? "active" : ""} onClick={() => {
                    setidx(0)
                }}>慢性并发症</p>
                <p style={{ marginLeft: "20px", width: "90px", height: "30px", textAlign: 'center', lineHeight: "30px", border: "2px solid gray", borderRadius: "50px" }} className={idx == 1 ? "active" : ""} onClick={() => {
                    setidx(1)
                }}>急性并发症</p>
            </div>
            {idx == 0 ? <div>
                {list.filter(item => item.type == idx).map(item => {
                    return <div>
                        <p>{item.title}</p>
                        <div>
                            <img src={item.img} width={90} height={90}></img>
                        </div>
                    </div>
                })}
            </div> : <div>
                {list.filter(item => item.type == idx).map(item => {
                    return <div>
                        <p>{item.title}</p>
                        <div>
                            <img src={item.img} width={90} height={90}></img>
                        </div>
                    </div>
                })}
            </div>}
        </div>
    )
}
