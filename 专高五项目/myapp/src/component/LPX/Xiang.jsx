import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'
import { NavBar, Space, Toast } from 'antd-mobile'
axios.defaults.baseURL = 'http://localhost:3000'

export default function Xiang() {
    const navigate = useNavigate()
    const [sj, setsj] = useState({})
    const getsj = async () => {
        let { data: { data } } = await axios.get('/LPX/gethot')
        let a = data.filter(item => item._id == sessionStorage.getItem('id'))
        setsj(a[0])
        console.log(sj, '1111111111111111');
    }
    useEffect(() => {
        getsj()
    }, [])
    const back = () => {
        navigate('/User_Home/shou')
    }
    return (
        <div>
            <NavBar onBack={back}>详情介绍</NavBar>
            <p style={{
                width: '90%',
                fontSize: '170%',
                margin: '0 auto',
                fongtweight: 'bold',
                marginTop: '20px',
                paddingBottom: '10px',
                borderBottom: '2px solid #ccc',
            }}>{sj.title}</p>
            <p style={{
                fontSize: '20px',
                width: '90%',
                margin: '10px auto',
                marginBottom: '20px',
                borderBottom: '2px solid #ccc',
                padding:'10px'
            }}>{sj.content}</p>
            <p style={{
                textAlign: 'center',
                margin: '0 auto',
            }}>
                <img src={sj.img} width='75%' height='30%'
                ></img>
            </p>
        </div>
    )
}
