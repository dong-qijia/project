import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { NavBar, Toast } from 'antd-mobile'
import { Button, Mask, Space } from 'antd-mobile';
import { useNavigate } from 'react-router-dom'
import './Login.css'
import Hk from './Hk'
import { CloseOutline, MoreOutline, SearchOutline } from 'antd-mobile-icons'
axios.defaults.baseURL = 'http://localhost:3000/LPX'


export default function Resigater() {
    let [username, setusername] = useState('')
    let [password, setpassword] = useState('')
    let [password1, setpassword1] = useState('')
    const navigate = useNavigate()
    const [flag, setflag] = useState(false)
    const [num, setnum] = useState(60)
    const back = () => {
        navigate('/login')
    }
    const zc = async () => {
        if (username == '') {
            Toast.show({
                icon: 'fail',
                content: '用户名不能为空',
            })
            return
        }
        if (password == '') {
            Toast.show({
                icon: 'fail',
                content: '密码不能为空',
            })
            return
        }
        if (password != password1) {
            Toast.show({
                icon: 'fail',
                content: '两次密码不一致',
            })
            return
        }
        let { data } = await axios.post('/adduser', { username,password })
        if (data.code == 200) {
            Toast.show({
                icon: 'success',
                content: '注册成功',
            })
            setVisible(true)
        } else {
            Toast.show({
                icon: 'fail',
                content: '注册失败',
            })
        }
    }

    const yzm = () => {
        let number = num
        let timer = setInterval(() => {
            if (number > 0) {
                number--
                setnum(number)
            } else {
                clearInterval(timer)
                setnum(60)
                setflag(true)
            }
        }, 1000)
    }

    useEffect(() => {
        yzm()
    }, [])

    const cxhq = () => {
        setflag(false)
        yzm()
    }

    const [visible, setVisible] = useState(false)


    return (
        <div>
            <NavBar onBack={back}>注册页面</NavBar>
            <div className='zc-top'>

            </div>
            <div className='zc-bottom'>
                <div className='zc'>
                    <p><input placeholder='请输入用户名' style={{
                        width: '300px',
                        height: '40px',
                        borderRadius: '10px',
                        border: 'none',
                        outline: 'none',
                        background: 'none',
                        borderRadius: '10px',
                        background: 'rgb(248, 245, 245)',
                        display: 'inline-block',
                        paddingLeft: '20px'
                    }} value={username} onChange={(e) => {
                        setusername(e.target.value)
                    }}></input></p>
                    <p><input placeholder='请输入密码' type='password' style={{
                        width: '300px',
                        height: '40px',
                        borderRadius: '10px',
                        border: 'none',
                        outline: 'none',
                        background: 'none',
                        borderRadius: '10px',
                        background: 'rgb(248, 245, 245)',
                        display: 'inline-block',
                        paddingLeft: '20px'
                    }} value={password} onChange={(e) => {
                        setpassword(e.target.value)
                    }}></input></p>
                    <p><input type='password' placeholder='请再次输入密码' style={{
                        width: '300px',
                        height: '40px',
                        borderRadius: '10px',
                        border: 'none',
                        outline: 'none',
                        background: 'none',
                        borderRadius: '10px',
                        background: 'rgb(248, 245, 245)',
                        display: 'inline-block',
                        paddingLeft: '20px'
                    }} value={password1} onChange={(e) => {
                        setpassword1(e.target.value)
                    }}></input></p>

                    <button onClick={() => {
                        zc()

                    }} style={{ width: '260px', height: '40px', textAlign: 'center', lineHeight: '40px', background: 'linear-gradient(to right, #FF009D, #6d0098)', border: 'none', borderRadius: '20px', color: 'white' }}>注册</button>
                </div>
            </div>
            <div>
                <Mask visible={visible} onMaskClick={() => setVisible(false)}>
                    <Hk></Hk>
                </Mask>
                {/* <Button onClick={() => setVisible(true)}>显示带内容的背景蒙层</Button> */}
            </div>

        </div>
    )
}
