import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import axios from 'axios'
import './Login.css'
import { Button, Space, Toast } from 'antd-mobile'
// 导入md5模块
import md5 from 'js-md5';
axios.defaults.baseURL = 'http://localhost:3000/LPX'
// 将md5模块添加到React.Component的原型链上，使其可以在所有组件中使用
React.Component.prototype.$md5 = md5;
export default function Login() {
    const navigate = useNavigate()
    const [username, setusername] = useState('')
    const [password, setpassword] = useState('')
    const dl = async () => {
        if (username == '') {
            Toast.show({
                icon: 'fail',
                content: '用户名不能为空',
            })
            return
        }
        if (password == '') {
            Toast.show({
                icon: 'fail',
                content: '密码不能为空',
            })
            return
        }

        let { data } = await axios.post('/login', { username, password })
        if (data.code == 200) {
            if (data.data.identity == false) {
                navigate('/User_Home/shou')
                console.log(md5(password),'加密后的密码');
                
            } else {
                navigate('/Dqj_home/Dqj_homepage')
            }
            Toast.show({
                icon: 'success',
                content: '登陆成功',
            })
            if(data.data.identity==false){
                navigate('/User_Home/shou')
            }else{
                navigate('/Dqj_home/Dqj_homepage')
            }
            setpassword('')
            setusername('')
            sessionStorage.setItem('user', JSON.stringify(data.data))
            sessionStorage.setItem('token', data.token)

        } else {
            Toast.show({
                icon: 'fail',
                content: '登陆失败',
            })
        }
    }
    return (
        <div id='login'>
            <div id='login-top'>
            </div>
            <div id='login-bottom'>
                <div className='main'>
                    <input type='text' placeholder='请输入用户名' style={{ display: 'inline-block', marginTop: '40px' }} value={username} onChange={(e) => {
                        setusername(e.target.value)
                    }}></input>
                    <input type='password' placeholder='请输入您的密码' value={password} onChange={(e) => {
                        setpassword(e.target.value)
                    }}></input>
                    <div style={{ marginTop: '30px' }}><button onClick={() => {
                        dl()
                    }}>登 录</button></div>
                    <div style={{ display: 'flex', justifyContent: 'space-between', width: '300px', marginTop: '20px' }}>
                        <span className='csd' onClick={() => {
                            navigate('/resigater')
                        }}>注册账号</span>
                        <span className='csd' onClick={() => {
                            navigate('/phone')
                        }}>验证码登录</span>
                    </div>
                </div>
            </div>
        </div >
    )
}
