import React, { useState, useEffect } from 'react'
import { useNavigate, Outlet } from 'react-router-dom'
import { NavBar, Space, Toast, Tabs } from 'antd-mobile'
import axios from 'axios'
export default function Wodezhuangzuo() {
    
    let navigate = useNavigate()
    const back = () =>
        Toast.show({
            content: navigate('/Article_Center'),
            duration: false,
        })
    return (
        <div>
            <NavBar onBack={back}>我的文章</NavBar>
            <Tabs onChange={(e) => {
                if (e == '文章') {
                    navigate('/wodechuangzuo/Wenzhang')
                } else {
                    navigate('/wodechuangzuo/Shipin')
                }
            }}>
                <Tabs.Tab title='文章' key='文章' />
                <Tabs.Tab title='视频' key='视频' />
            </Tabs>
            <Outlet></Outlet>
        </div>
    )
}
