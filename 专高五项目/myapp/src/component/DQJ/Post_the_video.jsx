import React, { useState, } from 'react'
import { NavBar, Space, Toast } from 'antd-mobile'
import { useNavigate } from 'react-router-dom'
import './style/post_the_video.css'
import axios from 'axios'
import { Image, Upload } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
export default function Write_articles() {

    let [title, settitle] = useState('') // 标题
    let [content, setcontent] = useState('') // 内容

    const [previewOpen, setPreviewOpen] = useState(false);
    const [previewImage, setPreviewImage] = useState('');
    const [fileList, setFileList] = useState([]);

    // 标题内容
    let navigate = useNavigate()
    const right = (
        <div style={{ fontSize: 15, color: 'blue' }}>
            <Space style={{ '--gap': '16px' }} onClick={() => {
                post_content()
            }}>
                发布
            </Space>
        </div>
    )
    const back = () =>
        Toast.show({
            content: navigate(-1),
            duration: false,
        })



    // 添加视频文章
    const post_content = async () => {
        let _id = JSON.parse(sessionStorage.getItem('user'))._id
        let from = {
            title: title,
            content: content,
            doctor_id: _id,
            video:fileList == [] ? '' : fileList[0].response.data,
        }
        let { data: { data, code } } = await axios.post('/DQJ/post_content', from)
        if (code == 200) {
            Toast.show({
                icon: 'success',
                content: '添加成功',
            })
            setcontent('')
            settitle('')
            setFileList([])
        }
    }

    const handlePreview = async (file) => {
        if (!file.url && !file.preview) {
            file.preview = await getBase64(file.originFileObj);
        }
        setPreviewImage(file.url || file.preview);
        setPreviewOpen(true);
    };
    const handleChange = ({ fileList: newFileList }) => {
        setFileList(newFileList)
    };


    const uploadButton = (
        <button style={{
            border: 0,
            background: 'none',
        }}
            type="button"
        >
            <PlusOutlined />
            <div
                style={{
                    marginTop: 8,
                }}
            >
                Upload
            </div>
        </button>
    );





    return (
        <div>

            <NavBar right={right} onBack={back}>
                发布视频
            </NavBar>



            <Upload
                action="http://localhost:3000/DQJ/post_the_video"
                listType="picture-card"
                fileList={fileList}
                onPreview={handlePreview}
                onChange={handleChange}
            >
                {fileList.length >= 8 ? null : uploadButton}
            </Upload>
            {previewImage && (
                <Image
                    wrapperStyle={{
                        display: 'none',
                    }}
                    preview={{
                        visible: previewOpen,
                        onVisibleChange: (visible) => setPreviewOpen(visible),
                        afterOpenChange: (visible) => !visible && setPreviewImage(''),
                    }}
                    src={previewImage}
                />
            )}






            <input type="text" placeholder='请输入标题' value={title} onChange={(e) => {
                settitle(e.target.value)
            }} />
            <br />
            <textarea placeholder='请输入内容' value={content} onChange={(e) => {
                setcontent(e.target.value)
            }
            }></textarea>
        </div>
    )
}

