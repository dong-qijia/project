import React from 'react'
import { Outlet, useNavigate } from 'react-router-dom'
import { TabBar } from 'antd-mobile'
import { UserAddOutline, AppOutline, UserCircleOutline } from 'antd-mobile-icons'
export default function Dqj_Home() {
  let navigate = useNavigate()
  
  const tabs = [
    {
      key: 'home',
      title: '首页',
      icon: <AppOutline onClick={()=>{
        navigate('/Dqj_home/Dqj_homepage')
      }}/>,
    },
    {
      key: 'todo',
      title: '患者',
      icon: <UserAddOutline onClick={()=>{
        navigate('/Dqj_home/Dqj_patient')
      }}/>,
      badge: '5',
    },
    {
      key: 'message',
      title: '我的',
      icon: <UserCircleOutline onClick={()=>{
        navigate('/Dqj_home/XXK_home')
      }}/>,
    },
  ]
  return (
    <div>
      <Outlet />
      <TabBar style={{width:'100%',position:'fixed',bottom:'0', backgroundColor:'white'}}>
        {tabs.map(item => (
          <TabBar.Item key={item.key} icon={item.icon} title={item.title} />
        ))}
      </TabBar>
    </div>
  )
}
