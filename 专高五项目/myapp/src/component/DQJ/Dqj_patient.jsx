import React from 'react'
import { Button, SearchBar, Space, Toast } from 'antd-mobile'
import './style/Dqj_patient.css'
export default function Dqj_patient() {
  return (
    <div style={{ backgroundColor: 'rgb(242,242,242)' }}>
      {/* 头部 */}
      <div className='title'>
        <div>
          <h2>患者</h2>
          <span>群友消息</span>
        </div>
        <hr />
      </div>


      {/* 搜索框 */}
      <div className='Patient_Search'>
        <div style={{width:'90%'}}>
          <SearchBar placeholder='请输入内容' />
        </div>
      </div>

      {/* 患者列表 */}
      <br />
      <div className='Dqj_item_usedata'>
        <div className='Dqj_Message_List_item'>
          <img src="https://ts4.cn.mm.bing.net/th?id=OIP-C.PobTVuzuQy7tAZtvVEc3VQAAAA&w=250&h=250&c=8&rs=1&qlt=90&o=6&dpr=1.5&pid=3.1&rm=2" alt="" />
          <div>
            <span style={{ marginTop: '4%', marginRight: '4%' }}>吴姗姗</span>
            <span style={{ marginTop: '4%', marginRight: '4%' }}>女</span>
            <span style={{ marginTop: '4%' }}>23岁</span>
          </div>
        </div>
        <span style={{ marginTop: '3%' }}>带接诊</span>
      </div>
    </div >
  )
}
