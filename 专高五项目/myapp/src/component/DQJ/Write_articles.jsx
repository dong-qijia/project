import React, { useState, } from 'react'
import { NavBar, Space, Toast, Button } from 'antd-mobile'
import { CloseOutline, MoreOutline, SearchOutline, UploadOutline } from 'antd-mobile-icons'
import { useNavigate } from 'react-router-dom'
import './style/Article_Center.css'
import axios from 'axios'
import { Upload } from 'antd';
import ImgCrop from 'antd-img-crop';
export default function Write_articles() {
    const [fileList, setFileList] = useState([
        // {
        //   uid: '-1',
        //   name: 'image.png',
        //   status: 'done',
        //   url: '',
        // },
    ]);
    const onChange = ({ fileList: newFileList }) => {
        setFileList(newFileList);
    };
    const onPreview = async (file) => {
        console.log(file);
        let src = file.url;
        console.log(file);
        if (!src) {
            antd - img - crop
            src = await new Promise((resolve) => {
                const reader = new FileReader();
                reader.readAsDataURL(file.originFileObj);
                reader.onload = () => resolve(reader.result);
            });
        }
        const image = new Image();
        image.src = src;
        const imgWindow = window.open(src);
        imgWindow?.document.write(image.outerHTML);
    };

    let navigate = useNavigate()
    const right = (
        <div style={{ fontSize: 15, color: 'blue' }}>
            <Space style={{ '--gap': '16px' }} onClick={() => {
                post_content()
            }}>
                发布
            </Space>
        </div>
    )
    const back = () =>
        Toast.show({
            content: navigate(-1),
            duration: false,
        })

    let [title, settitle] = useState('')
    let [content, setcontent] = useState('')


    const post_content = async () => {
        let _id = JSON.parse(sessionStorage.getItem('user'))._id
        let from = {
            title: title,
            content: content,
            doctor_id: _id,
            img: fileList == [] ? '' : fileList[0].response.data,
        }
        let { data: { data, code } } = await axios.post('/DQJ/post_content', from)
        if (code == 200) {
            Toast.show({
                icon: 'success',
                content: '添加成功',
            })
            setcontent('')
            settitle('')
            setFileList([])
        }
    }
    return (
        <div>
            <NavBar right={right} onBack={back}>
                发布文章
            </NavBar>
            
            <input type="text" placeholder='请输入标题' value={title} onChange={(e) => {
                settitle(e.target.value)
            }} />
            <br />
            <textarea placeholder='请输入内容' value={content} onChange={(e) => {
                setcontent(e.target.value)
            }
            }></textarea>
            <ImgCrop rotationSlider >
                <Upload
                    action="http://localhost:3000/DQJ/post_the_video"
                    listType="picture-card"
                    fileList={fileList}
                    onChange={onChange}
                    onPreview={onPreview}
                    maxCount={1}
                >
                    {fileList.length < 5 && '+ Upload'}
                </Upload>
            </ImgCrop>
        </div>
    )
}
