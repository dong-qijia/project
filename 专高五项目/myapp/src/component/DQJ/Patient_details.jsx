import React from 'react'
import {useNavigate} from 'react-router-dom'
import { NavBar, Space, Toast } from 'antd-mobile'
import { CloseOutline, MoreOutline, SearchOutline } from 'antd-mobile-icons'
export default function Patient_details() {
  let navigate = useNavigate()
  const right = (
    <div style={{ fontSize: 24 }}>
      <Space style={{ '--gap': '16px' }}>
        <SearchOutline />
        <MoreOutline />
      </Space>
    </div>
  )

  const back = () =>
    Toast.show({
      content: navigate(-1),
      duration: false,
    })
  return (
    <div>
      <NavBar onBack={back}>患者详情</NavBar>
    </div>
  )
}
