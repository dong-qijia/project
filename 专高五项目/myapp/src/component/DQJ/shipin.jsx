import React, { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import './style/Article_Center.css'
import axios from 'axios'
export default function wenzhang() {
    let [data, setdata] = useState([])
    let navigate = useNavigate()
    useEffect(() => {
        getcontent()
    }, [])
    const getcontent = async () => {
        let _id = JSON.parse(sessionStorage.getItem('user'))._id
        const { data: { data } } = await axios.get(`/DQJ/getvideo?_id=${_id}`)
        setdata(data)
    }
    return (
        <div>
            <div className='aritle'>
                <h2>视频精选</h2>
                <div className='aritle_item'>
                    {data.map((item) => {
                        return <div key={item._id} className='small_item' onClick={() => {
                            navigate('/Article_details', { state: { item: item } })
                        }}>
                            <h2>{item.title}</h2>
                            <p>{item.content}</p>
                        </div>
                    })}
                </div>

            </div></div>
    )
}
