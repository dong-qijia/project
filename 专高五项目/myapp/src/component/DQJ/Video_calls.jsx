// 视频通话，祖传代码勿动！！！！！
import React, { useState, useMemo, useEffect } from "react";
import {useLocation} from "react-router-dom"
import { TUICallKit, TUICallKitServer, TUIGlobal } from "@tencentcloud/call-uikit-react";
import * as GenerateTestUserSig from "../../../node_modules/@tencentcloud/call-uikit-react/debug/GenerateTestUserSig-es";
export default function Video_calls() {

    let location = useLocation()
    // 视频通话使用到的状态
    const [userData, setUserData] = useState({
        userID: '', //自己的电话号码
        callUserID:'', //对方的电话号码
        SDKAppID: '1600048381',        // Replace with your SDKAppID  腾讯api 实时通信中的的SDKAppID 
        SecretKey: "0247897496dce14f238a1a739dc664484275b1edf189cffdc5a0a1f9f14b6836",      // Replace with your SecretKey 腾讯api 实时通信中的的SDKAppkey
        isLogin: true,   //控制是否登录
    });


    // // 设置当前使用人员联系方式,用于发起视频通话,可以注释,注释之后需要手动设置自己的联系方式之后,才能进行视频通话
    useEffect(() => {
        fuzhi()
       
    }, [])
    const fuzhi = () => {
        if(JSON.parse(sessionStorage.getItem('user')).identity==false){
            setUserData((pr) => {
                return {
                    ...pr,
                    userID: location.state.from.bingphone,
                    callUserID:location.state.from.yiphone,
                    isLogin: true
                }
            })
        }else{
            setUserData((pr) => {
                return {
                    ...pr,
                    userID: location.state.from.yiphone,
                    callUserID:location.state.from.bingphone,
                    isLogin: true
                }
            })
        }
    }



    // 设置自己的联系方式,用于发起视频通话,如果上面的useEffect没有注释,则这一段代码用不到,
    const init = async () => {
        const { SDKAppID, SecretKey, userID } = userData;  //获取SDKAppID,SecretKey,userID
        if (SDKAppID && SecretKey && userID) {             //验证是否已经设置好SDKAppID,SecretKey,userID
            try {
                await login(SDKAppID, SecretKey, userID);  //触发login方法
                setUserData((prevState) => ({
                    ...prevState,
                    isLogin: true,
                }));
                alert("[TUICallKit] 设置本人联系方式成功.可以进行视频聊天了");
            } catch (error) {
                console.error(error);
            }
        } else {
            alert("[TUICallKit] 请填写 SDKAppID, userID 和 SecretKey.");
        }
    };


    const login = async (SDKAppID, SecretKey, userId) => {
        const { userSig } = GenerateTestUserSig.genTestUserSig({
            userID: userId,
            SDKAppID: Number(SDKAppID),
            SecretKey: SecretKey,
        });
        await TUICallKitServer.init({ //【1】初始化TUICallKit组件 
            userID: userId,
            userSig,
            SDKAppID: Number(SDKAppID),
        });
    };



    
    const call = async () => {
        const { SDKAppID, SecretKey, userID } = userData;
        await login(SDKAppID, SecretKey, userID);
        await TUICallKitServer.call({ userID: userData.callUserID, type: 2 }); //【2】进行1v1视频通话
    };




    const callKitStyle = useMemo(() => {
        if (TUIGlobal.isPC) {
            return { width: '100%', height: window.innerHeight, margin: '0 auto' }; //接听页面样式
        }
        return { width: '100%', height: window.innerHeight,margin:'0 auto' }; //发起聊天页面样式
    }, [TUIGlobal.isPC]);

 

    return (
        <div className="liaotian">
            <TUICallKit style={callKitStyle}></TUICallKit>
            {/* <h4> {userData.isLogin ? "Call Panel" : "Login Panel"} </h4>   根据当前isLogin状态的不同显示不同的标题 */}
            <h1 style={{ marginTop: '20%', marginBottom: '20%' }}>联络感情</h1>
            <div>
                <div style={{marginBottom:'20%'}}>
                    <input
                        className="input1"
                        value={userData.isLogin ? userData.callUserID : userData.userID}
                        onChange={(value) => {
                            const key = userData.isLogin ? "callUserID" : "userID";
                            setUserData((prevState) => ({
                                ...prevState,
                                [key]: value.target.value,
                            }));
                        }}
                        style={{ width: 230, margin: '0 auto' }}
                        placeholder={
                            userData.isLogin ? "请输入对方的联系方式" : "请输入您的联系方式"
                        }
                    />
                    <button className="" onClick={userData.isLogin ? call : init}> {userData.isLogin ? "拨打电话" : "设置联系方式"} </button>
                </div>
                <p style={{ margin: '10' }}> {userData.isLogin ? `您当前的联系方式:  ${userData.userID}` : ""} </p>
            </div>
        </div>
    )

}
