import React, { useState, useEffect, useMemo } from 'react'
import { useNavigate } from 'react-router-dom'
import { Button, Space } from 'antd-mobile'
import { InfiniteScroll, List, DotLoading } from 'antd-mobile'
import './style/Dqj_My_Consultation.css'
import './style/Commonly_Used_Medicines.css'
import './style/Dqj_My_Consultation.css'
import axios from 'axios'
export default function Commonly_Used_Medicines() {
  let navigate = useNavigate()
  useEffect(() => {
    Commonly_prescribed_prescription()
  }, [])

  // 获取当前登录医生的常用处方
  let [chufang, setchufang] = useState([])
  const Commonly_prescribed_prescription = async () => {
    let user = JSON.parse(sessionStorage.getItem('user'))
    let { data: { data } } = await axios.get('/DQJ/Commonly_prescribed_prescription?_id=' + user._id)
    setchufang(data)
  }



  // 触底加载
  const InfiniteScrollContent = ({ hasMore }) => {
    return (
      <>
        {hasMore ? (
          <>
            <span>Loading</span>
            <DotLoading />
          </>
        ) : (
          <span>--- 我是有底线的 ---</span>
        )}
      </>
    )
  }
  const [len, setlen] = useState(4)
  const [hasMore, setHasMore] = useState(true)
  async function loadMore() {
    let a = setTimeout(() => {
      setlen(len + 1)
      setHasMore(len < chufang.length)
      if (len == chufang.length) {
        clearTimeout(a)
      }
    }, 2000)
  }


  return (
    <div className='Dqj_body'>
      <div className='content'>
        <p style={{ marginTop: '5%', borderLeft: '3PX solid #0099FF', marginLeft: '9%', fontSize: '1.2rem', paddingLeft: '3%' }}>常用处方（{chufang.length}）</p>
        <div className='Dqj_content'>
          {chufang.map(item => {
            return <div  key={item._id} className='Dqj_content_item'>
              <div className='Dqj_item_usedata'>
                <p style={{ margin: '5%', fontSize: '1.2rem' }}>Rp</p>
              </div>

              <div style={{ padding: '3%', borderBottom: '1px solid rgb(190,190,190)' }} onClick={() => {
                navigate('/Prescription_details',{state:{item:item}})
              }}>
                <p>
                  <span style={{ fontSize: '1rem' }}>临床诊治：</span>
                  <span style={{ fontSize: '1rem' }}>{item.diagnosis}</span>
                </p>
                <br></br>
                <p>
                  <span style={{ color: 'rgb(179,153,153)' }}>{item.name}</span>
                </p>
                <br></br>
                {/* <p>
                  <span style={{ color: 'rgb(179,153,153)' }}>{item.name}{item.usage}</span>
                </p> */}
              </div>
              <div style={{ display: 'flex', justifyContent: 'right' }}>
                <div className='keystroke'>
                  <button>删除</button>
                  <button>编辑处方</button>
                </div>
              </div>
            </div>
          })}

        </div>
      </div>
      <InfiniteScroll loadMore={loadMore} hasMore={hasMore}>
        <InfiniteScrollContent hasMore={hasMore} />
      </InfiniteScroll>
      <div className='body_buttom'>
        <Button block color='primary' size='large'>新增药品</Button>
      </div>
    </div>
  )
}
