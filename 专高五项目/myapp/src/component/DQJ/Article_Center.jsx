import React, { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { NavBar, Space, Toast, InfiniteScroll, List, DotLoading } from 'antd-mobile'
import { CloseOutline, MoreOutline, SearchOutline } from 'antd-mobile-icons'
import { Popup, Button } from 'antd-mobile'
import './style/Article_Center.css'
import axios from 'axios'
export default function Article_Center() {
    const navigate = useNavigate()
    const [visible1, setVisible1] = useState(false)
    const [data, setdata] = useState([])
    useEffect(() => {
        getcontent()
    }, [])
    const right = (
        <div style={{ fontSize: 24 }}>
            <Space style={{ '--gap': '16px' }}>
                <SearchOutline />
                <MoreOutline />
            </Space>
        </div>
    )
    const back = () =>
        Toast.show({
            content: navigate('/DQJ_home/DQJ_homepage'),
            duration: false,
        })

    const getcontent = async () => {
        const { data: { data } } = await axios.get('/DQJ/getcontent')
        setdata(data)
    }



    // 触底加载
    const InfiniteScrollContent = ({ hasMore }) => {
        return (
            <>
                {hasMore ? (
                    <>
                        <span>Loading</span>
                        <DotLoading />
                    </>
                ) : (
                    <span>--- 我是有底线的 ---</span>
                )}
            </>
        )
    }
    const [len, setlen] = useState(4)
    const [hasMore, setHasMore] = useState(true)
    async function loadMore() {
        setTimeout(() => {
            setlen(len + 4)
            setHasMore(len < data.length)
        }, 2000)
    }

    return (
        <div>
            <NavBar onBack={back}>文章中心</NavBar>
            <div className='title'>
                <div onClick={() => {
                    setVisible1(true)
                }}>
                    <img src='新增文章logo.png'></img>
                    <p>新增文章</p>
                </div>
                <div onClick={() => {
                    navigate('/wodechuangzuo/Wenzhang')
                }}>
                    <img src='我的文章logo.png'></img>
                    <p>我的创作</p>
                </div>
            </div>
            <Popup
                visible={visible1}
                bodyStyle={{ height: '25vh' }}
            >
                <div style={{ width: '100%', textAlign: 'center', fontSize: '12px', marginTop: '3%', color: 'red', padding: '2%' }}>
                    请选择文章类型
                </div>
                <Button style={{ marginBottom: '2%', marginTop: '2%' }} block color='primary' size='large' onClick={() => {
                    navigate('/Write_articles')
                }}>
                    新增文章
                </Button>
                <Button style={{ marginBottom: '2%', marginTop: '2%' }} block color='primary' size='large' onClick={()=>{
                    navigate('/Post_the_video')
                }}>
                    上传视频
                </Button>
                <Button block color='primary' size='large' onClick={() => {
                    setVisible1(false)
                }}>取消</Button>
            </Popup>

            <div className='aritle'>
                <h2>文章精选</h2>
                <div className='aritle_item'>
                    {data.slice(0,len).map((item) => {
                        return <div key={item._id} className='small_item' onClick={() => {
                            navigate('/Article_details', { state: { item: item } })
                        }}>
                            <h2>{item.title}</h2>
                            <p>{item.content}</p>
                        </div>
                    })}
                </div>
            </div>
            <InfiniteScroll loadMore={loadMore} hasMore={hasMore}>
                <InfiniteScrollContent hasMore={hasMore} />
            </InfiniteScroll>
        </div>
    )
}
