import React, { useState, useEffect, useMemo } from 'react'
import { NavBar, Space, Toast, Tabs,InfiniteScroll, List, DotLoading, } from 'antd-mobile'
import { CloseOutline, MoreOutline, SearchOutline } from 'antd-mobile-icons'
import { useNavigate } from 'react-router-dom'
import './style/Dqj_My_consultation.css'
import './style/Dqj_My_pharmacy.css'
import axios from 'axios'
export default function Dqj_My_consultation() {
  let navigate = useNavigate()
  let [data, setdata] = useState([])//我的咨询
  let [istype, setistype] = useState(1)//控制我的咨询的tab切换
  useEffect(() => {
    getdoctor()
  }, [])


  // 获取当前医生名下的咨询
  const getdoctor = async () => {
    let user = JSON.parse(sessionStorage.getItem('user'))
    let { data: { data } } = await axios.get('/DQJ/getzixun?_id=' + user._id)
    console.log(data);
    setdata(data)
  }


  const showlist = useMemo(() => {
    if (istype == 1) {
      return data
    } else if (istype == 2) {
      return data.filter(item => item.istype == true)
    } else {
      return data.filter(item => item.istype == false)
    }
  })

    // 触底加载
    const InfiniteScrollContent = ({ hasMore }) => {
      return (
        <>
          {hasMore ? (
            <>
              <span>Loading</span>
              <DotLoading />
            </>
          ) : (
            <span>--- 我是有底线的 ---</span>
          )}
        </>
      )
    }
    const [len, setlen] = useState(4)
    const [hasMore, setHasMore] = useState(true)
    async function loadMore() {
      let a = setTimeout(() => {
        setlen(len + 1)
        setHasMore(len < data.length)
        if (len == data.length) {
          clearTimeout(a)
        }
      }, 2000)
    }


  // 标题栏
  const right = (
    <div style={{ fontSize: 24 }}>
      <Space style={{ '--gap': '16px' }}>
        <SearchOutline />
        <MoreOutline />
      </Space>
    </div>
  )

  const back = () =>
    Toast.show({
      content: navigate(-1),
      duration: false,
    })

  return (
    <div className='Dqj_body'>
      {/* 头部 */}
      <div className='Dqj_title'>
        <NavBar onBack={back}>在线接诊</NavBar>
        <Tabs onChange={(e) => {
          setistype(e)
        }}>
          <Tabs.Tab title='全部' key='1' />
          <Tabs.Tab title='进行中' key='2' />
          <Tabs.Tab title='已结束' key='3' />
        </Tabs>
      </div>

      {/* 内容 */}
      <div className='Dqj_content'>
        {showlist.slice(0,len).map((item) => {
          return <div key={item._id} className='Dqj_content_item' onClick={()=>{
            if(item.istype == false){
              Toast.show({
                icon: 'fail',
                content: '聊天已结束',
              })
            }else{
              navigate('/Consultation_chat',{state:{item:{_id:item._id,doctor_id:JSON.parse(sessionStorage.getItem('user'))._id}}})
            }
          }}>
            <div className='Dqj_item_usedata'>
              <div>
                <img src={item.avatar} alt="" />
                <p>
                  <span style={{ fontSize: '1.25rem', marginTop: '-5px' }} >{item.nickname}&nbsp;&nbsp;&nbsp;&nbsp;</span>
                  <span style={{ color: 'rgb(179,153,153)' }}>{item.gender}&nbsp;&nbsp;&nbsp;&nbsp;</span>
                  <span style={{ color: 'rgb(179,153,153)' }}>{item.age}岁</span>
                </p>
              </div>
            </div>
            <p style={{ color: 'rgb(179,153,153)', padding: '6%' }}>
              2022-12-12 14:00
            </p>
          </div>
        })
        }
        <InfiniteScroll loadMore={loadMore} hasMore={hasMore}>
          <InfiniteScrollContent hasMore={hasMore} />
        </InfiniteScroll>
      </div>
    </div>
  )
}
