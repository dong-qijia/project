import React from 'react'
import { useNavigate, useLocation } from 'react-router-dom'
import { NavBar, Space, Toast } from 'antd-mobile'
import { CloseOutline, MoreOutline, SearchOutline } from 'antd-mobile-icons'
import './style/Details_of_the_drug.css'
export default function Consultation_chat() {
  let navigate = useNavigate()
  let box = useLocation().state.item
  console.log(box);
  const right = (
    <div style={{ fontSize: 24 }}>
      <Space style={{ '--gap': '1rem' }}>
        <SearchOutline />
        <MoreOutline />
      </Space>
    </div>
  )

  const back = () =>
    Toast.show({
      content: navigate(-1),
      duration: false,
    })
  return (
    <div>
      <NavBar onBack={back}>药品详情</NavBar>
      <div className='box'>
        <img src={box.image}></img>
        <p className='box_P'>￥{box.price}</p>
        <p style={{fontSize:'1.5625rem',marginBottom:'3%'}}>{box.name}{box.unit}</p>
        <p>厂商:{box.manufacturer}</p>
        <br />
        <h2>药品说明</h2>
        <br></br>
        <p>请仔细阅读{box.name}{box.unit}的作用说明，并在药师指导下购买和使用。</p>
        <p>
          <span style={{fontWeight:'700',fontSize:'.9375rem'}}>【药品名称】:</span>
          {box.name}
        </p>
        <p>
          <span style={{fontWeight:'700',fontSize:'.9375rem'}}>【药品厂家】:</span>
          {box.manufacturer}
        </p>
        <p>
          <span style={{fontWeight:'700',fontSize:'.9375rem'}}>【药品规格】:</span>
          {box.specification}
        </p>
        <p>
          <span style={{fontWeight:'700',fontSize:'.9375rem'}}>【药品数量】:</span>
          {box.quantity}
        </p>        
        <p>
          <span style={{fontWeight:'700',fontSize:'.9375rem'}}>【药品单位】:</span>
          {box.unit}
        </p>        
        <p>
          <span style={{fontWeight:'700',fontSize:'.9375rem'}}>【药品用法】:</span>
          {box.usage}
        </p>        
        <p>
          <span style={{fontWeight:'700',fontSize:'.9375rem'}}>【药品储存】:</span>
          {box.storage}
        </p>        
        <p>
          <span style={{fontWeight:'700',fontSize:'.9375rem'}}>【药品使用症状】:</span>
          {box.indications}
        </p>        
        <p>
          <span style={{fontWeight:'700',fontSize:'.9375rem'}}>【药品不良反应】:</span>
          {box.Adverse_effects}
        </p>        <p>
          <span style={{fontWeight:'700',fontSize:'.9375rem'}}>【药品注意事项】:</span>
          {box.precautions}
        </p>        <p>
          <span style={{fontWeight:'700',fontSize:'.9375rem'}}>【药品保质期】:</span>
          {box.shelf_life}
        </p>
        <p>
          <span style={{fontWeight:'700',fontSize:'.9375rem'}}>【药品生产日期】:</span>
          {box.production_date}
        </p>
        <p>
          <span style={{fontWeight:'700',fontSize:'.9375rem'}}>【药品过期日期】:</span>
          {box.expiration_date}
        </p>
        <p>
          <span style={{fontWeight:'700',fontSize:'.9375rem'}}>【药品禁忌】:</span>
          {box.taboo}
        </p>
      </div>
    </div>
  )
}
