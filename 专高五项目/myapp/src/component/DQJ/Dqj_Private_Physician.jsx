import React from 'react'
import { useNavigate } from 'react-router-dom'
import { NavBar, Space, Toast, Tabs } from 'antd-mobile'
import { CloseOutline, MoreOutline, SearchOutline } from 'antd-mobile-icons'
import './style/Dqj_My_Consultation.css'
export default function Dqj_Private_Physician() {
    let navigate = useNavigate()
    const right = (
        <div style={{ fontSize: 24 }}>
            <Space style={{ '--gap': '16px' }}>
                <SearchOutline />
                <MoreOutline />
            </Space>
        </div>
    )

    const back = () =>
        Toast.show({
            content: navigate(-1),
            duration: false,
        })
    return (
        <div className='Dqj_body'>
            {/* 头部标题 */}
            <div className='Dqj_title'>
                <NavBar onBack={back}>我的私人医生服务</NavBar>
                <hr />
                <Tabs>
                    <Tabs.Tab title='全部' key='fruits' />
                    <Tabs.Tab title='进行中' key='vegetables' />
                    <Tabs.Tab title='已结束' key='animals' />
                </Tabs>
            </div>
            <div className='Dqj_content'>
                <div className='Dqj_content_item'>
                    <div className='Dqj_item_usedata'>
                        <div>
                            <img src="https://ts4.cn.mm.bing.net/th?id=OIP-C.PobTVuzuQy7tAZtvVEc3VQAAAA&w=250&h=250&c=8&rs=1&qlt=90&o=6&dpr=1.5&pid=3.1&rm=2" alt="" />
                            <p>
                                <span style={{ fontSize: '1.25rem' }}>吴姗姗&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <span style={{ color: 'rgb(179,153,153)' }}>女&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <span style={{ color: 'rgb(179,153,153)' }}>23岁</span>
                            </p>
                        </div>
                        <p style={{ margin: '5%', fontSize: '1.25rem', color: 'rgb(179,153,153)' }}>带接诊</p>
                    </div>
                    <div style={{ padding: '3%', borderBottom: '1px solid rgb(190,190,190)' }}>
                        <p>
                            <span style={{ fontSize: '1rem' }}>服务时长:</span>
                            <span style={{ color: 'rgb(179,153,153)' }}>半年</span>
                        </p>
                        <br></br>
                        <p>
                            <span style={{ fontSize: '1rem' }}>开始时间:</span>
                            <span style={{ color: 'rgb(179,153,153)' }}>2024-10-10</span>
                        </p>
                        <br></br>
                        <p>
                            <span style={{ fontSize: '1rem' }}>结束时间:</span>
                            <span style={{ color: 'rgb(179,153,153)' }}>2024-10-10</span>
                        </p>
                    </div>
                    <div style={{display:'flex',justifyContent:'right'}}>
                        <div className='keystroke'>
                            <button onClick={()=>{
                                navigate('/Patient_details')
                            }}>患者资料</button>
                            <button onClick={()=>{
                                navigate('/Consultation_chat')
                            }}>联系患者</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    )
}
