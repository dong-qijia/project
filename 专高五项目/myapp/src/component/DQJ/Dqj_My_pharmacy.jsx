import React from 'react'
import { useNavigate,Outlet } from 'react-router-dom'
import { NavBar, Space, Toast, Tabs } from 'antd-mobile'
import { CloseOutline, MoreOutline, SearchOutline } from 'antd-mobile-icons'
import './style/Dqj_My_Consultation.css'
import './style/Dqj_My_pharmacy.css'
export default function Dqj_My_pharmacy() {
  let navigate = useNavigate()
  const right = (
    <div style={{ fontSize: 24 }}>
      <Space style={{ '--gap': '16px' }}>
        <SearchOutline />
        <MoreOutline />
      </Space>
    </div>
  )

  const back = () =>
    Toast.show({
      content: navigate('/Dqj_home/Dqj_homepage'),
      duration: false,
    })
  return (
    <div className='Dqj_body'>
      <div className='Dqj_title'>
        <NavBar onBack={back}>我的药房</NavBar>
        <Tabs onChange={(e)=>{
          if(e==='常用药品'){
            navigate('/Dqj_My_pharmacy/Commonly_Used_Medicines')
          }else if(e == '常用处方'){
            navigate('/Dqj_My_pharmacy/Commonly_Prescribed_Prescriptions')
          }else(
            navigate('/Dqj_My_pharmacy/Historical_Prescriptions')
          )
        }}>
          <Tabs.Tab title='常用药品' key='常用药品' />
          <Tabs.Tab title='常用处方' key='常用处方' />
          <Tabs.Tab title='历史处方' key='历史处方' />
        </Tabs>
      </div>
      <Outlet></Outlet>
    </div>
  )
}
