import React, { useState, useEffect } from 'react'
import { useLocation,useNavigate} from 'react-router-dom'
import { NavBar, Space, Toast } from 'antd-mobile'
import { CloseOutline, MoreOutline, SearchOutline } from 'antd-mobile-icons'
import './style/Article_Center.css'
export default function Article_details() {
  const location = useLocation()
  const navigate = useNavigate()
  const [data, setdata] = useState([])
  const right = (
    <div style={{ fontSize: 24 }}>
      <Space style={{ '--gap': '16px' }}>
        <SearchOutline />
        <MoreOutline />
      </Space>
    </div>
  )
  useEffect(() => {
    setdata(location.state.item)
  }, [])
  console.log(location.state.item);

  const back = () =>
    Toast.show({
      content: navigate(-1),
      duration: false,
    })
  return (
    <div>
      <NavBar onBack={back}>文章详情</NavBar>
      <div className='aritle'>
        <div className='aritle_item'>
          <div key={data._id} className='small_item1' >
            <h2 style={{width:'100%',paddingBottom:'5%'}}>{data.title}</h2>
            <p style={{lineHeight:'30PX',fontSize:'17PX'}}>{data.content}</p>
            <img style={{width:'100%'}} src={data.img} alt="" />
            <video style={{width:'100%',display:data.video?'block':'none'}} controls src={data.video} ></video>
          </div>
        </div>

      </div>
    </div>
  )
}
