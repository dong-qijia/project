import React, { useState, useEffect } from 'react'
import { Button, Space } from 'antd-mobile'
import { useNavigate } from 'react-router-dom'
import { InfiniteScroll, List, DotLoading } from 'antd-mobile'
import './style/Dqj_My_Consultation.css'
import './style/Commonly_Used_Medicines.css'
import axios from 'axios'
export default function Commonly_Used_Medicines() {
  useEffect(() => {
    Commonly_used_medicines()
  }, [])

  // 获取药品
  let [data, setdata] = useState([])
  let navigate = useNavigate()
  const Commonly_used_medicines = async () => {
    let { data: { data } } = await axios.get('/DQJ/Commonly_used_medicines')
    setdata(data)
  }


  // 触底加载
  const InfiniteScrollContent = ({ hasMore }) => {
    return (
      <>
        {hasMore ? (
          <>
            <span>Loading</span>
            <DotLoading />
          </>
        ) : (
          <span>--- 我是有底线的 ---</span>
        )}
      </>
    )
  }
  const [len, setlen] = useState(4)
  const [hasMore, setHasMore] = useState(true)
  async function loadMore() {
    let a = setTimeout(() => {
      setlen(len + 1)
      setHasMore(len < data.length)
      if (len == data.length) {
        clearTimeout(a)
      }
    }, 2000)
  }


  return (
    <div className='Dqj_body'>
      <div className='content'>
        <p style={{ marginTop: '5%', borderLeft: '3PX solid #0099FF', marginLeft: '9%', fontSize: '1.2rem', paddingLeft: '3%' }}>常用药物列表（{data.length}）</p>
        {data.slice(0, len).map((item, index) => {
          return <div key={item._id} className='content_body' onClick={() => { navigate(`/Details_of_the_drug`, {state: {item:item}})}}>
            <div className='content_body_item'>
              <img src={item.image} alt="" />
              <div style={{ marginTop: '5%' }}>
                <p>
                  <span style={{ fontSize: '1.2rem' }}>{item.name}</span>
                  <span>{item.unit}</span>
                </p>
                <p style={{ color: 'rgb(179,153,153)' }}>
                  {item.manufacturer}
                </p>
                <p style={{ color: 'red' }}>
                  ￥{item.price}
                </p>
              </div>
              <div style={{ marginTop: '19%' }}>
                <button>删除</button>
              </div>
            </div>
          </div>
        })}
      </div>
      <InfiniteScroll loadMore={loadMore} hasMore={hasMore}>
        <InfiniteScrollContent hasMore={hasMore} />
      </InfiniteScroll>
      <div className='body_buttom'>
        <Button block color='primary' size='large'>新增药品</Button>
      </div>
    </div>
  )
}
