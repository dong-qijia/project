import React from 'react'
import { useNavigate, useLocation } from 'react-router-dom'
import { NavBar, Space, Toast } from 'antd-mobile'
import { CloseOutline, MoreOutline, SearchOutline } from 'antd-mobile-icons'
import './style/Prescription_details.css'
export default function Consultation_chat() {
  let box = useLocation().state.item
  let navigate = useNavigate()
  const right = (
    <div style={{ fontSize: 24 }}>
      <Space style={{ '--gap': '16px' }}>
        <SearchOutline />
        <MoreOutline />
      </Space>
    </div>
  )

  const back = () =>
    Toast.show({
      content: navigate(-1),
      duration: false,
    })
  return (
    <div >
      <NavBar className='title' onBack={back}>常用处方详情</NavBar>
      <div className='box'>
        <div className='diagnosis'>
          <h2>诊断:</h2>
          <br></br>
          <p>{box.diagnosis}</p>
        </div>
        <div className='diagnosis'>
          <h2>Rp</h2>
          <br />
          <p style={{fontSize:'20PX'}}>{box.name}</p>
          <br></br>
          <p>
            <span style={{fontSize:'20px'}}>使用方法：</span>
            {box.usage}
          </p>
        </div>
      </div>
    </div>
  )
}
