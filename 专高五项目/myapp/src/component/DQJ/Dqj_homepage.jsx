import React, { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import {
  UserAddOutline, BellOutline, TextOutline, UserContactOutline, HeartOutline, FillinOutline, AppOutline,
  UserCircleOutline
} from 'antd-mobile-icons'
import { InfiniteScroll, List, DotLoading } from 'antd-mobile'
import './style/Dqj_homepage.css'
import axios from 'axios'
export default function Home() {
  let navigate = useNavigate()
  let [doctor, setdoctor] = useState([])//存储医生信息
  let [chat, setchat] = useState([])//存储聊天人员信息

  // window.location.reload()
  useEffect(() => {
    getdoctor()
  }, [])
  
  // 获取当前登录的医生信息
  const getdoctor = async () => {
    let user = JSON.parse(sessionStorage.getItem('user'))
    const { data: { data } } = await axios.get(`/DQJ/getdoctor?_id=${user._id}`)
    setdoctor(data)
    getchat(user._id)
  }

  // 获取聊天人员信息
  const getchat = async (_id) => {
    let { data: { data } } = await axios.get(`/DQJ/getzixun?_id=`+_id)
    console.log(data);
    let arr = data.filter(item=>item.istype ==true)
    setchat(arr)
  }

  // 触底加载
  const InfiniteScrollContent = ({ hasMore }) => {
    return (
      <>
        {hasMore ? (
          <>
            <span>Loading</span>
            <DotLoading />
          </>
        ) : (
          <span>--- 我是有底线的 ---</span>
        )}
      </>
    )
  }
  const [len, setlen] = useState(5)
  const [hasMore, setHasMore] = useState(true)
  async function loadMore() {
    let a = setTimeout(() => {
      setlen(len + 1)
      setHasMore(len < chat.length)
      if(len == chat.length){
        clearTimeout(a)
      }
    }, 2000)
  }


  return (
    <div style={{ backgroundColor: 'rgb(242,242,242)' }}>

      {/* 头部 */}
      <div className='Dqj_head'>
        <h3 className='Dqj_home_title'>
          <div className='Shared'>
            <span>首页</span>
            <span><BellOutline /></span>
          </div>
        </h3>
        {/* 医生信息 */}
        {doctor.map(item => {
          return <div key={item._id} className='Dqj_doctor'>
            <img src={item.avatar}></img>
            <div>
              <span className='Dqj_Doctor_Name'>{item.doctor_name}&nbsp;&nbsp;&nbsp;&nbsp;</span>
              <span className='Dqj_Doctor_detail'>{item.doctor_title}</span><br />
              <br />
              <span className='Dqj_Doctor_detail'>{item.doctor_department}</span>
            </div>
            <button className='Dqj_btn' onClick={()=>{
              navigate('/XXK_prove')
            }}>已认证</button>
          </div>
        })}
      </div>



      {/* 功能选项 */}
      <div className='Dqj_Functional_Options'>
        <div onClick={() => {
          navigate('/Dqj_My_consultation')
        }}>
          <div className='Dqj_Function'><TextOutline /></div> <span>我的问诊</span>
        </div>
        <div onClick={() => {
          navigate('/Dqj_My_pharmacy/Commonly_Used_Medicines')
        }}>
          <div className='Dqj_Function'><HeartOutline /></div><span>我的药房</span>
        </div>
        <div onClick={()=>{
          navigate('/Article_Center')
        }}>
          <div className='Dqj_Function'><FillinOutline /></div><span>创作中心</span>
        </div>
      </div>


      {/* 消息列表 */}
      <h2 className='Dqj_Message_List'>消息列表</h2>
      {chat.slice(0,len).map(item => {
        return <div key={item._id} className='Dqj_Message_List_item'>
          <div style={{ display: 'flex' }}>
            <img src={item.avatar}alt="" />
            <p>
              <span>{item.nickname}</span><br></br>
              <br></br>
              <span style={{ color: 'black' }}>吃了很多要,但是不管用,庸医!!!!!!</span>
            </p>
          </div>
          <div style={{ width: '45%' }}>
            <span>2023-10-10</span><br></br>
            <br></br>
            <span>{item.istype?'进行中':'已结束'}</span>
          </div>
        </div>
      })}

      <InfiniteScroll loadMore={loadMore} hasMore={hasMore}>
        <InfiniteScrollContent hasMore={hasMore} />
      </InfiniteScroll>
    </div>
  )
}