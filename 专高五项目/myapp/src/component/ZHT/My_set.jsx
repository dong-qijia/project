import React, { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import './css/my_set.css'
import { ArrowLeft, Arrow } from '@react-vant/icons';
import axios from 'axios';

export default function My_set() {
    let nav = useNavigate()

    const logout = ()=>{
        sessionStorage.clear()
        nav('/User_Home/mys')
    }
    return (
        <div className='myset'>
            <div className='ms_top'>
                <div className='ms-top_ico'><ArrowLeft onClick={() => { nav('/User_Home/mys') }} /></div>
                <div className='ms-top_name'>设置</div>
            </div>

            {/*  */}
            <div className={sessionStorage.getItem('token')?'dis':'ms_con'}>
                <div className='ms_con_li1'>
                    <div className='li1_left'>消息设置</div>
                    <div className='li1_right'><Arrow /></div>
                </div>
                <div className='ms_con_li'>
                    <div className='li1_left'>隐私管理</div>
                    <div className='li1_right'><Arrow /></div>
                </div>
                <div className='ms_con_li'>
                    <div className='li1_left'>用户服务协议</div>
                    <div className='li1_right'><Arrow /></div>
                </div>
                <div className='ms_con_li'>
                    <div className='li1_left'>清理本地缓存</div>
                    <div className='li1_right'><Arrow /></div>
                </div>
                <div className='ms_con_li2'>
                    <div className='li1_left'>关于我们</div>
                    <div className='li1_right'><Arrow /></div>
                </div>
            </div>
            {/*  */}

            {/* 2 */}
            <div className={sessionStorage.getItem('token')?'ms_con':'dis'}>
                <div className='ms_con_li1'>
                    <div className='li1_left' onClick={()=>{nav('/personage')}}>个人资料</div>
                    <div className='li1_right'><Arrow /></div>
                </div>
                <div className='ms_con_li2'>
                    <div className='li1_left' onClick={()=>{nav('/zhanghao')}} >账号与安全</div>
                    <div className='li1_right'><Arrow /></div>
                </div>

                <div className='ms_con_li3'>
                    <div className='li1_left'>消息设置</div>
                    <div className='li1_right'><Arrow /></div>
                </div>
                <div className='ms_con_li'>
                    <div className='li1_left'>隐私设置</div>
                    <div className='li1_right'><Arrow /></div>
                </div>
                <div className='ms_con_li'>
                    <div className='li1_left'>用户服务协议</div>
                    <div className='li1_right'><Arrow /></div>
                </div>
                <div className='ms_con_li'>
                    <div className='li1_left'>清理本地缓存</div>
                    <div className='li1_right'><Arrow /></div>
                </div>
                <div className='ms_con_li2'>
                    <div className='li1_left'>关于我们</div>
                    <div className='li1_right'><Arrow /></div>
                </div>

                <div className='ms_con_li4' onClick={()=>{logout()}}>
                    退出登录
                </div>
            </div>


            {/* {localStorage.getItem('token')} */}




            {/*  */}
        </div>
    )
}
