import React from 'react'
import './css/aichat.css'
import { useNavigate } from 'react-router-dom'
import { ArrowLeft } from '@react-vant/icons';

export default function Aichart() {
  let nav = useNavigate()
  return (
    <div className='ai'>
       <div className='ai_top'>
                <div className='ai-top_ico'><ArrowLeft onClick={() => { nav('/User_Home/mys') }} /></div>
                <div className='ai-top_name'>客服与帮助</div>
            </div>
      <div className='list'>
        <div className='li'  onClick={()=>{nav('/chatserve')}}>客服</div>
      </div>
    </div>
  )
}
