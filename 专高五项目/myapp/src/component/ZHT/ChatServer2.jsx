


import React from 'react';
import { UIKitProvider, TUIChat } from '@tencentcloud/chat-uikit-react';
import { TUILogin } from '@tencentcloud/tui-core';
import { TUIConversationService } from "@tencentcloud/chat-uikit-engine";
import '@tencentcloud/chat-uikit-react/dist/cjs/index.css';
const config = {
  SDKAppID: 1600052418, // Your SDKAppID
  userID: '111', // Login UserID
  userSig: 'eJwtzEsLgkAUhuH-Mutw5hw9OgktCimpQOi2chPMFCfNBpPoQv89U5ff88H7Ebv11nvYWsQCPSVG3WZjq4ZP3DEADHw3xdE5NiKGUClFGIDuH-t0XNvWiQjbq9eGr3*LkEJNQDhU*NxWV7PlXF8cJIds4xdl6k*rLLGR2WevNFhALm8NlrlU43cuIzUR3x**9C9w', // Your userSig
}

TUILogin.login({
  ...config,
  useUploadPlugin: true
}).then(() => {
  openChat();
}).catch(() => { });

function openChat() {
  // 1v1 chat: conversationID = 
  // group chat: conversationID = `GROUP${groupID}`
  const userID = '222'; // userID: 消息接收方的 userID
  const conversationID = `C2C${userID}`;
  TUIConversationService.switchConversation(conversationID);
}

export default function SampleChat() {
  // language support en-US / zh-CN / ja-JP / ko-KR
  return (
    <div style={{display: 'flex', height: '100vh'}}>
      <UIKitProvider language={'en-US'}>
        <TUIChat />
      </UIKitProvider>
    </div>   
  );
}