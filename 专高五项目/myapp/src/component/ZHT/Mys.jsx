import React, { useState, useEffect } from 'react'
import '../ZHT/css/mys.css'
import { Tabbar } from 'react-vant'
import axios from 'axios'

import {
  ChatO, SettingO, PendingPayment, SendGiftO, GiftO,
  NotesO, ClockO, WarningO, OrdersO, AwardO, UnderwayO, StarO, LocationO,
  SmileO, CommentO, InfoO, Qr, HomeO, Search, LikeO, BagO, Contact
} from '@react-vant/icons';
import { useNavigate } from 'react-router-dom'

export default function Mys() {
  const nav = useNavigate()
  const [name, setName] = useState('mys')
  const [user,setUser] = useState([])


  const goss = (v) => {
    if (sessionStorage.getItem('user')) {
      nav('/ym_order')
      // if(v== 'dfk'){
      //   nav('/ym_order',{state:{tag:0}})
      // }
      // if(v== 'dfh'){
      //   nav('/ym_order',{state:{tag:1}})
      // }

    }
    else {
      nav('/Login')
    }

  }

  const gos2 = (tag) => {
    if (sessionStorage.getItem('user')) {
      if (tag == 'xt') {
        nav('/blood_d')
      }
      if (tag == 'yy') {
        nav('/use_drug')
      }
      if (tag == 'ys') {
        nav('/diet')
      }
      if (tag == 'use_rec') {
        nav('/use_rec')
      }
      if (tag == 'jf') {
        nav('/integral')
      }
      if (tag == 'ai') {
        nav('/Aichart')
      }
    }
    else {
      nav('/Login')
    }
  }
  const chenge = (v) => {
    setName()
    nav('/' + v)

  }

  const getuser = async () => {
    let data2 = JSON.parse(sessionStorage.getItem('user'))
    let ids = data2._id
    let data = await axios.get(`/ZHT/getuser?id=${ids}`)
    console.log(data.data.data);
    setUser(data.data.data)
    
  }

  const goset = ()=>{
    nav('/my_set')
  }

  useEffect(() => {
    getuser()
    // console.log(sessionStorage.getItem('user'));
    
  }, [])


  return (
    <div className='mys'>
      {/* 上面色块 */}
      <div className={sessionStorage.getItem('user') ? 'dis' : 'mys-top'}>
        <div className='mys-top-xiao'>
          <ChatO color='white' />
        </div>
        <div className='mys-top-she'>
          <SettingO color='white' onClick={()=>{goset()}}  />
        </div>
        <div className='mys-top-con'>
          <div className='mys-top-con-toux'>未登录</div>
          <div
            className='mys-top-con-login'
            onClick={() => { nav('/Login') }}
          >登录/注册</div>
        </div>
      </div>

      {/* 上面色块2 */}

      <div className={sessionStorage.getItem('user') ? 'mys-tops' : 'dis'}>
        <div className='mys-top-xiao'>
          <ChatO color='white' onClick={() => { nav('/logins') }} />
        </div>
        <div className='mys-top-she'>
          <SettingO color='white' onClick={()=>{goset()}} />
        </div>
        {user.map(item => (
            <div className='mys-top-ok-con' key={item._id}>
       
          <div className='mys-top-ok-zhu' onClick={() => { nav('/perhome') }}>个人主页</div>


          <div className='mys-top-ok-toux'>
            <img src={item.avatar} alt='' />

          </div>
          <div className='mys-top-ok-name' >
              <span className='m-t-o-name'>{item.username}</span>
              <span className='m-t-o-ath'>{item.age}周岁</span>
            </div>


          <div className='mys-top-ok-list'>
            <div className='guanzhu'>
              <span className='g-z-num'>{item.guanzhu}</span>
              <span className='g-z'>关注</span>
            </div>
            <div className='guanzhu'>
              <span className='g-z-num'>{item.fensi}</span>
              <span className='g-z'>粉丝</span>
            </div>
            <div className='guanzhu'>
              <span className='g-z-num'>{item.beizan}</span>
              <span className='g-z'>被赞</span>
            </div>
          </div>
        </div>
          ))}
        

      </div>
      {/* 模块1 */}
      <div className='mys-mode1'>
        <div className='m-m-head'>
          <span className='m-m-head-tit'>我的订单</span>
          <span className='m-m-head-go'>全部订单</span>
        </div>
        <div className='m-m-list'>
          <div className='m-m-li'>
            <span className='m-m-li-ico'>
              <PendingPayment color='rgb(26, 209, 179)' onClick={() => { goss('dfk') }} />
            </span>
            <span className='m-m-li-iname'>待付款</span>
          </div>

          <div className='m-m-li'>
            <span className='m-m-li-ico'>
              <SendGiftO color='rgb(26, 209, 179)' onClick={() => { goss('dfh') }} />
            </span>
            <span className='m-m-li-iname'>待发货</span>
          </div>

          <div className='m-m-li'>
            <span className='m-m-li-ico'>
              <GiftO color='rgb(26, 209, 179)' onClick={() => { goss() }} />
            </span>
            <span className='m-m-li-iname'>待收货</span>
          </div>

          <div className='m-m-li'>
            <span className='m-m-li-ico'>
              <ChatO color='rgb(26, 209, 179)' onClick={() => { goss() }} />
            </span>
            <span className='m-m-li-iname'>待评价</span>
          </div>
        </div>
      </div>
      {/*  */}


      {/* 模块2 */}
      <div className='mys-mode2'>
        <div className='m-m-head'>
          <span className='m-m-head-tit'>健康管理</span>
          {/* <span className='m-m-head-go'>全部订单</span> */}
        </div>
        <div className='m-m-list'>
          <div className='m-m-li'>
            <span className='m-m-li-ico'>
              <NotesO color='rgb(0, 128, 128)' onClick={() => { gos2('xt') }} />
            </span>
            <span className='m-m-li-iname'>血糖监测</span>
          </div>

          <div className='m-m-li'>
            <span className='m-m-li-ico'>
              <ClockO color='rgb(245, 154, 35)' onClick={() => { gos2('yy') }} />
            </span>
            <span className='m-m-li-iname'>用药提醒</span>
          </div>

          <div className='m-m-li'>
            <span className='m-m-li-ico'>
              <WarningO color='rgb(2, 125, 180)' onClick={() => { gos2('ys') }} />
            </span>
            <span className='m-m-li-iname'>饮食监测</span>
          </div>

          <div className='m-m-li'>
            <span className='m-m-li-ico'>
              <OrdersO color='rgb(163, 0, 20)' onClick={() => { gos2('use_rec') }} />
            </span>
            <span className='m-m-li-iname'>用药记录</span>
          </div>
        </div>
      </div>
      {/*  */}


      {/* 模块3 */}
      <div className='mys-mode3'>
        <div className='m-m-head'>
          <span className='m-m-head-tit'>基础服务</span>
          {/* <span className='m-m-head-go'>全部订单</span> */}
        </div>
        <div className='m-m-list2'>
          <div className='m-m-li2'>
            <span className='m-m-li-ico2'>
              <AwardO color='rgb(245, 154, 35)' onClick={() => { gos2('jf') }} />
            </span>
            <span className='m-m-li-iname2'>积分兑换</span>
          </div>

          <div className='m-m-li2'>
            <span className='m-m-li-ico2'><InfoO color='rgb(245, 154, 35)' onClick={() => { gos2('ai') }} /></span>
            <span className='m-m-li-iname2'>帮助与客服</span>
          </div>
          
          <div className='m-m-li2'>
            <span className='m-m-li-ico2'><StarO color='rgb(0, 135, 128)' onClick={() => { gos2() }} /></span>
            <span className='m-m-li-iname2'>我的收藏</span>
          </div>

          <div className='m-m-li2'>
            <span className='m-m-li-ico2'><LocationO color='rgb(112, 182, 3)' onClick={() => { gos2() }} /></span>
            <span className='m-m-li-iname2'>收货地址</span>
          </div>

          <div className='m-m-li2'>
            <span className='m-m-li-ico2'><SmileO color='rgb(0, 135, 128)' onClick={() => { gos2() }} /></span>
            <span className='m-m-li-iname2'>大字模式</span>
          </div>

          <div className='m-m-li2'>
            <span className='m-m-li-ico2'><CommentO color='rgb(217, 0, 27)' onClick={() => { gos2() }} /></span>
            <span className='m-m-li-iname2'>意见反馈</span>
          </div>

          <div className='m-m-li2'>
            <span className='m-m-li-ico2'><UnderwayO color='rgb(52, 120, 246)' onClick={() => { gos2() }} /></span>
            <span className='m-m-li-iname2'>浏览足迹</span>
          </div>

          <div className='m-m-li2'>
            <span className='m-m-li-ico2'><Qr color='rgb(127, 127, 127)' onClick={() => { gos2() }} /></span>
            <span className='m-m-li-iname2'>更多</span>
          </div>

        </div>
      </div>
      {/*  */}

      {/* 底部标签栏 */}

      {/* <div className='bot_tab'>
        <div className='demo-tabbar'>
          <Tabbar value={name} onChange={(v) => { chenge(v) }}>
            <Tabbar.Item name='User_Home' icon={<HomeO />}>
              首页
            </Tabbar.Item>
            <Tabbar.Item name='she' icon={<Search />}>
              社区
            </Tabbar.Item>
            <Tabbar.Item name='wen' icon={<LikeO />}>
              问诊
            </Tabbar.Item>
            <Tabbar.Item name='shang' icon={<BagO />}>
              商城
            </Tabbar.Item>
            <Tabbar.Item name='mys' icon={<Contact />}>
              我的
            </Tabbar.Item>
          </Tabbar>
        </div>
      </div> */}

    </div>
  )
}
