import './css/history.css'
import React, { useState, useEffect, useMemo } from 'react'
import { useNavigate } from 'react-router-dom';
import { ArrowLeft } from '@react-vant/icons';
import axios from 'axios';
import { DropdownMenu } from 'react-vant'

const option1 = [
  { text: '2022年', value: '2022' },
  { text: '2023年', value: '2023' },
  { text: '2024年', value: '2024' },
]

const option2 = [
  { text: '1月', value: '01' },
  { text: '2月', value: '02' },
  { text: '3月', value: '03' },
  { text: '4月', value: '04' },
  { text: '5月', value: '05' },
  { text: '6月', value: '06' },
  { text: '7月', value: '07' },
  { text: '8月', value: '08' },
  { text: '9月', value: '09' },
  { text: '10月', value: '10' },
  { text: '11月', value: '11' },
  { text: '12月', value: '12' },
]

export default function History() {
  const nav = useNavigate()
  const [value, setValue] = useState({})
  const [list, setList] = useState([])

  const getlist = async () => {
    let data = await axios.get(`/ZHT/getblood?years=${value.value1}&mounth=${value.value2}`)
    setList(data.data.data)
  }

  useEffect(() => {
    getlist()
  },[value])


  return (
    <div>
      <div className='his_header'>
        <div className='his_top'>
          <div className='his-top_ico'><ArrowLeft onClick={() => { nav('/blood_d') }} /></div>
          <div className='his-top_name'>监测记录</div>
        </div>
      </div>
      <div className='h_list'>
        <div className='list_top'>
          <DropdownMenu value={value} onChange={v => {
            setValue(v)
            // getlist(v)
          }}>
            <DropdownMenu.Item name='value1' options={option1} />
            <DropdownMenu.Item name='value2' options={option2} />
          </DropdownMenu>
        </div>
        {list.map(item => (
          <div className='h_li' key={item._id}>
            <div className='h_li_left'>
              <div className='rip'>{item.years}年</div>
              <div className='rip'>{item.mounth}月</div>
              <div className='rip'>{item.times}</div>
            </div>
            <div className='h-li_right'>
              {item.nums}mmol/l
            </div>
          </div>
        ))}
      </div>

    </div>
  )
}
