import React, { useState, useEffect, useMemo } from 'react'
import '../ZHT/css/my_order.css'
import { ArrowLeft } from '@react-vant/icons';
import { Tabs } from 'react-vant'
import { useNavigate, useLocation } from 'react-router-dom';
import axios from 'axios';

const items = Array.from({ length: 5 }, (_, i) => i + 1)

export default function My_order() {
  const nav = useNavigate()
  const state = useLocation()
  const [lists, setLists] = useState([])

  const li = ['un', '全部', '待付款', '待发货', '待收货', '待评价']
  const list = useState('全部数据')
  const [tag, setTag] = useState('')
  const change = (v) => {
    console.log(li[v.index + 1]);
    setTag(li[v.index + 1])
  }

  const getlists = async () => {
    let data = await axios.get('/ZHT/getgoods')
    setLists(data.data.data)
    console.log(data.data.data);
  }

  const changetype = async(id,ty)=>{    
    await axios.get(`/ZHT/chengegoods?id=${id}&types=${ty}`)
    getlists()
  }

  useEffect(() => {
    getlists()
  }, [])



  let newList = useMemo(() => {
    if (tag == '全部') {
      return lists
    }
    if (tag == '待付款') {
      return lists.filter(item => item.types == '0')
    }
    if (tag == '待发货') {
      return lists.filter(item => item.types == '1')
    }
    if (tag == '待收货') {
      return lists.filter(item => item.types == '2')
    }
    if (tag == '待评价') {
      return lists.filter(item => item.types == '3')
    }
    else {
      return lists
    }
  }, [li, list])



  return (
    <div className='my_order'>
      <div className='my_header'>
        <div className='my_top'>
          <div className='top_ico'><ArrowLeft onClick={() => { nav('/User_Home/mys') }} /></div>
          <div className='top_name'>我的订单</div>
        </div>
        <div className='my-shai'>
          <Tabs defaultActive={0} onClickTab={(v) => { change(v) }}>
            {items.map(item => (
              <Tabs.TabPane key={item} className='shai_con' title={`${li[item]}`}>
                <div className='con'>
                  {newList.map(item => (
                    <div className='my_con_li' onClick={()=>{nav('/Detailsxxy')}} key={item._id}>
                      <div className='con_li_top'>
                        <span>订单号:{item.dan}</span>
                        <span className='lei'>{item.types == 0 ? '待付款':item.types == 1 ? '待发货':item.types == 2 ? '待收货':item.types == 3 ? '待评价':'已完成'}</span>
                      </div>
                      <div className='con_li_cons'>
                        <div className='cons_img'>
                          <img src={item.img} />
                        </div>
                        <div className='cons_text'>
                          <div className='text_name'>{item.name}</div>
                          <div className='text_price'>￥{item.price}</div>
                        </div>
                      </div>
                      <div className='con_li_bottom'>
                        <div className='bottom_wu'>确认物流</div>
                        <div className='bottom_ok' onClick={()=>{changetype(item._id,3)}}>确定收货</div>
                        <div className={item.types == 0?'bottom_fu':'dis'} onClick={()=>{changetype(item._id,1)}}>去付款</div>
                        <div className={item.types == 1?'bottom_fa':'dis'} onClick={()=>{changetype(item._id,2)}}>催发货</div>
                        <div className={item.types == 3?'bottom_w':'dis'}>完成</div>
                      </div>
                    </div>
                  ))}

                </div>
              </Tabs.TabPane>
            ))}
          </Tabs>

        </div>

      </div>
    </div>
  )
}
