
import React from 'react';
import { UIKitProvider, TUIChat } from '@tencentcloud/chat-uikit-react';
import { TUILogin } from '@tencentcloud/tui-core';
import { TUIConversationService } from "@tencentcloud/chat-uikit-engine";
import '@tencentcloud/chat-uikit-react/dist/cjs/index.css';
const config = {
  SDKAppID: 1600052418, // Your SDKAppID
  userID: '222', // Login UserID
  userSig: 'eJyrVgrxCdYrSy1SslIy0jNQ0gHzM1NS80oy0zIhwkZGUOHilOzEgoLMFCUrQzMDAwNTIxNDC4hMakVBZlEqUNzU1NQIKAURLcnMBYmZG5maWZgaWlpCTclMB5qalBIUEeAc5pNT6pia7mvhm*pWbBGYEpicm1bu6xeQmOfu5puUY5mR7BkYaatUCwBeRzBm', // Your userSig
}
TUILogin.login({
  ...config,
  useUploadPlugin: true
}).then(() => {
  openChat();
}).catch(() => { });

function openChat() {
  // 1v1 chat: conversationID = 
  // group chat: conversationID = `GROUP${groupID}`
  const userID = '111'; // userID: 消息接收方的 userID
  const conversationID = `C2C${userID}`;
  TUIConversationService.switchConversation(conversationID);
}

export default function SampleChat() {
  // language support en-US / zh-CN / ja-JP / ko-KR
  return (
    <div style={{display: 'flex', height: '100vh'}}>
      <UIKitProvider  language={'en-US'}>
        <TUIChat />
      </UIKitProvider>
    </div>   
  );
}