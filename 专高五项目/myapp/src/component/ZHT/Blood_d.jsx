import React, { useState } from 'react'
import './css/blood_d.css'
import { useNavigate } from 'react-router-dom';
import { ArrowLeft } from '@react-vant/icons';
import axios from 'axios';
import dayjs from 'dayjs'

export default function Blood_d() {
  const nav = useNavigate()
  const [blood_nums, setBlood_nums] = useState(0)
  
  const [year,setYears] = useState('')
  const [mounth,setMounth] = useState('')
  const [day,setDay] = useState('')
  const [times,setTimes] = useState('')
  const [t, setT] = useState('开始检测')
  // const [b_list, setB_list] = useState([
  //   { id: 1, name: '凌晨' }, { id: 2, name: '早餐前' },
  //   { id: 3, name: '早餐后' }, { id: 4, name: '午餐前' },
  //   { id: 5, name: '午餐后' }, { id: 6, name: '晚餐前' },
  //   { id: 7, name: '晚餐后' }, { id: 8, name: '睡前' },
  // ])
  const [zid_num, setZid_num] = useState(0)


  const countime = async() => {
    setT('检测中...')
    setTimeout(function () {
      setT('开始检测')
      let times = Math.floor(Math.random() * 10) + 0.5
      setZid_num(times.toFixed(2))
      // console.log(dayjs().format(),zid_num);
      
    }, 3000)
    // await axios.get(`/ZHT/addblood?data=${dayjs().format()}&nums=${tag}`)
    
  }

  const addhis = async() => {
    await axios.get(`/ZHT/addblood?years=${year}&mounth=${mounth}&day=${day}&times=${times}&nums=${blood_nums}`)
    nav('/history')
  }

  return (
    <div className='boold_d'>
      <div className='boold_header'>
        <div className='boold_top'>
          <div className='b-top_ico'><ArrowLeft onClick={() => { nav('/User_Home/mys') }} /></div>
          <div className='b-top_name'> 血糖监测</div>
        </div>
      </div>
      <div className='banner'>
        <div className='banner_top'>
          <span className='banner_tit'>血糖曲线(近24小时)</span>
          <div className='lish' onClick={() => { nav('/history') }}>历史记录</div>
        </div>
        <div className='banner_con'>{zid_num}</div>
        {/*  */}
        <div className='banner_tu'>
          <div className='ce_zid' onClick={() => { countime() }}>{t}</div>
          {/* <div className='ce_zid2'></div> */}
        </div>
        {/*  */}
      </div>

      <div className='boold_record'>
        <div className='b-r-top'>手动录入</div>
        {/* 图表2 */}
        <div className='clb'>
          <div className='tab'>
            <button 
          onClick={()=>{setYears(dayjs().format('YYYY'))}}
          className='gettime'
          >获取当前年份</button>
          <div className='show_time'>{year}</div>
          </div>

          <div className='tab'>
            <button 
          onClick={()=>{setMounth(dayjs().format('MM'))}}
          className='gettime'
          >获取当前月份</button>
          <div className='show_time'>{mounth}</div>
          </div>

          <div className='tab'>
            <button 
          onClick={()=>{setDay(dayjs().format('DD'))}}
          className='gettime'
          >获取当前日期</button>
          <div className='show_time'>{day}</div>
          </div>

          <div className='tab'>
            <button 
          onClick={()=>{setTimes(dayjs().format('HH:mm:ss'))}}
          className='gettime'
          >获取当前时间</button>
          <div className='show_time'>{times}</div>
          </div>


          
          <div className='cl'></div>
          <div className='billd_nums'>
            <div className='nums_tit'>血糖值</div>
            <div className='nums_inp'>
              <input type="text"  value={blood_nums} onChange={(e)=>{setBlood_nums(e.target.value)}} />
            </div>
          </div>

        </div>
        {/*  */}
        <div className='submit' onClick={() => { addhis() }}>保存</div>

      </div>
    </div>
  )
}
