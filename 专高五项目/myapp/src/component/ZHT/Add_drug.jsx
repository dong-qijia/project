import React, { useState } from 'react'
import './css/add_drug.css'
import { ArrowLeft } from '@react-vant/icons';
import { useNavigate, useLocation } from 'react-router-dom';
import { Popup, Cell } from 'react-vant';
import { Form, Selector } from 'react-vant'
import { Picker } from 'react-vant'
import axios from 'axios'

const options = [
  {
    label: '周一',
    value: '1',
  },
  {
    label: '周二',
    value: '2',
  },
  {
    label: '周三',
    value: '3',
  },
  {
    label: '周四',
    value: '4',
  },
  {
    label: '周五',
    value: '5',
  },
  {
    label: '周六',
    value: '6',
  },
  {
    label: '周日',
    value: '7',
  },
]

export default function Add_drug() {
  const nav = useNavigate()

  const [yaoname, setYaoname] = useState('')

  // 弹出层
  const [state, setState] = useState('')
  const onClose = () => setState('')
  // 单次用量选择
  const [value, setValue] = useState()
  // 重复周次
  const [cycle,setCycle] = useState([])

  // 请求
  const adds = async()=>{
    if(yaoname !==''&value !== ''){
      await axios.get(`/ZHT/addhavmed?name=${yaoname}&once=${value}`)
      nav('/use_drug')
    }
  }

  return (
    <div className='add_drug'>
      <div className='add_drug_top'>
        <div className='add_top_ico'><ArrowLeft onClick={() => { nav('/use_drug') }} /></div>
        <div className='add_top_name'>添加用药提醒</div>
      </div>
      
      

      <div className='add_drug_content1'>
        <div className='a_d_c1_text'>药品名称</div>
        <div className='a_d_c1_inp'>
          <input type="text" placeholder='请输入药品名称' value={yaoname} onChange={(e) => { setYaoname(e.target.value) }} />
        </div>
      </div>

      <div className='add_drug_content2'>
        <div className='add_d_c2_num'>
          <Cell title='次数/用量' isLink onClick={() => setState('bottom')} />
          <Popup
            visible={state === 'bottom'}
            style={{ height: '40%', }}
            position='bottom'
            onClose={onClose}
          >
            <Picker
              value={value}
              onChange={(val, index) => {
                setValue(val)
                console.log(val, index);

              }}
              columns={[
                ['每次1片', '每次2片', '每次3片', '每次4片', '每次5片', '每次6片'],
                ['每天1次', '每天2次', '每天3次', '每天4次', '饭前(每天3次)', '饭后(每天3次)', '睡前(每天3次)'],
              ]}
            />
          </Popup>
        </div>
        <div className='add_d_c2_num2'>{value}</div>
      </div>

      <div className='add_drug_content3'>
        <div className='a_d_c1_text'>重复周期</div>
        <div className='xuan'>
          <Selector
            className='aaa'
            options={options}
            defaultValue={['1','2', '3','4','5','6','7']}
            multiple={true}
            onChange={(arr, extend) => {
              console.log(extend.items)
            }}
          />
        </div>
      </div>
      <button className={yaoname == '' || value == ''?'submit2-x':'submit2'} disabled={yaoname == '' || value == ''} onClick={()=>{adds()}}>保存</button>

    </div>
  )
}
