import React, { useState, useEffect } from 'react'
import './css/use_drug.css'
import { ArrowLeft } from '@react-vant/icons';
import { useNavigate, useLocation } from 'react-router-dom';
import axios from 'axios';
import { DeleteO } from '@react-vant/icons';

export default function Use_drug() {
  const nav = useNavigate()
  const [druglist, setDruglist] = useState([])

  const getlist = async () => {
    let data = await axios.get('/ZHT/havmedlist')
    setDruglist(data.data.list)
  }
  const del = async(id)=>{
    console.log(id);
    await axios.get(`/ZHT/delhavmed/?id=${id}`)
    getlist()
  }

  useEffect(() => {
    getlist()
  }, [])


  return (
    <div className='use_drug'>
      <div className='use_drug_top'>
        <div className='use-top_ico'><ArrowLeft onClick={() => { nav('/User_Home/mys') }} /></div>
        <div className='use-top_name'>用药提醒</div>
      </div>

      <div className={druglist.length == 0?'null_con':'dis'}>
        <div className='null_img'>还没有提醒~~~</div>
        <div className='null_but' onClick={() => { nav('/add_drug') }}>添加提醒</div>
      </div>

      <div className={druglist.length !==0 ?'hav_con':'dis'}>
        {druglist.map(item => (
          <div className='h_con_li' key={item._id}>
            <div className='li_left'>
              <span className='li_name'>{item.name}</span>
              <span className='li_once'>{item.once}</span>
            </div>
            <div className='li_right'>
              <div className='li_ic'> <DeleteO onClick={()=>{del(item._id)}} /></div>
            </div>
          </div>
        ))}
      </div>
    </div>
  )
}
