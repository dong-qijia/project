import './css/personage.css'
import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom';
import { ArrowLeft } from '@react-vant/icons';
import axios from 'axios';
export default function Personage() {
  const nav = useNavigate();
  const users = JSON.parse(sessionStorage.getItem('user'))

  return (
    <div className='personage'>
      <div className='personage_header'>
        <div className='personage_top'>
          <div className='per-top_ico'><ArrowLeft onClick={() => { nav('/my_set') }} /></div>
          <div className='per-top_name'>个人中心</div>
        </div>
      </div>
      <div className='per_con'>
        <div className='per_li'>
          <div className='per_name'>头像</div>
          <div className='per_img'>
            <img src={users.avatar} alt="" />
          </div>

        </div>

        <div className='per_li2'>
          <div className='per_name2'>昵称</div>
          <div className='per_text'>
            {users.username}
          </div>

        </div>

        <div className='per_li2'>
          <div className='per_name2'>年龄</div>
          <div className='per_text'>
            {users.age}
          </div>
        </div>

        <div className='per_li3'>
          <div className='per_name2'>性别</div>
          <div className='per_text'>
            {users.sex}
          </div>
        </div>

      </div>
    </div>
  )
}
