import React, { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { ArrowLeft, SettingO, ChatO } from '@react-vant/icons';
import axios from 'axios'
import '../ZHT/css/perhome.css'

export default function Perhome() {
  let nav = useNavigate()
  const [user, setUser] = useState([])
  const [title, setTitle] = useState([])
  const [isgz, setIsgz] = useState(false)
  const getuser = async () => {
    let data2 = JSON.parse(sessionStorage.getItem('user'))
    let ids = data2._id
    let data = await axios.get(`/ZHT/getuser?id=${ids}`)
    // console.log(data.data.data);
    setUser(data.data.data)
  }

  const gettitle = async () => {
    let uids = JSON.parse(sessionStorage.getItem('user'))
    let data = await axios.get(`/ZHT/gettitle?uid=${uids._id}`)
    setTitle(data.data.data);
    console.log(data);

    // console.log(uids._id);

  }

  useEffect(() => {
    getuser()
    gettitle()
  }, [])
  return (
    <div className='perhome'>
      <div className='ph_top'>
        <div className='ph_top_back'>
          <div className='ph_top_ico'><ArrowLeft color='white' onClick={() => { nav('/User_Home/mys') }} /></div>
          <div className='ph_top_name'><SettingO color='white' onClick={() => { nav('/User_Home/mys') }} /></div>
        </div>
        {/*  */}
        {user.map(item => (
          <div className='ph_info' key={item._id}>
            <div className='ph_toux'>
              <img src={item.avatar} />
            </div>
            <div className='ph_name'>{item.username}</div>
            <div className='ph_sex'>{item.sex}|</div>
            <div className='ph_age'>{item.age}周岁</div>
            <div className='ph_list'>

              <div className='ph_guanzhu'>
                <span className='ph_g-z-num'>{item.guanzhu}</span>
                <span className='ph_g-z'>关注</span>
              </div>
              <div className='ph_guanzhu'>
                <span className='ph_g-z-num'>{item.fensi}</span>
                <span className='ph_g-z'>粉丝</span>
              </div>
              <div className='ph_guanzhu'>
                <span className='ph_g-z-num'>{item.beizan}</span>
                <span className='ph_g-z'>被赞</span>
              </div>
            </div>
          </div>
        ))}

        {/*  */}

        <div className={isgz?'dis':'ph_gz'} onClick={()=>{setIsgz(true)}}>关注</div>
        <div className={isgz?'':'dis'}>
          <div className='gz_ok' onClick={()=>{setIsgz(false)}}>已关注</div>
          <div className='tack'>发信息</div>
        </div>
        {/* <div className='tack_ico'><ChatO  /></div> */}

      </div>
      <div className='ph_con'>
        <div className='ph_con_tit'>
          全部帖子({title.length})
        </div>
        <div className='ph_lists'>
          {title.map(item => (
            <div className='ph_li' key={item._id}>
              <div className='ph_li_img'>
                <img src={item.img}></img>
              </div>
              <div className='ph_li_t'>
                <span className='li_t'>{item.title}</span>
              </div>
            </div>
          ))}

        </div>
      </div>
    </div>

  )
}
