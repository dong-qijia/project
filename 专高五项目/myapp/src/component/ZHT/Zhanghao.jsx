import React, { useState } from 'react'
import './css/zhanghao.css'
import { useNavigate } from 'react-router-dom';
import { ArrowLeft,Arrow } from '@react-vant/icons';
import { Switch } from 'react-vant';

export default function Zhanghao() {
    const nav = useNavigate();
    const users = JSON.parse(sessionStorage.getItem('user'))
    return (
        <div className='zhang'>
            <div className='zhang_header'>
                <div className='zhang_top'>
                    <div className='zha-top_ico'><ArrowLeft onClick={() => { nav('/my_set') }} /></div>
                    <div className='zha-top_name'>账号与安全</div>
                </div>
            </div>

            <div className='zhang_con'>
                <div className='zhang_list'>
                    <div className='zhang_tit'>账号设置</div>
                    <div className='zhang_tow'>
                        <div className='two_name'>手机号</div>
                        <div className='two_text'>
                            {users.phone}
                        </div>
                    </div>
                </div>

                <div className='zhang_list'>
                    <div className='zhang_tit'>第三方登录</div>
                    <div className='zhang_tow'>
                        <div className='two_name'>微信</div>
                        <div className='two_text3'>
                            <Switch defaultChecked />
                        </div>
                    </div>
                </div>

                <div className='zhang_list'>
                    <div className='zhang_tit'>安全设置</div>
                    <div className='zhang_tow'>
                        <div className='two_name'>注销账号</div>
                        <div className='two_text2'>
                            <Arrow />
                        </div>
                    </div>
                </div>

            </div>

        </div>
    )
}
