import React,{useState,useEffect} from 'react'
import { NavBar,Ellipsis} from 'antd-mobile'
import { useNavigate } from 'react-router-dom'
import style from './style.module.css'

import axios from 'axios'
axios.defaults.baseURL = 'http://127.0.0.1:3000/HDD'
export default function Consultation() {
  const navigate = useNavigate()
  const [list_U,setList_U] = useState([])

  const getList_U = async()=>{
    let {data} = await axios.post('/DocList')
    console.log(data);
    setList_U(data.list)
  }
  const gotoItem = (id)=>{
    navigate(`/item?id=${id}`)
  }
  useEffect(()=>{
        getList_U()
  },[])
  return (
    <div style={{margin:'15px'}}>
       <p style={{fontSize:'20px',lineHeight:'40px',textAlign:'left',color:'#000000',marginTop:'10px'}}> 寻医问诊</p>
      <div style={{flex:'flex',textAlign:'center',marginTop:'10px',color:'#164179',lineHeight:'35px'}}>
        <div className={style.video} onClick={()=>{
          navigate(`/docList?id=${'视频联系医生'}`)
        }
        }>
          <img style={{margin:'5px 0'}} src="https://cdn7.axureshop.com/demo2024/2280786/images/%E5%AF%BB%E5%8C%BB%E9%97%AE%E8%AF%8A/u839.svg" alt="" />
          <h3 >视频医生会诊</h3>
          <p>一对一私密就医</p>
        </div>
        <div className={style.chat} onClick={()=>{
          navigate(`/docList?id=${'在线联系医生'}`)
        }}>
          <img style={{margin:'5px 0'}} src="https://cdn7.axureshop.com/demo2024/2280786/images/%E5%AF%BB%E5%8C%BB%E9%97%AE%E8%AF%8A/u844.svg" alt="" />
          <h3>在线联系医生</h3>
          <p>详细寻医问诊</p>
        </div>
      </div>
      <div className={style.bottom} onClick={()=>{
        navigate('/MapContainer')
      }}>
        <img style={{width:'100%',height:'100%',marginTop:'25px'}} src="https://cdn7.axureshop.com/demo2024/2280786/images/%E5%AF%BB%E5%8C%BB%E9%97%AE%E8%AF%8A/u911.svg" alt="" />
      </div>
      <div style={{backgroundColor:'#F7FAFF',marginTop:'10px'}}>
          <li style={{listStyle:'none',padding:'15px',display:'flex',position:'relative',margin:'5px',lineHeight:'20px',height:'20px'}}>
            <h3 style={{position:'absolute',left:0}}> 我关注的医生</h3>
            <p style={{position:'absolute',right:0}}>查看全部</p>
          </li>
          <ul style={{display:'flex'}}>
            {list_U.slice(0,4).map(item=>
              <li key={item._id} style={{listStyle:'none',flex:1,margin:'5px',textAlign:'center'}}>
                <img src={item.avatar} alt="" style={{width:'50px',borderRadius:'50%'}}/>
                {/* <p>{item.username}</p> */}
                <Ellipsis direction='end' content={item.username} />
              </li>
            )}
          </ul>
        </div>
        <div className="docU">
          <h3 style={{margin:'10px 0'}}>最近联系的医生</h3>
          {list_U.map(item=>{
            return <div key={item._id} style={{display:'flex',alignItems:'center',margin:'25px 5px'}}>
              <img src={item.avatar} alt="" style={{width:'50px',borderRadius:'50%'}}/>
              <div style={{marginLeft:'15px',lineHeight:'25px'}}>
                <h3>
                  {item.username}
                </h3>
                <p style={{color:'#999'}}>
                  {`${item.doctor_title}  .  ${item.doctor_department}`}
                  <span>{}</span>
                </p>
              </div>
            </div>
          })}
        </div>
    </div>
  )
}
