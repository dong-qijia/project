import React, { useEffect, useState } from 'react'
import { Button, Space,Ellipsis } from 'antd-mobile'
import { useNavigate } from 'react-router-dom'

import axios from 'axios'
axios.defaults.baseURL = 'http://127.0.0.1:3000/HDD'
import style from './style.module.css'

export default function Community() {
  const navigate = useNavigate()

  const [list,setList] = useState([])
  const [list_U,setList_U] = useState([])
  const getList_T = async()=>{
    
    let {data} = await axios.post('/listT')
    console.log(data);
    setList(data.list)
  }
  const getList_U = async()=>{
    
    let {data} = await axios.post('/listU')
    // console.log(data);
    setList_U(data.list)
  }
  const gotoItem = (id)=>{
    navigate(`/item?id=${id}`)
  }
  useEffect(()=>{
    
    getList_T()
    getList_U()
  },[])
  return (
    <div >
      <div className={style.header}>
        <p style={{fontSize:'20px',margin:'5px',position:'absolute',left:0}}>病友社区</p>
        <Button color='success' onClick={()=>{navigate('/H_add')}} style={{position:'absolute',right:0}}>发布</Button>
      </div>
      {/* <hr /> */}
      <div className={style.hotList}>
        <ul style={{height:'100%'}}>
          <li style={{listStyle:'none',boxSizing:'border-box',display:'flex',position:'relative',margin:'15px',lineHeight:'20px',height:'20px'}}>
            <h3 style={{position:'absolute',left:0}}> 热门推荐</h3>
            <p style={{position:'absolute',right:0}}>换一批</p>
          </li>
          {list.slice(0,3).map(item =>
              <li key={item._id} style={{listStyle:'none',display:'flex',padding:'5px',boxSizing:'border-box',height:'28%'}}>
                <div>
                  <img style={{width:'45px',borderRadius:'50%',padding:'5px'}} src={item.uid.avatar
                  } alt="" />
                </div>
                <div>
                  <p>{item.title}</p>
                  <p style={{fontSize:'14px',color:'gray',lineHeight:'40px'}}>{item.uid.username} <span style={{marginLeft:'15px'}}>{`评论   ${item.comment!==undefined?item.comment:0}`}</span></p>
                </div>
              </li>
          )}
        </ul>
          
      </div>
      <div className={style.userList}>
          <li style={{listStyle:'none',padding:'15px',display:'flex',position:'relative',margin:'5px',lineHeight:'20px',height:'20px'}}>
            <h3 style={{position:'absolute',left:0}}> 最近私聊</h3>
            <p style={{position:'absolute',right:0}}>查看全部</p>
          </li>
          <ul style={{display:'flex'}}>
            {list_U.slice(0,4).map(item=>
              <li key={item._id} style={{listStyle:'none',flex:1,margin:'5px',textAlign:'center'}}>
                <img src={item.avatar} alt="" style={{width:'70%',borderRadius:'50%'}}/>
                <p>{item.nickname}</p>
              </li>
            )}
          </ul>
      </div>
      <div className={style.textList}>
          <h3>病友分享</h3>
          {list.map(item=>{
    return  <div style={{display:'flex',padding:'10px'}} key={item._id} onClick={()=>{gotoItem(item._id)}}>
              <div className={style.textleft}>
                  <div>
                    <Ellipsis direction='end' rows={3} content={item.content} />
                  </div>
                  <div >
                    <p style={{height:'20px',lineHeight:'20px',margin:'10px 0'}}>
                      <img src={item.uid.avatar
                      } style={{width:'25px',borderRadius:'50%',verticalAlign:'middle'}
                      
                      } alt="" />
                      <span>{item.uid.username   }</span>
                      <span style={{margin:'0 10px'}}>{`评论${item.comment==undefined?0:item.comment}`}</span>
                      <span>{"item.date"}</span>
                    </p>
                  </div>
              </div>
              <div className={style.imgright}>  
                <img src={item.img} style={{width:'100%',height:'100%'}} alt="" />
              </div>
            </div>
          })}
      </div>
    </div>
  )
}
