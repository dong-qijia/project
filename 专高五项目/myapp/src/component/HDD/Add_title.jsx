import React,{useEffect, useState} from 'react'

import Keyboard from 'react-simple-keyboard';
import 'simple-keyboard/build/css/index.css';


import { NavBar, Button ,TextArea,ImageUploader,Toast} from 'antd-mobile'
import { Upload } from 'antd';
import ImgCrop from 'antd-img-crop';
import { useNavigate } from 'react-router-dom'
import style from './style.module.css'
import './H_Css.css'

import axios from 'axios';
axios.defaults.baseURL = 'http://127.0.0.1:3000/HDD'
export default function Add_title() {
  const navigate = useNavigate()
  const back = () =>{
    navigate(-1)
  }
  const [value, setValue] = useState('')
  const [img,setImg] = useState('')
  const time = new Date
  // console.log(time.toLocaleString().split(' ')[0]);
  
  const add = async()=>{
    let obj = {
      title:value[0,10],
      content:value,
      img:img.data,
      time:time.toLocaleString().split(' ')[0]
    }
    let {data} = await axios.post('/addT',obj)
    console.log(data);
  }
  const right = (
    <Button color='success' onClick={add}>发布</Button>
  )

  // const  onChange = (input) => {
  //   console.log("Input changed", input);
  // }
 
  const onKeyPress = (button) => {
    console.log("Button pressed", button);
  }
  const [fileList, setFileList] = useState([
    // {
    //   uid: '-1',
    //   name: 'image.png',
    //   status: 'done',
    //   url: '',
    // },
  ]);

  const onChange = ({ fileList: newFileList }) => {
    console.log(newFileList[0].response);
    setImg(newFileList[0].response)
    setFileList(newFileList);
  };
  const onPreview = async (file) => {
    console.log(file)
    console.log(333333333);
    
    let src = file.url;
    if (!src) {antd-img-crop
      src = await new Promise((resolve) => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj);
        reader.onload = () => resolve(reader.result);
      });
    }
    const image = new Image();
    image.src = src;
    const imgWindow = window.open(src);
    imgWindow?.document.write(image.outerHTML);
  };
  
  return (
    <div style={{height:'98vh'}}>
       <NavBar onBack={back}  right={right} >发布帖子</NavBar>
       <TextArea className = {style.TextArea}
          placeholder='您的抗糖经验，对他人也很有用哦...'
          showCount
          maxLength={30}
          value={value}
          onChange={val => {
            setValue(val)
          }}
          
        />
         <ImgCrop rotationSlider >
            <Upload
              action="http://127.0.0.1:3000/HDD/upload"
              listType="picture-card"
              fileList={fileList}
              onChange={onChange}
              onPreview={onPreview}
              maxCount={1}
            >
                {fileList.length < 5 && '+'}

            </Upload>
          </ImgCrop>

        {/* <Keyboard 
        onChange={input =>
          onChange(input)}
        onKeyPress={button =>
          onKeyPress(button)}
      /> */}
    </div>
  )
}
