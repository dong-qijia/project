import React, { useState, useEffect, useMemo, useContext } from 'react'
import { Outlet, useNavigate } from 'react-router-dom'
import { Badge, TabBar } from 'antd-mobile'
import { UserAddOutline, AppOutline, UserCircleOutline} from 'antd-mobile-icons'
import {WechatWorkOutlined ,ShoppingCartOutlined } from '@ant-design/icons';
import style from './style.module.css'
export default function Dqj_Home() {
  let navigate = useNavigate()
  const tabs = [
    {
        key: 'home',
        title: '首页',
        icon: <AppOutline onClick={()=>{
            navigate('/User_Home/shou')
        }}/>,
    },
    {
        key: 'Community',
        title: '社区',
        icon: <WechatWorkOutlined  onClick={()=>{
            navigate('/User_Home/Community')
        }}/>,
        badge: '5',
    },
    {
        key: 'Consultation',
        title: '问诊',
        icon: <UserAddOutline onClick={()=>{
          navigate('/User_Home/Consultation')
        }}/>,
        badge: '5',
    },
    {
        key: 'Shop',
        title: '商城',
        icon: <ShoppingCartOutlined  onClick={()=>{
          navigate('/User_Home/Shop')
        }}/>,
        badge: '5',
    },
    {
      key: 'message',
      title: '我的',
      icon: <UserCircleOutline onClick={()=>{
        navigate('/User_Home/mys')
      }}/>,
    },
  ]
  let [activeKey, setActiveKey] = useState('home')

  return (
    <div>
      <Outlet />
      <TabBar className={style.tab}>
        {tabs.map(item => (
          <TabBar.Item key={item.key} icon={item.icon} title={item.title} />
        ))}
      </TabBar>

    </div>
  )
}
