import React, { useEffect, useState } from 'react'
import { NavBar, Button ,TextArea,ImageUploader} from 'antd-mobile'
import { useLocation, useNavigate, useSearchParams } from 'react-router-dom';

import axios from 'axios';
axios.defaults.baseURL = 'http://127.0.0.1:3000/HDD'
export default function Item() {
  const Navigate = useNavigate()

  let [id] =  useSearchParams()
  let tid = id.get('id')

  const back = ()=>{
      Navigate(-1)
  }
  const [item,setItem] = useState({})
  const [content,setContent] = useState('')

  const getItem = async()=>{
    let {data} = await axios.post('/item',{tid})
    console.log(data.item);
    setItem(data.item)
    setContent(data.item.content)
  }
  useEffect(()=>{
    getItem()
  },[])
  return (
    <div>
        <NavBar onBack={back} >{item.title}</NavBar>
        <div style={{padding:'20px'}}>
          <div className="userbox">

          </div>
          <div>
            <div style={{lineHeight:'30px',fontSize:'18px'}}>
              {content.slice(0,46)}
            </div>
            <div>
              <img src={item.img} alt="" style={{width:'100%',margin:'30px 0'}} />
            </div>
            <div style={{lineHeight:'30px',fontSize:'18px'}}>
            {content.slice(46)}
              
            </div>
          </div>
        </div>
        
    </div>
  )
}
