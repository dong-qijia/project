import React, { useState ,useEffect} from 'react'
import { NavBar,SearchBar} from 'antd-mobile'
import { useNavigate, useSearchParams } from 'react-router-dom'
import { VideoOutline } from 'antd-mobile-icons'
import style from './style.module.css'

import axios from 'axios'
axios.defaults.baseURL = 'http://localhost:3000/HDD'
export default function DocList() {
    let [id] =  useSearchParams()
    let title = id.get('id')
    const navigate = useNavigate()
    const back = () =>{
        navigate(-1)
      }

    const [list, setList] = useState([])
    const getList = async() =>{
      let {data} = await axios.post('/DocList')
      console.log(data.list);
      
      setList(data.list)
    }
    useEffect(()=>{
      getList()
    },[])
  return (
    <div>
        <NavBar onBack={back} >{title}</NavBar>
        <SearchBar placeholder='请输入内容' style={{margin:'10px'}}/>
        <div style={{display:'flex',flexWrap:'wrap',marginTop:'10px'}}>
        {
            list.map(item => <div key={item._id} style={{width:'45%',background:'#eee',height:'150px',padding:'10px',boxSizing:'border-box',margin:'7% 2.5%',position:'relative'}} onClick={()=>{
              navigate(`/Consultation_chat`,{state:{item:{_id:"66d458f9fcde96eddecc55cc",doctor_id:item._id}}})
            }}>
 
              <div style={{position:'absolute',top:'-20px',left:'0',width:'40%',height:'25px',lineHeight:'25px',background:'#EC808D',color:'#fff',fontSize:'12px',textAlign:'center',borderRadius:'5px', display:item.Professional_Experience?'none':'block'}}> <VideoOutline />义诊中</div>
              <div style={{position:'absolute',top:'-20px',right:'0',width:'40%',height:'25px',lineHeight:'25px',background:'#1AD1B3',color:'#fff',fontSize:'12px',textAlign:'center',borderRadius:'5px',display:item.Professional_Experience?'block':'none'}}> 空闲中</div>
              <div style={{margin:'10px 0'}}>
                <div style={{display:'flex',alignItems:'center'}}>
                  <div>
                    <img src={item.avatar} alt="" className={item.Professional_Experience? style.avatar:style.avatar1} />
                  </div>
                  <div style={{fontSize:'12px',color:'#999',marginLeft:'10px'}}>
                    <p>{item.username}</p>
                    <p>{item.doctor_title}</p>
                  </div>
                </div>
              </div>
            </div>)
        }
        </div>
    </div>
  )
}
