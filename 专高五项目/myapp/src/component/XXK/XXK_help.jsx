import React, { useEffect, useState } from 'react'
import { NavBar, Space, Toast, Input } from 'antd-mobile'
import { useNavigate } from 'react-router-dom'
import './XXK.css'


export default function XXK_help() {
    const navigate = useNavigate()


    const back = () =>
        Toast.show({
            content: navigate(-1),
            duration: false,
        })

    return (
        <div>
            <NavBar onBack={back}>
                帮助中心
            </NavBar>
            <div className='Home_Modular'
                onClick={() => {
                    navigate('/XXK_helpname')
                }}>
                <p style={{fontSize: '130%'}}>
                    <span>如何修改用户名?</span>
                </p>
                <p>〉</p>
            </div>
            <div className='Home_Modular'
                onClick={() => {
                    navigate('')
                }}>
                <p style={{fontSize: '130%'}}>
                    <span>如何修改密码?</span>
                </p>
                <p>〉</p>
            </div>
            <div className='Home_Modular'
                onClick={() => {
                    navigate('')
                }}>
                <p style={{fontSize: '130%'}}>
                    <span>如何进行聊天?</span>
                </p>
                <p>〉</p>
            </div>
            <div className='Home_Modular'
                onClick={() => {
                    navigate('')
                }}>
                <p style={{fontSize: '130%'}}>
                    <span>如何排泄?</span>
                </p>
                <p>〉</p>
            </div>
            <div className='Home_Modular'
                onClick={() => {
                    navigate('')
                }}>
                <p style={{fontSize: '130%'}}>
                    <span>如何吃饭?</span>
                </p>
                <p>〉</p>
            </div>
            <div className='Home_Modular'
                onClick={() => {
                    navigate('')
                }}>
                <p style={{fontSize: '130%'}}>
                    <span>如何吃完了就去拉?</span>
                </p>
                <p>〉</p>
            </div>
            <div className='Home_Modular'
                onClick={() => {
                    navigate('')
                }}>
                <p style={{fontSize: '130%'}}>
                    <span>如何拉完了再去吃?</span>
                </p>
                <p>〉</p>
            </div>
            <div className='Home_Modular'
                onClick={() => {
                    navigate('')
                }}>
                <p style={{fontSize: '130%'}}>
                    <span>如何三块钱活七天?</span>
                </p>
                <p>〉</p>
            </div>
        </div>
    )
}
