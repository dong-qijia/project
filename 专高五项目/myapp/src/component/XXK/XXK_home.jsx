import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { Button, Mask, Space, Modal } from 'antd-mobile'
import { UserCircleOutline, SetOutline, CheckShieldOutline, UploadOutline, BillOutline, VideoOutline, QuestionCircleOutline } from 'antd-mobile-icons'
import './XXK.css'
import axios from 'axios'
export default function Home() {
  const navigate = useNavigate()
  const [visible, setVisible] = useState(false)//蒙层
  const [visible1, setVisible1] = useState(true) //验证蒙层

  const [box, setBox] = useState([])//人员数据

  useEffect(() => {
    getdoctor()
  }, [])


  const getdoctor = async () => {
    let user = JSON.parse(sessionStorage.getItem('user'))
    let res = await axios.get('/XXK/getdoctor?id=' + user._id)
    setBox(res.data.list[0])
    setVisible1(res.data.list[0].authentication)
  }

  const upauthentication = async () => {
    // setVisible(true)//打开蒙层
    let res = await axios.post('/XXK/upauthentication', { id: box._id, authentication: !visible1 })
    navigate('/login');
    //清除缓存localStorage中的user
    localStorage.removeItem('user')
  }

  const WithContent = () => {
    return (
      <>
        <Mask visible={visible1} onMaskClick={() => setVisible(false)}>
          <div className='overlayContent1'>
            <div className='overlayContent2'>
              <p>请进行身份验证</p>
              <p>
                <button onClick={() => {
                  navigate('/XXK_renlian');
                }}>确定</button>
                <button onClick={() => navigate('/Dqj_home/Dqj_homepage')}>取消</button>
              </p>
            </div>
          </div>
        </Mask>
      </>
    )
  }


  return (
    <div >
      <div className='Home_Top'>
        <div className='Home_Top1'>
          <h2>我的</h2>
          <p>
            <UserCircleOutline style={{ fontSize: '200%' }} />
            <UploadOutline style={{ fontSize: '180%', color: 'red' }} onClick={() => setVisible(true)} />
          </p>
        </div>
        <WithContent />
        <Mask visible={visible} onMaskClick={() => setVisible(false)}>
          <div className='overlayContent1'>
            <div className='overlayContent2'>
              <p>退出后将无法接受到消息确定退出?</p>
              <p>
                <button onClick={() => {
                  upauthentication()
                }}>确定</button>
                <button onClick={() => setVisible(false)}>取消</button>
              </p>
            </div>
          </div>
        </Mask>

        <div>
          <div className='Home_Top2' onClick={() => { navigate('/XXK_personal') }}>
            <img src={box.avatar}></img>
            <div className='Home_Top2_div'>
              <p className='Home_Top2_p1'>
                <span>{box.doctor_name}</span>
                <span>{box.doctor_title}</span>
              </p>
              <p className='Home_Top2_p2'>{box.doctor_department}</p>
            </div>
            <p className='Home_Top2_p3'>〉</p>
          </div>

        </div>
        <div>
          <div className='Home_Modular'
            onClick={() => {
              navigate('/XXK_prove')
            }}>
            <p>
              <span style={{ marginRight: 12, fontSize: 24, color: 'blue' }}><CheckShieldOutline /></span>
              <span>我的认证</span>
            </p>
            <p>〉</p>
          </div>

          <div className='Home_Modular'
            onClick={() => {
              navigate('/XXK_income')
            }}>
            <p>
              <span style={{ marginRight: 12, fontSize: 24, color: 'blue' }}><BillOutline /></span>
              <span>我的收入</span>
            </p>
            <p>〉</p>
          </div>
          <div className='Home_Modular'
            onClick={() => {
              navigate('/XXK_bank')
            }}>
            <p>
              <span style={{ marginRight: 12, fontSize: 24, color: 'blue' }}><VideoOutline /></span>
              <span>我的银行卡</span>
            </p>
            <p>〉</p>
          </div>
          <div className='Home_Modular'
            onClick={() => {
              navigate('/XXK_help')
            }}>
            <p>
              <span style={{ marginRight: 12, fontSize: 24, color: 'blue' }}><QuestionCircleOutline /></span>
              <span>帮助中心</span>
            </p>
            <p>〉</p>
          </div>
          <div className='Home_Modular'
            onClick={() => {
              navigate('/XXK_setup')
            }}>
            <p>
              <span style={{ marginRight: 12, fontSize: 24, color: 'blue' }}><SetOutline /></span>
              <span>个人设置</span>
            </p>
            <p>〉</p>
          </div>
        </div>
      </div>
    </div>
  )
}
