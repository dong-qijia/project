import React, { useState,useEffect } from 'react'
import { NavBar, Space, Toast, Cascader, Button, Image } from 'antd-mobile'
import { CheckShieldFill, CloseCircleFill } from 'antd-mobile-icons'
import { useNavigate } from 'react-router-dom'
import './XXK.css'
import axios from 'axios'

export default function XXK_prove() {
    const navigate = useNavigate()
    const [isprove, setIsprove] = useState(true)
    const [box, setBox] = useState('')

    const back = () =>
        Toast.show({
            content: navigate(-1),
            duration: false,
        })

    useEffect(() => {
        getincome()
    }, [])

    const getincome = async () => {
        let id = JSON.parse(sessionStorage.getItem('user'))._id
        let res = await axios.get('/XXK/getdoctor?id=' + id)
        setBox(res.data.list[0])
        setIsprove(res.data.list[0].identity)
    }




    return (
        <div>
            <NavBar onBack={back}>
                医师认证
            </NavBar>
            <div className='provetop1'
                style={{
                    display: isprove ? 'block' : 'none'
                }}
            >
                <CheckShieldFill style={{
                    fontSize: '150%',
                    color: 'green',
                    marginBottom: '2%',
                }} />
                <p>医师认证成功</p>
            </div>
            <div className='provetop1'
                style={{
                    display: !isprove ? 'block' : 'none'
                }}
            >
                <CloseCircleFill style={{
                    fontSize: '150%',
                    color: 'red',
                    marginBottom: '2%',
                }} />
                <p>未完成认证,请认证</p>
            </div>

            <div style={{
                padding: '5%',
                borderBottom: '12px solid rgb(241, 241, 241) ',
            }}>
                <p style={{
                    fontSize: '150%',
                    marginBottom: '3%',
                }}>医生头像</p>
                <Image src={box.avatar} width={100} height={100} />
            </div>

            <div style={{
                padding: '5%',
                borderBottom: '12px solid rgb(241, 241, 241) ',
                display: !isprove ? 'block' : 'none'
            }}>
                <p style={{
                    fontSize: '150%',
                    marginBottom: '3%',
                }}>身份证件(正反需上传)</p>
                <div style={{ display: 'flex' }}>
                    <Image src='/404' width={100} height={100} style={{ marginRight: '5%' }} />
                    <Image src='/404' width={100} height={100} />
                </div>
            </div>

            <div style={{
                padding: '5%',
                borderBottom: '12px solid rgb(241, 241, 241) ',
            }}>
                <p style={{
                    fontSize: '150%',
                    marginBottom: '3%',
                }}>
                    胸牌/执业证/资格证/职称证</p>
                <Image src={box.Qualifications} width={100} height={100} style={{ marginBottom: '5%' }} />
                <p>照片仅用作认证，平台会保护您的隐私，不会泄漏给第三方</p>
            </div>

            <p style={{ textAlign: 'center',margin:'auto' }}>
                <button className='prove_button' onClick={
                    () => { navigate('/XXK_upprove') }
                }>
                    修改认证
                </button>
            </p>

        </div>
    )
}
