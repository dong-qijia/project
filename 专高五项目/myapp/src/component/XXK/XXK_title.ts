export const TitleOptions = [
    {
        label: '主任医师',
        value: '主任医师',
    },
    {
        label: '副主任医师',
        value: '副主任医师',
    },
    {
        label: '主治医师',
        value: '主治医师',
    },
    {
        label: '住院医师',
        value: '住院医师',
    }
]
