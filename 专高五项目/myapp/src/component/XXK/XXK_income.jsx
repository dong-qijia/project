import React, { useState, useEffect } from 'react'
import { NavBar, Space, Toast, Tabs } from 'antd-mobile'
import { CheckShieldFill, CloseCircleFill } from 'antd-mobile-icons'
import { useNavigate, useSearchParams } from 'react-router-dom'
import './XXK.css'
import axios from 'axios'


export default function XXK_income() {
    const navigate = useNavigate()
    const [serch] = useSearchParams();
    const [list, setList] = useState([])
    const [box, setBox] = useState('')

    const back = () =>
        Toast.show({
            content: navigate(-1),
            duration: false,
        })

    useEffect(() => {
        getincome()
    }, [])

    const getincome = async () => {
        let id = JSON.parse(sessionStorage.getItem('user'))._id
        let res = await axios.post('/XXK/getprescription', { id })
        let res1 = await axios.get('/XXK/getdoctor?id='+ id )
        setBox(res1.data.list[0])
        // console.log(res1.data.list[0].price);
        setList(res.data.list)
    }

    return (
        <div>
            <NavBar onBack={back}>
                我的收入
            </NavBar>
            <p style={{
                backgroundColor: 'rgb(255, 45, 94)',
                padding: '3%',
                fontSize: '130%',
                color: 'white'
            }}>本月收入</p>
            <div className='income_top'>
                ￥{list.length*box.price}
            </div>
            <div style={{
                padding: '3%',
                backgroundColor: 'rgb(241, 241, 241)',
            }}
            >
                <p style={{
                    fontSize: '130%',
                    borderLeft: '5px solid rgb(0, 132, 255)',
                    paddingLeft: '3%',
                }}>累计面板</p>
            </div>
            <div className='income_cumulative'>
                <p>
                    <span>订单总数</span>
                    <span style={{
                        fontSize: '180%',
                    }}>{list.length}</span>
                </p>
                <span style={{ borderRight: '1px solid rgb(201, 201, 201)', height: '5vh', width: '1px' }}></span>
                <p>
                    <span>完成订单</span>
                    <span style={{
                        fontSize: '180%',
                    }}>{list.length}</span>
                </p>
            </div>
            <div style={{
                padding: '3%',
                backgroundColor: 'rgb(241, 241, 241)',
            }}
            >
                <p style={{
                    fontSize: '130%',
                    borderLeft: '5px solid rgb(0, 132, 255)',
                    paddingLeft: '3%',
                }}>累计面板</p>
            </div>
            {/* <div className='income_listtop'>
                <p>全部</p>
                <p>进行中</p>
                <p>已结束</p>
            </div> */}
            {list.map(Item => {
                return <div key={Item._id} className='income_list'>
                    <div>
                        <span>{Item.username}</span>
                        <span style={{
                            fontSize: '80%',
                            color: 'rgb(201, 201, 201)',
                        }}>{Item.usersex}</span>
                        <span style={{
                            fontSize: '80%',
                            color: 'rgb(201, 201, 201)',
                        }}>{Item.userage}</span>
                        <p>{Item.suoshuyiyuan}</p>
                    </div>
                    <p>
                        <span style={{
                            marginRight: '10%',
                        }}>诊断:{Item.diagnosis}</span>
                        <span style={{
                            color: 'red'
                        }}>￥{box.price}</span>
                    </p>
                </div>
            })}


        </div >
    )
}
