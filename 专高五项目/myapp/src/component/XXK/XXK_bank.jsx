import React, { useEffect, useState } from 'react'
import { NavBar, Space, Toast, Input } from 'antd-mobile'
import { useNavigate } from 'react-router-dom'
import './XXK.css'


export default function XXK_bank() {
    const navigate = useNavigate()
    const [bankip, setbankip] = useState('*******12365') //银行账号
    const [bankname, setbankname] = useState('大棍子银行') //银行名称
    const [nameip, setnameip] = useState('大棍子银行') //开户行名称
    const [name, setname] = useState('**佬')  //持卡人姓名
    const [ip, setip] = useState('123***********5489')   //身份证号

    const [bankok, setbankok] = useState(true) //银行账号


    const back = () =>
        Toast.show({
            content: navigate(-1),
            duration: false,
        })

    return (
        <div>
            <NavBar onBack={back}>
                银行卡
            </NavBar>
            <div style={{
                width: '100%',
                height: '100vh',
                backgroundColor: 'rgb(241, 241, 241)',
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'space-between'
            }}>

                <div className='bank_inp'>
                    <p>
                        <span>银行卡号</span>
                        <input placeholder="请输入银行卡号" value={bankip}
                            onChange={(e) => {
                                setbankip(e.target.value)
                            }}
                        />
                    </p>
                    <p>
                        <span>银行名称</span>
                        <input placeholder="请输入银行名称" value={bankname}
                            onChange={(e) => {
                                setbankname(e.target.value)
                            }}
                        />
                    </p>
                    <p>
                        <span>开户行名称</span>
                        <input placeholder="请输入开户行名称" value={nameip}
                            onChange={(e) => {
                                setnameip(e.target.value)
                            }}
                        />
                    </p>
                    <p>
                        <span>卡主姓名</span>
                        <input placeholder="卡主姓名必须与注册姓名一致" value={name}
                            onChange={(e) => {
                                setname(e.target.value)
                            }}
                        />
                    </p>
                    <p>
                        <span>身份证号码</span>
                        <input placeholder="请输入身份证号码" value={ip}
                            onChange={(e) => {
                                setip(e.target.value)
                            }}
                        />
                    </p>
                </div>
                <p style={{
                    textAlign: 'center',
                    backgroundColor: 'white',
                    display: !bankok ? 'block' : 'none',
                }}>
                    <button className='prove_button'
                        onClick={
                            () => {
                                if (nameip === '' || name === '' || ip === '' || bankip === '' || bankname === '' || bankip === '') {
                                    alert('请输入完整信息')
                                } else {
                                    localStorage.setItem('name', name)
                                    if (name !== localStorage.getItem('name')) {
                                        alert('姓名必须与注册姓名一致')
                                    } else {
                                        alert('提交成功')
                                        setbankok(true)
                                    }
                                }
                            }
                        }>
                        提交
                    </button>


                </p>
                <p style={{
                    textAlign: 'center',
                    backgroundColor: 'white',
                    display: bankok ? 'block' : 'none',
                }}>
                    <button className='prove_button' style={{
                        width: '40%',
                        marginRight: '10%',
                        backgroundColor: 'white',
                        border: '1px solid #000',
                    }}>
                        删除
                    </button>
                    <button className='prove_button' style={{
                        width: '40%',
                    }}
                    onClick={()=>{
                        setbankok(false)
                    }}
                    >
                        修改
                    </button>
                </p>

            </div>
        </div>
    )
}
