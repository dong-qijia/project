export const HospitalOptions = [
  {
    label: '浙江',
    value: '浙江',
    children: [
      {
        label: '杭州',
        value: '杭州',
        children: [
          {
            label: '杭州一号医院',
            value: '杭州一号医院',
          },
          {
            label: '杭州二号医院',
            value: '杭州二号医院',
          },
          {
            label: '杭州三号医院',
            value: '杭州三号医院',
            disabled: true,
          },
        ],
      },
      {
        label: '温州',
        value: '温州',
        children: [
          {
            label: '温州一号医院',
            value: '温州一号医院',
          },
          {
            label: '温州二号医院',
            value: '温州二号医院',
            disabled: true,
          },
          {
            label: '温州三号医院',
            value: '温州三号医院',
          },
        ],
      },
      {
        label: '宁波',
        value: '宁波',
        children: [
          {
            label: '宁波一号医院',
            value: '宁波一号医院',
          },
          {
            label: '宁波二号医院',
            value: '宁波二号医院',
          },
          {
            label: '宁波三号医院',
            value: '宁波三号医院',
          },
        ],
      },
    ],
  },
  {
    label: '河北',
    value: '河北',
    children: [
      {
        label: '石家庄',
        value: '石家庄',
        children: [
          {
            label: '石家庄第一医院',
            value: '石家庄第一医院',
          },
          {
            label: '石家庄第二医院',
            value: '石家庄第二医院',
          },
          {
            label: '石家庄第三医院',
            value: '石家庄第三医院',
            disabled: true,
          },
        ],
      },
      {
        label: '保定',
        value: '保定',
        children: [
          {
            label: '保定第一医院',
            value: '保定第一医院',
          },
          {
            label: '保定第二医院',
            value: '保定第二医院',
            disabled: true,
          },
          {
            label: '保定第三医院',
            value: '保定第三医院',
          },
        ],
      },
      {
        label: '邯郸',
        value: '邯郸',
        children: [
          {
            label: '邯郸第一医院',
            value: '邯郸第一医院',
          },
          {
            label: '邯郸第二医院',
            value: '邯郸第二医院',
          },
          {
            label: '邯郸第三医院',
            value: '邯郸第三医院',
          },
        ],
      },
    ],
  },
  {
    label: '安徽',
    value: '安徽',
    children: [
      {
        label: '合肥',
        value: '合肥',
        children: [
          {
            label: '合肥一号医院',
            value: '合肥一号医院',
          },
          {
            label: '合肥二号医院',
            value: '合肥二号医院',
          },
          {
            label: '合肥三号医院',
            value: '合肥三号医院',
          },
        ],
      },
      {
        label: '芜湖',
        value: '芜湖',
        children: [
          {
            label: '芜湖一号医院',
            value: '芜湖一号医院',
          },
          {
            label: '芜湖二号医院',
            value: '芜湖二号医院',
          },
          {
            label: '芜湖三号医院',
            value: '芜湖三号医院',
          },
        ],
      },
    ],
  },
  {
    label: '江苏',
    value: '江苏',
    children: [
      {
        label: '南京',
        value: '南京',
        children: [
          {
            label: '南京一号医院',
            value: '南京一号医院',
          },
          {
            label: '南京二号医院',
            value: '南京二号医院',
          },
          {
            label: '南京三号医院',
            value: '南京三号医院',
          },
        ],
      },
      {
        label: '苏州',
        value: '苏州',
        children: [
          {
            label: '苏州一号医院',
            value: '苏州一号医院',
          },
          {
            label: '苏州二号医院',
            value: '苏州二号医院',
          },
          {
            label: '苏州三号医院',
            value: '苏州三号医院',
          },
        ],
      },
    ],
  },
]
