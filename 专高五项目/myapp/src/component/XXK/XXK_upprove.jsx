import React, { useState,useEffect } from 'react'
import { NavBar, Toast } from 'antd-mobile'
import { useNavigate } from 'react-router-dom'
import './XXK.css'
import { PlusOutlined } from '@ant-design/icons';
import { Upload, Image } from 'antd';
import axios from 'axios';

export default function XXK_upprove() {
    const navigate = useNavigate()
    const [isprove, setIsprove] = useState(false)
    const [box, setBox] = useState()
    const back = () =>
        Toast.show({
            content: navigate(-1),
            duration: false,
        })


    useEffect(() => {
        getincome()
    }, [])

    const getincome = async () => {
        let id = JSON.parse(sessionStorage.getItem('user'))._id
        let res = await axios.get('/XXK/getdoctor?id=' + id)
        setBox(res.data.list[0])
    }


    const getBase64 = (file) =>
        new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = (error) => reject(error);
        });

    const [previewOpen, setPreviewOpen] = useState(false);
    const [previewImage, setPreviewImage] = useState('');
    const [fileList, setFileList] = useState([]);
    const handlePreview = async (file) => {
        if (!file.url && !file.preview) {
            file.preview = await getBase64(file.originFileObj);
        }
        setPreviewImage(file.url || file.preview);
        setPreviewOpen(true);
    };
    const handleChange = ({ fileList: newFileList }) => setFileList(newFileList);
    const uploadButton = (
        <button
            style={{
                border: 0,
                background: 'none',
            }}
            type="button"
        >
            <PlusOutlined />
            <div
                style={{
                    marginTop: 8,
                }}
            >
                Upload
            </div>
        </button>
    );
    return (
        <div>
            {previewImage && (
                <Image
                    wrapperStyle={{
                        display: 'none',
                    }}
                    preview={{
                        visible: previewOpen,
                        onVisibleChange: (visible) => setPreviewOpen(visible),
                        afterOpenChange: (visible) => !visible && setPreviewImage(''),
                    }}
                    src={previewImage}
                />
            )}
            <NavBar onBack={back}>
                医师认证
            </NavBar>
            <div style={{
                padding: '3%',
                backgroundColor: 'rgb(241, 241, 241)',
            }}
            >
                <p style={{
                    fontSize: '100%',
                    borderLeft: '5px solid rgb(0, 132, 255)',
                    paddingLeft: '3%',
                }}>请如实上传您的资料</p>
            </div>

            <div style={{
                padding: '5%',
                borderBottom: '12px solid rgb(241, 241, 241) ',
            }}>
                <p style={{
                    fontSize: '150%',
                    marginBottom: '3%',
                }}>医生头像</p>
                <span>示例图</span>
                <div style={{ display: 'flex' }}>
                    <Image src='https://img2.baidu.com/it/u=3701794891,501559915&fm=253&fmt=auto&app=138&f=JPEG?w=475&h=478' width={100} height={100} style={{ marginRight: '5%' }} />
                    <Upload
                        action='http://localhost:3000/XXK/upload'
                        listType="picture-card"
                        fileList={fileList}
                        onPreview={handlePreview}
                        onChange={handleChange}
                    >
                        {fileList.length >= 8 ? null : uploadButton}
                    </Upload>
                </div>

            </div>
            <div style={{
                padding: '5%',
                borderBottom: '12px solid rgb(241, 241, 241) ',
            }}>
                <p style={{
                    fontSize: '150%',
                    marginBottom: '3%',
                }}>身份证件(正反需上传)</p>
                <span>示例图</span>
                <div style={{ display: 'flex' }}>
                    <Image src='https://img0.baidu.com/it/u=520399806,12707092&fm=253&fmt=auto&app=138&f=JPG?w=499&h=315' width={100} height={100} style={{ marginRight: '5%' }} />
                    <Upload
                        action="http://localhost:3000/XXK/upload"
                        listType="picture-card"
                        fileList={fileList}
                        onPreview={handlePreview}
                        onChange={handleChange}
                    >
                        {fileList.length >= 8 ? null : uploadButton}
                    </Upload>
                </div>
            </div>

            <div style={{
                padding: '5%',
                borderBottom: '12px solid rgb(241, 241, 241) ',
            }}>
                <p style={{
                    fontSize: '150%',
                    marginBottom: '3%',
                }}>
                    胸牌/执业证/资格证/职称证，至少上传一项</p>
                <span>示例图</span>
                <div style={{ display: 'flex', marginBottom: '5%' }}>
                    <Image src='https://img1.baidu.com/it/u=2281096286,614213895&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=354' width={100} height={100} style={{ marginRight: '5%' }} />
                    <Upload
                        action="http://localhost:3000/XXK/upload"
                        listType="picture-card"
                        fileList={fileList}
                        onPreview={handlePreview}
                        onChange={handleChange}
                    >
                        {fileList.length >= 8 ? null : uploadButton}
                    </Upload>
                </div>
                <p>照片仅用作认证，平台会保护您的隐私</p>
            </div>
            <p style={{ textAlign: 'center' }}>
                <button className='prove_button' onClick={
                    () => { navigate(-1) }
                }>
                    确认认证
                </button>
            </p>

        </div>
    )
}
