import React, { useState } from 'react'
import { NavBar, Space, Toast, Switch } from 'antd-mobile'
import { useNavigate } from 'react-router-dom'
import { UnlockOutline } from 'antd-mobile-icons'
import './XXK.css'
import axios from 'axios'

export default function XXK_uppsd() {
    const navigate = useNavigate()
    const [color, setColor] = useState(false)
    const [color2, setColor2] = useState(false)
    const [psd, setPsd] = useState('')
    const [newpsd, setNewpsd] = useState('')


    const back = () =>
        Toast.show({
            content: navigate(-1),
            duration: false,
        })


    const confirm = async () => {
        let id = JSON.parse(sessionStorage.getItem('user'))._id
        if (psd == '' || newpsd == '') {
            Toast.show({
                content: '密码不能为空',
                duration: 1000,
            })
        } else {
            if (psd === newpsd) {
                let res = await axios.post('/XXK/uppsd', { id,psd })
                navigate(-1)
                Toast.show({
                    content: '修改成功',
                    duration: 1000,
                })
            } else {
                Toast.show({
                    content: '两次密码不一致',
                    duration: 1000,
                })
            }
        }
    }

    return (
        <div>
            <NavBar onBack={back}>
                更换手机号
            </NavBar>
            <div>
                <div className='XXK_upphone2' style={{
                    border: color ? '1px solid blue' : '1px solid #ccc',
                    marginTop: '20px',
                }}>
                    <div>
                        <UnlockOutline style={{
                            color: color ? 'blue' : ''
                        }} />
                        {/* 获取焦点事件 */}
                        <input type="text" placeholder='请输入新密码'
                            onFocus={() => { setColor(true) }}
                            onBlur={() => { setColor(false) }}
                            value={psd}
                            onChange={(e) => setPsd(e.target.value)}
                        />
                    </div>
                </div>

                <div className='XXK_upphone2' style={{
                    border: color2 ? '1px solid blue' : '1px solid #ccc',
                }}>
                    <div>
                        <UnlockOutline style={{
                            color: color2 ? 'blue' : ''
                        }} />
                        {/* 获取焦点事件 */}
                        <input type="text" placeholder='请再次输入密码'
                            onFocus={() => { setColor2(true) }}
                            onBlur={() => { setColor2(false) }}
                            value={newpsd}
                            onChange={(e) => setNewpsd(e.target.value)}
                        />
                    </div>
                </div>
                <p style={{ textAlign: 'center' }}>
                    <button className='prove_button'
                        onClick={
                            () => { confirm() }
                        }>
                        确认修改
                    </button>
                </p>
            </div>
        </div>
    )
}
