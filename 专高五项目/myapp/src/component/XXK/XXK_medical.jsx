import React, { useEffect, useState } from 'react'
import { NavBar, Space, Toast, Input } from 'antd-mobile'
import { useNavigate } from 'react-router-dom'
import './XXK.css'

export default function XXK_medical() {

    const navigate = useNavigate()

    const [inp, setInp] = useState('')
    const [visible, setVisible] = useState(true)
    const [num, setNum] = useState(0)
    const [num1, setNum1] = useState(false)

    const right = (
        <div style={{ fontSize: 24 }}>
            <Space style={{ '--gap': '13px' }}>
                <button style={{
                    fontSize: '80%',
                    color: visible ? 'rgb(220, 220, 220)' : 'blue',
                    backgroundColor: 'white',
                    border: 'none'
                }}
                    disabled={visible} onClick={() => {
                        navigate(-1)
                    }}>保存</button>
            </Space>
        </div>
    )
    const back = () =>
        Toast.show({
            content: navigate(-1),
            duration: false,
        })
    //输入值修改
    const upvisable = (e) => {
        setInp(e.target.value)
        setNum(e.target.value.length)
        if (e.target.value.length > 0 & e.target.value.length <= 150) {
            setVisible(false)
        } else {
            setVisible(true)
        }
    }

    return (
        // 灰色背景
        <div style={{ width: '100vw', height: '100vh', backgroundColor: 'rgb(240, 240, 240)' }}>
            <div style={{ backgroundColor: 'white' }}>
                <NavBar right={right} onBack={back}>
                    从医经历
                </NavBar>
                <div style={{
                    backgroundColor: 'white',
                    margin: '7%',
                    fontSize: '50%',
                    height: '25vh',
                }}>
                    {/* 开启换行 */}
                    <textarea placeholder='请填写真是的中文姓名并且与身份证保持一致'
                        style={{
                            width: '100%',
                            fontSize: '18px',
                            height: '80%',
                            border: '1px solid #ccc',
                        }}
                        value={inp}
                        onChange={(e) => upvisable(e)}
                    />
                    <p style={{ fontSize: '2vh', textAlign: 'right' }}>
                        <span style={{
                            color: 'red',
                            textAlign: 'right',
                            display: num1 ? 'block' : 'none'
                        }}
                        >已达到最大字数</span>
                        <span style={{
                            color: num1 ? 'red' : 'black',
                            textAlign: 'right',
                            marginLeft: '5%  '
                        }}>{num}/150</span>
                    </p>
                </div>
            </div>

        </div>
    )
}
