import React, { useState, useEffect } from 'react'
import { NavBar, Space, Toast, Switch, NumberKeyboard, VirtualInput } from 'antd-mobile'
import { useNavigate } from 'react-router-dom'
import './XXK.css'
import axios from 'axios'

export default function XXK_setup() {
    const navigate = useNavigate()
    const right = (
        <div style={{ fontSize: 24 }}>
            <Space style={{ '--gap': '13px' }}>
                <button style={{
                    fontSize: '80%',
                    color: 'blue',
                    backgroundColor: 'white',
                    border: 'none'
                }}
                    onClick={() => {
                        navigate(-1)
                    }}>确定</button>
            </Space>
        </div>
    )
    const back = () =>
        Toast.show({
            content: navigate(-1),
            duration: false,
        })

    const [checked, setChecked] = useState()
    const [box, setBox] = useState('')
    const [isnumber, setIsnumber] = useState(false)

    useEffect(() => {
        getdoctor()
    }, [])
    // /获取数据
    const getdoctor = async () => {
        let id = JSON.parse(sessionStorage.getItem('user'))._id
        let res = await axios.get('/XXK/getdoctor?id=' + id)
        setBox(res.data.list[0])
        setChecked(res.data.list[0].Professional_Experience)
    }

    const upcheck = async () => {
        let res = await axios.post('/XXK/upcheck', { id: box._id, check: checked })
        getdoctor()
    }
    const upprice = async () => {
        let res = await axios.post('/XXK/upprice', { id: box._id, visible: visible })
        getdoctor()
    }


    const [visible, setVisible] = useState('')


    return (
        <div>
            <NavBar right={right} onBack={back}>
                设置
            </NavBar>
            <p style={{
                fontSize: '140%',
                padding: '4% 4% 4% 9%',
                backgroundColor: '#F0F0F0',
                boxSizing: 'borderBox',

            }}>服务问诊设置</p>
            <div className='setup1'>
                <p style={{
                    fontSize: '130%',
                    marginLeft: '9%',
                    boxSizing: 'borderBox',

                }}>接诊开关</p>
                <div>
                    <p style={{ display: !checked ? 'block' : 'none' }} >已关闭，展示给患者的状态为休息中</p>
                    <p style={{ display: checked ? 'block' : 'none' }} >已开启,患者可向你发起问诊</p>
                    <Switch checked={box.Professional_Experience} style={{ width: '40px' }} onChange={() => { upcheck() }} />
                </div>
            </div>
            <p style={{
                fontSize: '160%',
                padding: '1%',
                backgroundColor: '#F0F0F0',
                boxSizing: 'borderBox',
            }}></p>
            <div
                style={{
                    fontSize: '160%',
                    padding: '4% 4% 4% 9%',
                    boxSizing: 'borderBox',
                    display: 'flex',
                    justifyContent: 'space-between',
                    fontSize: '120%'
                }}>
                <span>问诊价格</span>
                <p style={{ marginRight: '10%' }}>
                    <span style={{
                        color: 'orange',
                        marginRight: '30%',
                        fontSize: '100%',
                        display: isnumber ? 'none' : 'block'
                    }}
                        onClick={() => {
                            setIsnumber(!isnumber)
                        }}
                    >￥{box.price}</span>
                </p>

                <VirtualInput
                    placeholder='请输入问诊价格'
                    style={{
                        width: '50%',
                        marginRight: '10%',
                        border: '1px solid #ccc',
                        display: isnumber ? 'block' : 'none',
                    }}
                    onBlur={() => {
                        setIsnumber(!isnumber)
                    }}
                    keyboard={<NumberKeyboard
                        confirmText='确定'
                        customKey={'.'}
                        onInput={(event)=>{
                            setVisible(visible+event)
                        }}
                        onDelete={()=>{
                            setVisible(visible.slice(0,-1))
                        }}
                        onConfirm={()=>{
                            upprice()
                        }}
                    />}
                />

            </div>
            <p style={{
                fontSize: '160%',
                padding: '4% 4% 4% 9%',
                backgroundColor: '#F0F0F0',
                boxSizing: 'borderBox',
            }}>账号设置</p>
            <div
                style={{
                    fontSize: '160%',
                    padding: '4% 4% 4% 9%',
                    boxSizing: 'borderBox',
                    display: 'flex',
                    justifyContent: 'space-between',
                    fontSize: '120%',
                    borderBottom: '2px solid #F0F0F0'
                }}
                onClick={() => { navigate('/XXK_upphone') }}
            >
                <span>修改手机号</span>
                <span>{'>'}</span>
            </div>
            <div
                style={{
                    fontSize: '160%',
                    padding: '4% 4% 4% 9%',
                    boxSizing: 'borderBox',
                    display: 'flex',
                    justifyContent: 'space-between',
                    fontSize: '120%',
                    borderBottom: '2px solid #F0F0F0'
                }}
                onClick={() => { navigate('/XXK_uppsd') }}
            >
                <span>修改密码</span>
                <span>{'>'}</span>
            </div>
        </div>
    )
}
