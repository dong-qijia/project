import React, { useState, useEffect } from 'react'
import { NavBar, Space, Toast, Switch } from 'antd-mobile'
import { useNavigate } from 'react-router-dom'
import { UserOutline, MessageOutline } from 'antd-mobile-icons'
import './XXK.css'
import axios from 'axios'

export default function XXK_upphone() {
    const navigate = useNavigate()
    const [color, setColor] = useState(false)
    const [color2, setColor2] = useState(false)
    const [cell, setCell] = useState('')

    const [upPhone, setupphone] = useState(false)
    let [isyzm, setisyzm] = useState(false)
    let [yzm, setyzm] = useState('')

    let [yzmtime, setyzmtime] = useState(60)

    const [phone, setPhone] = useState('')
    const [code, setCode] = useState('')

    const back = () =>
        Toast.show({
            content: navigate(-1),
            duration: false,
        })

    useEffect(() => {
        getbox()
    }, [])

    const getbox = async () => {
        let id = JSON.parse(sessionStorage.getItem('user'))._id
        let res = await axios.get('/XXK/getdoctor?id=' + id)
        setCell(res.data.list[0].phone.slice(0, 3) + '****' + res.data.list[0].phone.slice(7));
    }

    let getyzm = async () => {
        setisyzm(true)
        console.log(phone);
        let { data: { len } } = await axios.get(`/XXK/getyzm?phone=${phone}`)
        setyzm(len)
        let aaskdjfhjasjf = setInterval(() => {
            setyzmtime(--yzmtime)
            if (yzmtime === 0) {
                setisyzm(false)
                setyzmtime(60)
                clearInterval(aaskdjfhjasjf)
            }
        }, 1000)
    }

    // 修改手机号
    let phone_updata = async () => {
        if (code == yzm) {
            let id = JSON.parse(sessionStorage.getItem('user'))._id
            let res = await axios.post('/XXK/upphone', { id, phone })
            Toast.show({
                content: '修改成功'
            })
            let a = setTimeout(() => {
                localStorage.clear()
                navigate("/log")
            }, 1000);
            clearTimeout(a)
           window.location.reload()
        } else {
            Toast.show({
                content: '验证码错误'
            })

        }
    }
    return (
        <div>
            <NavBar onBack={back}>
                更换手机号
            </NavBar>
            {/* 手机号验证 */}
            <div style={{
                display: upPhone ? 'none' : 'block',
            }}>
                <p style={{
                    fontSize: '140%',
                    padding: '4% 4% 4% 9%',
                    boxSizing: 'borderBox',
                }}>{cell}</p>
                <div className='XXK_upphone1' style={{
                    border: color ? '1px solid blue' : '1px solid #ccc',
                }}>
                    <div>
                        <UserOutline style={{
                            color: color ? 'blue' : ''
                        }} />+86
                    </div>
                    {/* 获取焦点事件 */}
                    <input type="text" placeholder='请输入手机号'
                        onFocus={() => { setColor(true) }}
                        onBlur={() => { setColor(false) }}
                        onKeyUp={(event)=>{
                            setPhone(event.target.value)
                          }}
                    />
                </div>

                <div className='XXK_upphone2' style={{
                    border: color2 ? '1px solid blue' : '1px solid #ccc',
                }}>
                    <div>
                        <MessageOutline style={{
                            color: color2 ? 'blue' : ''
                        }} />
                        {/* 获取焦点事件 */}
                        <input type="text" placeholder='请输入验证码'
                            onFocus={() => { setColor2(true) }}
                            onBlur={() => { setColor2(false) }}
                            onKeyUp={(event)=>{
                                setCode(event.target.value)
                              }}
                        />
                    </div>
                    <button onClick={() => {
                        getyzm()
                    }}> {isyzm == true ? `${yzmtime}后重发` : '获取验证码'}</button>
                </div>
                <p style={{ textAlign: 'center' }}>
                    <button className='prove_button' onClick={
                        () => {phone_updata() }
                    }>
                       修改手机号
                    </button>
                </p>
            </div>


        </div>
    )
}
