import React, { useEffect, useState } from 'react'
import { NavBar, Space, Toast, Input } from 'antd-mobile'
import { useNavigate } from 'react-router-dom'
import './XXK.css'


export default function XXK_help() {
    const navigate = useNavigate()


    const back = () =>
        Toast.show({
            content: navigate(-1),
            duration: false,
        })

    return (
        <div>
            <NavBar onBack={back}>
                帮助中心
            </NavBar>
            <div style={{
                width: '80%%',
                margin: 'auto',
                padding: '20px',
                fontSize: '20px',
            }}>
                <h2>如果您想修改用户名</h2>
                <h3>第一步</h3>
                <p>点击个人主页顶部头像或用户名称</p>
                <h3>第二步</h3>
                <p>点击用户名，进入修改用户名页面</p>
                <h3>第三步</h3>
                <p>输入新的用户名，点击保存</p>
                
            </div>
        </div>
    )
}
