import React, { useEffect, useState } from 'react'
import { NavBar, Space, Toast, Input } from 'antd-mobile'
import { useNavigate,useLocation,useSearchParams } from 'react-router-dom'
import './XXK.css'
import axios from 'axios'
 
export default function XXK_name() {
    const navigate = useNavigate()
    const location = useLocation()
    const [serch] = useSearchParams();

    const [inp, setInp] = useState('')
    const [visible, setVisible] = useState(true)
    const [id,setid]=useState()

    useEffect(() => {
        setid(serch.get('id'))
    })
    const upname=async ()=>{
        let res=await axios.post('/XXK/upname',{id,inp})
    }

    const right = (
        <div style={{ fontSize: 24 }}>
            <Space style={{ '--gap': '13px' }}>
                <button style={{
                    fontSize: '80%',
                    color: visible ? 'rgb(220, 220, 220)' : 'blue',
                    backgroundColor: 'white',
                    border: 'none'
                }}
                    disabled={visible} onClick={() => {
                        upname()
                        navigate(-1)
                    }}>保存</button>
            </Space>
        </div>
    )
    const back = () =>
        Toast.show({
            content: navigate(-1),
            duration: false,
        })
    //输入值修改
    const upvisable = (value) => {
        setInp(value)
        if (value.length > 0) {
            setVisible(false)
        } else {
            setVisible(true)
        }
    }

    return (
        // 灰色背景
        <div style={{ width: '100vw', height: '100vh', backgroundColor: 'rgb(240, 240, 240)' }}>
            <div style={{ backgroundColor: 'white' }}>
                <NavBar right={right} onBack={back}>
                    真实姓名
                </NavBar>
                <div style={{
                    backgroundColor: 'white',
                    margin: '7%',
                    fontSize: '50%',
                    height: '5vh',
                }}>
                    <Input placeholder='请填写真是的中文姓名并且与身份证保持一致'
                        clearable style={{ '--font-size': '18px' }}
                        value={inp}
                        onChange={(value) => {
                            upvisable(value)
                        }
                        }
                    />
                </div>
            </div>

        </div>
    )
}
