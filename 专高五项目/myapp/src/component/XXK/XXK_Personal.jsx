import React, { useState, useEffect } from 'react'
import { NavBar, Space, Toast, Cascader, Button } from 'antd-mobile'
import { useNavigate } from 'react-router-dom'
import { HospitalOptions } from './XXK_hospital'
import { TitleOptions } from './XXK_title'

import './XXK.css'
import axios from 'axios'


export default function Personal() {
  const navigate = useNavigate()

  const [HospitalVisible, setHospitalVisible] = useState(false) //医院选择弹窗
  const [doctor_department, setDoctor_department] = useState('') //医生在职医院

  const [TitleVisible, setTitleVisible] = useState(false) //医生职称选择弹窗
  const [doctor_title, setDoctor_title] = useState('') //医生职称

  // /返回的回调
  const back = () =>
    Toast.show({
      content: navigate(-1),
      duration: false,
    })

  const [box, setbox] = useState([])

  useEffect(() => {
    getbox()
  }, [])

  //获取数据
  const getbox = async () => {
    let id = JSON.parse(sessionStorage.getItem('user'))._id
    let res = await axios.get('/xxk/getdoctor?id=' + id)
    setbox(res.data.list[0])
  }

  //医院选择
  const setdoctor_department = async () => {
    let id = JSON.parse(sessionStorage.getItem('user'))._id
    let res = await axios.post('/xxk/setdoctor_department', { id, doctor_department })
    getbox()
  }
  //医生职称选择
  const upDoctor_title = async () => {
    let id = JSON.parse(sessionStorage.getItem('user'))._id
    let res = await axios.post('/xxk/updoctor_title', { id, doctor_title })
    getbox()
  }

  return (
    <div style={{ width: '100vw', height: '100vh', backgroundColor: 'rgb(240, 240, 240)' }}>
      <NavBar onBack={back} style={{ backgroundColor: 'white', paddingBottom: '2%' }}>标题</NavBar>
      <div className='personal' onClick={() => {
        navigate('/XXK_name?id=' + box._id)
      }}>
        <p>
          <span style={{ color: 'red' }}>*</span>
          <span>真实名字</span>
        </p>
        <div style={{ width: '70%', display: 'flex', justifyContent: 'space-between' }}>
          <p style={{ width: '70%' }}>{box.doctor_name}</p>
          <p style={{ width: '5%' }}>〉</p>
        </div>
      </div>
      <div className='personal' onClick={() => {
        setHospitalVisible(true)
      }}>
        <p>
          <span style={{ color: 'red' }}>*</span>
          <span>就职医院</span>
        </p>
        <div style={{ width: '70%', display: 'flex', justifyContent: 'space-between' }}>
          <p style={{ width: '70%' }}>{box.doctor_department}</p>
          <p style={{ width: '5%' }}>〉</p>
        </div>
      </div>
      <div className='personal' onClick={() => {
        setTitleVisible(true)
      }}>
        <p>
          <span style={{ color: 'red' }}>*</span>
          <span>职称</span>
        </p>
        <div style={{ width: '70%', display: 'flex', justifyContent: 'space-between' }}>
          <p style={{ width: '70%' }}>{box.doctor_title}</p>
          <p style={{ width: '5%' }}>〉</p>
        </div>
      </div>

      <div style={{ height: '1vh', width: '100%', backgroundColor: '#F4F4F4', boxShadow: '0 5px 5px 0' }}></div>

      <div className='personal' onClick={() => {
        navigate('/XXK_field?id=' + box._id)
      }}>
        <p>
          <span style={{ color: 'red' }}>*</span>
          <span>擅长领域</span>
        </p>
        <div style={{ width: '70%', display: 'flex', justifyContent: 'space-between' }}>
          <p style={{ width: '70%' }}>{box.Areas_of_expertise}</p>
          <p style={{ width: '5%' }}>〉</p>
        </div>
      </div>
      <div className='personal' onClick={() => {
        navigate('/XXK_experience?id='+box._id)
      }}>
        <p>
          <span>个人经历</span>
        </p>
        <p style={{
          width: '55%', overflow: 'hidden',
          textOverflow: 'ellipsis',
          whiteSpace: 'nowrap'
        }}>{box.Biography}</p>
        <p>〉</p>
      </div >
      <div className='personal' onClick={() => {
        navigate('/XXK_medical')
      }}>
        <p>
          <span>从医经历</span>
        </p>
        <p style={{
          width: '55%', overflow: 'hidden',
          textOverflow: 'ellipsis',
          whiteSpace: 'nowrap'
        }}>为大量糖尿病患者开具过有效药剂 被称为糖尿病圣手</p>
        <p>〉</p>
      </div>
      {/* 选择医院 */}
      <div>
        <Cascader
          options={HospitalOptions}
          visible={HospitalVisible}
          onClose={() => {
            setHospitalVisible(false)
            setdoctor_department()
          }}
          onSelect={(value) => {
            setDoctor_department(value[2])
          }}
        />
      </div>
      {/* 所属职称 */}
      <div>
        <Cascader
          options={TitleOptions}
          visible={TitleVisible}
          onClose={() => {
            setTitleVisible(false);
            upDoctor_title()
          }}
          onSelect={(value) => {
            setDoctor_title(value[0])
          }}
        />
      </div>
    </div >
  )
}
