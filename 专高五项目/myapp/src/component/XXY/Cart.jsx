import React, { useState, useEffect, useMemo } from 'react'
import { useNavigate, useLocation } from 'react-router-dom'
import '@nutui/nutui-react/dist/style.css'
import './xxy.css'
import { Card } from 'antd-mobile'
import { Cart, ArrowLeft, Checklist } from '@nutui/icons-react'
import { NavBar, Button, Cell, Checkbox, Toast } from '@nutui/nutui-react'
import axios from 'axios'
export default function Cartd() {
  const navigate = useNavigate()
  const [cart, setCart] = useState([])
  const [carts, setCarts] = useState([])
  const [uid, setUid] = useState('66d458f9fcde96eddecc55cc')
  const getCart = () => {
    axios.get(`/XXY/getcart?uid=${uid}`).then(res => {
      let cartlist= res.data.data[0].goodslist
      let newlist=cartlist.map(ele => {
        ele['ischecked']=true
        return ele
      });
      setCarts(newlist)
      setCart(res.data.data)
    })
  }
  useEffect(() => {
    getCart()
  }, [])
  const [Checkbox1,setCheckbox1]= useState(true)
  const changeCheck = (index) => {
    let newlist = carts.map((ele, i) => {
      if (i === index) {
        ele.ischecked = !ele.ischecked
      }
      return ele
    })
    setCarts(newlist)
    if(newlist.filter(ele => ele.ischecked == true).length == newlist.length ){
      setCheckbox1(true)
    }else{
      setCheckbox1(false)
    }
  }
const sum = useMemo(() => {
    let sum = 0
    carts.forEach(ele => {
      if (ele.ischecked) {
        sum += ele.goods_id.discount * ele.num
      }
    })
    return sum
},[carts])
const changeAll = (state) => {
    let newlist = carts.map((ele, i) => {
      ele.ischecked = state
      return ele
    })
    setCarts(newlist)
}
  const addcart = (goods_id, num) => {
    console.log(goods_id, num);
    axios.post('/XXY/addcart', {
      goodsid: goods_id,
      num: num,
      uid:uid
    }).then(res => {
      if (res.data.code == 200) {
        getCart()
      }
    })
  }
  const delcart = (goods_id) => {
    axios.post('/XXY/delcart', {
      goodsid: goods_id,
      uid:uid
    }).then(res => {
      if (res.data.code == 200) {
        getCart()
      }
    })
  }
  const goorder = () => {
    let statelist = carts.filter(ele => ele.ischecked == true)
    // console.log(state);
    navigate('/Orderxxy',{state:statelist})

  }
  return (
    <div className='cart'>
      <div className='cart_top'>
        <NavBar
          back={
            <ArrowLeft />
          }
          right={
            <span className="flex-center" onClick={() => { }}>
              管理
            </span>
          }
          onBackClick={() => { navigate(-1) }}
        >
          购物车
        </NavBar>
      </div>
      <div className='cart_content'>
        <div className='cart_list'>
            {carts.map((item,index) => {
              return (
                <Card key={index}>
                  <div className='cart_item'>
                    <div style={{display: 'flex', alignItems: 'center'}}>
                      <Checkbox
                        value={index}
                        checked={item.ischecked}
                        onChange={()=>{
                          changeCheck(index)
                        }}
                        defaultChecked={item.ischecked}
                        icon={<img src='https://cdn7.axureshop.com/demo2024/2280786/images/%E8%B4%AD%E7%89%A9%E8%BD%A6/u1272.svg' />}
                        activeIcon={<img src='https://cdn7.axureshop.com/demo2024/2280786/images/%E8%B4%AD%E7%89%A9%E8%BD%A6/u1272_selected.svg' />}
                      >
                       
                      </Checkbox>
                    </div>
                    <div>
                      <img className='cart_img' src={item.goods_id.img} />
                    </div>
                    <div style={{width: '250px'}}>
                      <p style={{fontSize:'15px'}}>{item.goods_id.name}</p>
                      <div style={{display: 'flex', justifyContent: 'space-between'}}>
                        <div>
                          <span style={{ fontSize: '14px', marginRight: '4px',color:'red'}}>￥{item.goods_id.discount % 1 == 0 ? item.goods_id.discount + '.00' : (item.goods_id.discount % 0.1 ? item.goods_id.discount + '0' : item.goods_id.discount)}</span>
                          <span style={{ fontSize: '12px', color: '#aaa' }}><s>{item.goods_id.price % 1 == 0 ? item.goods_id.price + '.00' : (item.goods_id.price % 0.1 ? item.goods_id.price + '0' : item.goods_id.price)}</s></span>
                        </div>
                        <div className='cart_num'>
                          <button className='cart_btn' onClick={() => {
                            if (item.num == 1) {
                              delcart(item.goods_id._id)
                            }else{
                              addcart(item.goods_id._id,-1)
                            }
                          }}>-</button><span>{item.num}</span>
                          <button className='cart_btn' onClick={() => {
                            addcart(item.goods_id._id,1)
                          }}>+</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </Card>
              )
            })}
        </div>
      </div>
      <div className='cart_foot'>
        <div className='detail_foot_left'>
          <div className='foot_cart'>
            <Checkbox
              defaultChecked={Checkbox1}
              icon={<img src='https://cdn7.axureshop.com/demo2024/2280786/images/%E8%B4%AD%E7%89%A9%E8%BD%A6/u1272.svg' />}
              activeIcon={<img src='https://cdn7.axureshop.com/demo2024/2280786/images/%E8%B4%AD%E7%89%A9%E8%BD%A6/u1272_selected.svg' />}
              checked={Checkbox1}
              onChange={(state) => {
                setCheckbox1(state)
                changeAll(state)
              }}
            >
              全选
            </Checkbox>
          </div>
          <div className='foot_buy'>
            <div className='foot_hj'>
              <span>合计：</span>
              <div style={{margin:'0 10px'}}>
                <p style={{color:'red'}}>￥{sum}</p>
                <p style={{ color: '#999999'}}>不含运费</p>
              </div>
            </div>
            <Button type="primary" style={{ width: '105px' }} onClick={()=>{
              goorder()
            }}>
              去结算({carts.filter(item => item.ischecked).length})
            </Button>
          </div>
        </div>
      </div>
    </div>
  )
}
