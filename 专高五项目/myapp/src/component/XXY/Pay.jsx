import React, { useState, useEffect, useMemo, useRef } from 'react'
import { CountDown } from '@nutui/nutui-react'
import { NavBar, Button, Popup } from '@nutui/nutui-react'
import { Modal } from 'antd-mobile'
import { useNavigate, useLocation } from 'react-router-dom'
import { Card } from 'antd-mobile'
import axios from 'axios'
import './xxy.css'
import '@nutui/nutui-react/dist/style.css'
import { ArrowRight, ArrowLeft } from '@nutui/icons-react'
export default function Pay() {
  let [price, setPrice] = useState(0)
  const location = useLocation()
  const navigate = useNavigate()
  const stateRef = useRef({
    endTime: Date.now() + 60 * 1000 * 60 * 24,
  })
  const onEnd = () => {
    console.log('countdown: ended.')
  }
  const [showBottom, setShowBottom] = useState(false)
  useEffect(() => {
    setPrice(location.state)
  })
  const shou = () => {
    let nums = ''
    for (let i = 0; i < 8; i++) {
      nums += Math.floor(Math.random() * 10)
    }
    return nums
  }
  const pay = () => {
    let a = shou()
    axios.post('/XXY/api/payment', {
      price: price,
      name: '支付宝支付',
      orderId: a
    }).then(res => {
      Modal.confirm({
        content: '是否跳转到支付宝支付页面',
        onConfirm: async () => {
          await sleep(3000)
          let url=res.data.paymentUrl
          window.location.href = url
        },
      })
    })
  }
  const sleep = (time) => new Promise(resolve => setTimeout(resolve, time))
  return (
    <div className='pay'>
      <div className='pay_top'>
        <NavBar
          back={
            <ArrowLeft />
          }
          onBackClick={() => { navigate(-1) }}
        >
          安全支付
        </NavBar>
      </div>
      <div className='pay_center'>
        <Card>
          <div style={{ textAlign: 'center' }}>
            <p style={{ fontSize: '40px', margin: '17px 0' }}><b>￥{price.toFixed(2)}</b></p>
            <p style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}><span style={{ fontSize: '15px', marginRight: '10px', color: '#999' }}>剩余时间:</span><span style={{ '--nutui-countdown-color': 'rgb(110, 157, 249 )' }}><CountDown endTime={stateRef.current.endTime} onEnd={onEnd} /></span></p>
          </div>
        </Card>
        <Card onClick={() => {
          setShowBottom(true)
        }}>
          <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', padding: '10px 0' }}>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <img style={{ width: '38px', height: '33px' }} src="https://cdn7.axureshop.com/demo2024/2280786/images/%E5%BE%AE%E4%BF%A1%E6%94%AF%E4%BB%98/u1310.svg" alt="" />
              <div style={{ marginLeft: '10px' }}>
                <p style={{ fontSize: '16px', margin: '0' }}>微信支付</p>
                <p style={{ color: '#999', display: 'flex', alignItems: 'center', fontSize: '12px', margin: '0' }}><img src='https://cdn7.axureshop.com/demo2024/2280786/images/%E5%BE%AE%E4%BF%A1%E6%94%AF%E4%BB%98/u1313.svg'></img>微信安全支付</p>
              </div>
            </div>
            <div>
              <ArrowRight />
            </div>
          </div>
        </Card>
        <Card onClick={() => {
          pay()
        }}>
          <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', padding: '10px 0' }}>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <img style={{ width: '55px', height: '55px' }} src="https://t12.baidu.com/it/u=1724323204,214961249&fm=30&app=106&f=JPEG?w=640&h=640&s=80B1577EEA71568A598A60510200E0EA" alt="" />
              <div style={{ marginLeft: '10px' }}>
                <p style={{ fontSize: '16px', margin: '0' }}>支付宝支付</p>
                <p style={{ color: '#999', display: 'flex', alignItems: 'center', fontSize: '12px', margin: '0' }}><img src='https://cdn7.axureshop.com/demo2024/2280786/images/%E5%BE%AE%E4%BF%A1%E6%94%AF%E4%BB%98/u1313.svg'></img>支付宝安全支付</p>
              </div>
            </div>
            <div>
              <ArrowRight />
            </div>
          </div>
        </Card>
      </div>

      <Popup
        visible={showBottom}
        position="bottom"
        onClose={() => {
          setShowBottom(false)
          
        }}
      >
        <div className='pay-bottom'>
          <div style={{ color: 'rgb(9,187,7)', fontSize: '22px', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
            <img src="https://cdn7.axureshop.com/demo2024/2280786/images/%E7%A1%AE%E8%AE%A4%E6%94%AF%E4%BB%98/u1326.svg" alt="" />
            <span>支付成功</span>
          </div>
          <div>
            <p style={{ fontSize: '16px' }}>微信支付</p>
            <p style={{ fontSize: '43px', marginTop: '10px' }}><b><span style={{ fontSize: '28px' }}>￥</span>{price.toFixed(2)}</b></p>
          </div>
          <div>
            <div onClick={() => {
              navigate('/User_Home/Shop')
            }}>
              <Button color='rgb(26,209,179)' style={{ padding: '22px 110px', fontSize: '16px' }}>返回商城</Button>
            </div>
            <div style={{ marginTop: '19px' }}>
              <Button color='rgb(247,247,247)' style={{ color: 'balck', padding: '22px 110px', fontSize: '16px', border: '1px solid rgb(210,210,210)' }}>查看订单</Button>
            </div>
          </div>
        </div>
      </Popup>
    </div>
  )
}
