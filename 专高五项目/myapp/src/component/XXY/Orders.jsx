import React, { useState, useEffect, useMemo } from 'react'
import { NavBar, Button, Cell, Checkbox, Toast, Input } from '@nutui/nutui-react'
import { useNavigate, useLocation } from 'react-router-dom'
import { Card } from 'antd-mobile'
import axios from 'axios'
import './xxy.css'
import '@nutui/nutui-react/dist/style.css'
import { ArrowRight, ArrowLeft } from '@nutui/icons-react'
export default function Orders() {
    const location = useLocation()
    const navigate = useNavigate()
    const [cart, setCart] = useState([])
    const [user, setuser] = useState({})
    const [uid, setUid] = useState('66d458f9fcde96eddecc55cc')
    const [val, setVal] = useState('')
    const getuser = () => {
        axios.get(`/XXY/getuser?uid=${uid}`).then((res) => {
            setuser(res.data.data)
        })
    }
    const sum = useMemo(() => {
        let sum = 0
        cart.forEach(ele => {
            if (ele.ischecked) {
                sum += ele.goods_id.discount * ele.num
            }
        })
        return sum
    }, [cart])
    useEffect(() => {
        setCart(location.state)
        // console.log(location.state[0].goods_id);
        getuser()
    }, [])
    return (
        <div className='orders'>
            <div className='orders_top'>
                <NavBar
                    back={
                        <ArrowLeft />
                    }
                    onBackClick={() => { navigate(-1) }}
                >
                    提交订单
                </NavBar>
            </div>
            <div className='orders_content'>
                <Card className='jsq1'>
                    <div>
                        <p style={{ fontSize: '16px', padding: '3px 0' }}>收货地址</p>
                    </div>
                    <div>
                        <p style={{ fontSize: '16px' }}><b>{user.username}，{user.phone}</b></p>
                    </div>
                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                        <p style={{ width: '95%', fontSize: '0.94rem', color: '#777777' }}>北京市东城区东直门街道 东直门外大街48号东方银座写字楼</p>
                        <div><ArrowRight /></div>
                    </div>
                </Card>
                <Card className='jsq2'>
                    <div>
                        <p style={{ fontSize: '16px', padding: '3px 0' }}>商品清单</p>
                    </div>
                    {
                        cart.map((item, index) => {
                            return (
                                <div key={index} style={{ display: 'flex', justifyContent: 'space-between', margin: '25px 0' }}>
                                    <div style={{ display: 'flex', alignItems: 'center' }}>
                                        <img src={item.goods_id.img} alt="" style={{ width: '90px', height: '90px', marginRight: '17px' }} />
                                    </div>
                                    <div style={{ fontSize: '18px' }}><p><b>{item.goods_id.name.length > 17 ? item.goods_id.name.slice(0, 17) + '...' : item.goods_id.name}</b></p></div>
                                    <div style={{ textAlign: 'right', width: '199px' }}>
                                        <p style={{ fontSize: '17px' }}>￥{item.goods_id.discount % 1 == 0 ? item.goods_id.discount + '.00' : (item.goods_id.discount % 0.1 ? item.goods_id.discount + '0' : item.goods_id.discount)}</p>
                                        <p>x{item.num}</p>
                                    </div>
                                </div>
                            )
                        })
                    }
                    <div style={{ textAlign: 'right', margin: '25px 0', fontSize: '17px' }}>
                        <span style={{ marginRight: '10px' }}>共<span style={{ color: '#FF6700', margin: '0 5px' }}>{cart.length}</span>件商品</span>
                        <span>共计：</span><span style={{ color: '#FF6700', fontSize: '15px' }}><b>￥</b>{sum.toFixed(2)}</span>
                    </div>
                </Card>
                <Card>
                    <div style={{display:'flex',justifyContent:'space-between',alignItems:'center'}}>
                    <span style={{ fontSize: '16px' }}>买家留言</span>
                    <Input
                        value={val}
                        onChange={(val) => setVal(val)}
                        placeholder="50字以内 (选填)"
                        maxLength={50}
                    />
                    </div>
                </Card>
                <Card style={{fontSize:'17px'}}>
                    <div style={{display:'flex',justifyContent:'space-between',alignItems:'center'}}>
                        <p>商品小计</p>
                        <p>￥{sum.toFixed(2)}</p>
                    </div>
                    <hr style={{margin:'18px 0',border:'1px solid #E0E0E0'}}/>
                    <div style={{display:'flex',justifyContent:'space-between',alignItems:'center'}}>
                        <p>运费</p>
                        <p>￥0.00</p>
                    </div>
                </Card>
            </div>
            <div className='cart_foot'>
        <div className='detail_foot_left'>
          <div className='foot_cart'>
          </div>
          <div className='foot_buy'>
            <div className='foot_hj' style={{fontSize:'17px', margin:'6px 10px'}}>
              <span>应付款：</span>
                <p style={{color:'red'}}>￥{sum.toFixed(2)}</p>
            </div>
            <Button type="primary" style={{ width: '105px' }} onClick={()=>{
                navigate('/payxxy',{state:sum})
            }}>
              立即支付
            </Button>
          </div>
        </div>
      </div>
        </div>
    )
}
