import React, { useState, useEffect } from 'react'
import { Card, Toast } from 'antd-mobile'
import { Button } from '@nutui/nutui-react'
import axios from 'axios'
import { Sticky } from '@nutui/nutui-react'
import './xxy.css'
import '@nutui/nutui-react/dist/style.css'
import { useNavigate } from 'react-router-dom'
import { NavBar } from '@nutui/nutui-react'
import { Share, More, Cart, ArrowLeft, Close } from '@nutui/icons-react'
import { useLocation } from 'react-router-dom'
export default function Detail() {
  const navigate = useNavigate()
  const location = useLocation()
  const [log1, setlog] = useState([])
  const [data, setData] = useState([])
  const [carts, setCarts] = useState([])
  const [uid, setUid] = useState('66d458f9fcde96eddecc55cc')
  const getCart = () => {
    axios.get(`/XXY/getcart?uid=${uid}`).then(res => {
      let cartlist= res.data.data[0].goodslist
      setCarts(cartlist)
    })
  }
  const addcart = () => {
    axios.post('/XXY/addcart', {
      goodsid: data._id,
      num: 1,
      uid:uid
    }).then(res => {
      if (res.data.code == 200) {
        getCart()
      }
    })
  }
  useEffect(() => {
    getCart()
    setData(location.state);
    setlog(location.state.descimg)
  }, [])
  return (
    <div className='xxy_detail'>
      <div className='detail_top'>
          <NavBar
            back={
              <ArrowLeft />
            }
            right={
              <span className="flex-center" onClick={(e) => Toast.show('icon')}>
                <img src="https://cdn7.axureshop.com/demo2024/2280786/images/%E5%95%86%E5%93%81%E8%AF%A6%E6%83%85%E9%A1%B5/u1221.svg"></img>
              </span>
            }
            onBackClick={() => { navigate(-1) }}
          >
            商品详情
          </NavBar>
      </div>
      <div className='detail_img' style={{ backgroundImage: `url(${data.img})` }}>
      </div>
      <div className='detail_content'>
        <div className='detail_jsq'>
          <Card className='jsq1' style={{ backgroundColor: 'red', color: 'white' }}>
            <div>
              <span style={{ fontSize: '23px', marginRight: '13px' }}>￥{data.discount % 1 == 0 ? data.discount + '.00' : (data.discount % 0.1 ? data.discount + '0' : data.discount)}</span>
              <span style={{ fontSize: '15px', color: '#ddd' }}><s>{data.price % 1 == 0 ? data.price + '.00' : (data.price % 0.1 ? data.price + '0' : data.price)}</s></span>
            </div>
          </Card>
          <Card className='jsq2' style={{ width: '100%', color: 'black' }}>
            <p style={{ fontSize: '17px' }}><b>{data.name}</b></p>
            <div style={{ display: 'flex', justifyContent: 'space-between', marginTop: '10px' }}>
              <span style={{ fontSize: '13px', color: '#999' }}>已售{data.soldnum}件</span>
              <span style={{ fontSize: '13px', color: '#999' }}><img style={{margin:'0 3px'}} src='https://cdn7.axureshop.com/demo2024/2280786/images/%E6%96%87%E7%AB%A0%E8%AF%A6%E6%83%85%E9%A1%B5/u392.svg'/>收藏</span>
            </div>
          </Card>
        </div>
        <Card className='detail_parameter'>
          <div className='detail_parameter_title'>
          <p><span style={{ fontSize: '14px', color: '#999' ,marginRight:'18px'}}>参数</span> {data.parameter}...</p>
          <img style={{width: '6px', height: '10px'}} src='https://cdn7.axureshop.com/demo2024/2280786/images/%E5%95%86%E5%93%81%E8%AF%A6%E6%83%85%E9%A1%B5/u1232.svg'/>
          </div>
        </Card>
        <Card>
          <p style={{ fontSize: '16px', marginBottom: '30px' }}><b>详情</b></p>
          <p style={{ fontSize: '14px', marginBottom: '7px' }}>&nbsp;&nbsp;&nbsp;&nbsp;{data.desctitle}</p>
          <span style={{ fontSize: '10.4px' }}>{data.desc}</span>
          <div>
            {log1.map((item, index) => {
              return (<div key={index}>
                <img src={item} style={{ width: '100%', marginBottom: '6px' }}></img>
              </div>)
            })}
          </div>
        </Card>
      </div>
      <div className='detail_foot'>
        <div className='detail_foot_left'>
          <div className='foot_cart' onClick={()=>{ navigate('/Cartxxy')}}>
          <span className='cartCount'>{carts.length}</span>
            <img src='https://cdn7.axureshop.com/demo2024/2280786/images/%E5%95%86%E5%9F%8E%E4%B8%BB%E9%A1%B5/u1127.svg'/>
            <p>购物车</p>
          </div>
          <div className='foot_addcard'>
          <Button type="warning" style={{width:'145px'}} onClick={()=>{
            addcart()
          }}>
            加入购物车
          </Button>
          </div>
          <div className='foot_buy'>
          <Button type="primary" style={{width:'145px'}} onClick={
            ()=>{
              navigate('/Orderxxy',{state:[{
                goods_id:data,num:1,ischecked:true
              }]})
              // console.log([{good_id:data,num:1}]);
            }
          }>
            立即购买
          </Button>
          </div>
        </div>
      </div>
    </div>
  )
}
