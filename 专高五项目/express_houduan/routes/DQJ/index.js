var express = require('express');
var router = express.Router();
// 对暴露的表进行导入，在{}写入要导入的需要导入的表名，然后在require中写上module.js的路径
var { userModel,
  doctorModel,
  chatModel,
  Commonly_used_medicinesModel,
  Commonly_prescribed_prescriptionModel,
  List_of_historical_prescriptionModel,
  titleModel,
  chatModel,
} = require('../../module/module')
// 使用multiparty对获取前端上传的图片或者文件
var multiparty = require('multiparty')
// 导入node-xlsx对上传过来的文件中的内容进行读取
var xlsx = require('node-xlsx')
// 使用jsonwebtoken创建生成token
var jwt = require("jsonwebtoken")



/* 获取当前登录的医生信息 */
router.get('/getdoctor', async (req, res) => {
  let _id = req.query._id
  let data = await doctorModel.find({ _id: _id })
  res.send({
    code: 200,
    data: data,
  })
})

// 获取当登录医生聊天的信息
router.post('/getchat', async (req, res) => {
  let item = req.body
  let data = await userModel.find({ _id: { $in: item } })
  res.send({
    code: 200,
    data: data,
  })
})

//获取当前登录医生的咨询信息
router.get('/getzixun', async (req, res) => {
  let _id = req.query._id
  let userid = []
  let item = await chatModel.find({ doctor_id: _id })
  item.forEach((item) => {
    userid.push(item.user_id)
  })
  let data = await userModel.find({ _id: { $in: userid } }).lean()
  for (let i in data) {
    for (let j in item) {
      if (data[i]._id == item[j].user_id) {
        data[i].istype = item[j].istype
      }
    }
  }
  console.log(data);
  res.send({
    data
  })
})

//获取当前登录医生的常用药品
router.get('/Commonly_used_medicines', async (req, res) => {
  let data = await Commonly_used_medicinesModel.find()
  res.send({
    data
  })
})

//获取当前登录医生的常用处方
router.get('/Commonly_prescribed_prescription', async (req, res) => {
  let _id = req.query._id
  let data = await Commonly_prescribed_prescriptionModel.find({ doctor_id: _id })
  res.send({
    data
  })
})

// 获取当前登录医生的历史处方
router.get('/List_of_historical_prescription', async (req, res) => {
  let _id = req.query._id
  let data = await List_of_historical_prescriptionModel.find({ doctor_id: _id })
  res.send({
    data
  })
})


// 添加文章
router.post('/post_content', async (req, res) => {
  let data = req.body
  await titleModel.create(data)
  res.send({
    code: 200,
    msg: '添加成功'
  })
})

// 上传图片
router.post('/post_the_video', (req, res) => {
  let form = new multiparty.Form()
  form.uploadDir = 'upload'
  form.parse(req, (error, formdata, imgData) => {
    //let path=fileData.file[0].path
    //let [{data}]=xlsx.parse(path)
    //data.shift()
    res.send({
      data: 'http://127.0.0.1:3000/' + imgData.file[0].path
    })
  })
})



// 获取全部文章
router.get('/getcontent', async (req, res) => {
  let data = await titleModel.find()
  res.send({
    data
  })
})

// 获取当前登录医生所写的文章
router.get('/getcontentById', async (req, res) => {
  let _id = req.query._id
  let data_filter = await titleModel.find({ doctor_id: _id })
  let data = []
  data_filter.forEach((item, index) => {
    if (item.video) {
      console.log();
    } else {
      data.push(item)
    }
  })
  res.send({
    data
  })
})


// 获取视频
router.get('/getvideo', async (req, res) => {
  let _id = req.query._id
  let data_filter = await titleModel.find({ doctor_id: _id })
  let data = []
  data_filter.forEach((item, index) => {
    if (item.video) {
      data.push(item)
    } else {
      console.log();
    }
  })
  res.send({
    data
  })
})




// 结束聊天修改状态
router.post('/postcallstype', async (req, res) => {
  let { user_id, doctor_id } = req.body
  await chatModel.updateMany({ user_id: user_id, doctor_id: doctor_id },{istype:false})
  res.send({
    msg: '成功',
    code: 200
  })
})


// 获取当前咨询病人得详情
router.get('/getchatPatient', async (req, res) => {
  let _id = req.query
  let data = await userModel.find({ _id: _id._id })
  let doctor = await doctorModel.find({ _id: _id.doctor_id })
  res.send({
    data,
    doctor
  })
})


// 存储当前病人聊天信息
router.post('/postchat', async (req, res) => {
  let data = req.body
  await chatModel.create(data)
  res.send({
    msg: '成功',
    code: 200
  })
})

// 获取当前和病人聊天信息
router.get('/getchattalk', async (req, res) => {
  let { user_id, doctor_id } = req.query

  let data_sort = await chatModel.find({ user_id, doctor_id })
  let data = data_sort.sort((a, b) => { a.datetime - b.datetime })
  res.send({
    data
  })
})






module.exports = router;
