var express = require('express');
var router = express.Router();
// 对暴露的表进行导入，在{}写入要导入的需要导入的表名，然后在require中写上module.js的路径
var { userModel, doctorModel ,jkzModel,bfzModel,hotModel,videoModel} = require('../../module/module')
// 使用multiparty对获取前端上传的图片或者文件
var multiparty = require('multiparty')
// 导入node-xlsx对上传过来的文件中的内容进行读取
var xlsx = require('node-xlsx')
// 使用jsonwebtoken创建生成token
var jwt = require("jsonwebtoken")




//获取验证码
router.get('/send_code', (req, res) => {
  let tel_number = req.query.phone;
  console.log(tel_number);
  global.require = require;
  // 调用sdk
  const SMSClient = require('../../node_modules/@alicloud/sms-sdk');
  // let smsCode = Math.random().toString().slice(-6);
  // 设置值
  let accessKeyId = "LTAI5tH2rHVneV1SaSKN9Qou";// AccessKey ID
  let secretAccessKey = "C3Lgb6BTd7Pq7Tz8AqhdD6X8GsCJWv";// AccessKey Secret
  let signName = "振涛教育平台"; // 签名名称
  let templateCode = "SMS_468955440";// 短信模板code
  // let tel_number = '17631250767' ;//手机号

  // 初始化sms_client
  const smsClient = new SMSClient({ accessKeyId, secretAccessKey })

  let phoneNum = tel_number;//手机号
  // 生成六位随机验证码
  let smsCode = Math.random().toString().slice(-6);
  console.log("smsCode:", smsCode);

  // 开始发送短信
  smsClient.sendSMS({
    PhoneNumbers: phoneNum,
    SignName: signName, //签名名称 前面提到要准备的
    TemplateCode: templateCode, //模版CODE  前面提到要准备的
    // TemplateParam: `{"code":'${str}'}`, // 短信模板变量对应的实际值，JSON格式
    TemplateParam: `{"code":'${smsCode}'}`, // 短信模板变量对应的实际值，JSON格式
  }).then(result => {
    let { Code } = result;
    if (Code == 'OK') {
      res.json({
        code: 200,
        msg: 'success',
        sms: smsCode
      })
    }
  }).catch(err => {
    console.log("报错：", err);
    res.json({
      code: 1,
      msg: 'fail: ' + err.data.Message
    })
  })
})

//用户注册
router.post('/adduser', (req, res) => {
  userModel.create(req.body)
  res.send({
    code: 200,
    msg: '添加用户成功'
  })
})

//验证用户登录信息
router.post('/login', async (req, res) => {
  let { username, password } = req.body
  let data = await userModel.findOne({ username, password })//查询用户
  let doctor = await doctorModel.findOne({ username, password }) //查询医生
  let token = jwt.sign({ username }, '123456', { expiresIn: '1h' })
  if (data) {
    res.send({
      code: 200,
      msg: '登录成功',
      data,
      token
    })
  } else if (doctor) {
    res.send({
      code: 200,
      msg: '登录成功',
      data: doctor,
      token
    })
  }
  else {
    res.send({
      code: 400,
      msg: '登录失败'
    })
  }
})


//健康知识数据添加
router.post('/addjkz',(req,res)=>{
  jkzModel.create(req.body)
  res.send({
    code:200,
    msg:'添加成功'
  })
})

//健康知识获取
router.get('/getjkz',async(req,res)=>{
  let data = await jkzModel.find()
  res.send({
    code:200,
    msg:'获取成功',
    data
  })
})
//并发症数据添加
router.post('/addbfz',(req,res)=>{
  bfzModel.create(req.body)
  res.send({
    code:200,
    msg:'添加成功'
  })
})
//并发症数据获取
router.get('/getbfz',async(req,res)=>{
  
  let data = await bfzModel.find()
  res.send({
    code:200,
    msg:'获取成功',
    data
  })
})

//热门科普
router.post('/addhot',(req,res)=>{
  hotModel.create(req.body)
  res.send({
    code:200,
    msg:'添加成功'
  })
})
//获取热点
router.get('/gethot',async(req,res)=>{
  let data = await hotModel.find()
  res.send({
    code:200,
    msg:'获取成功',
    data
  })
})

//热门视频添加
router.post('/addvideo',(req,res)=>{
  videoModel.create(req.body)
  res.send({
    code:200,
    msg:'添加成功'
  })
})

//获取视频
router.get('/getvideo',async(req,res)=>{
  let data = await videoModel.find()
  res.send({
    code:200,
    msg:'获取成功',
    data
  })
})


module.exports = router;