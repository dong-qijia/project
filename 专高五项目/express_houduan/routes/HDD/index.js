var express = require('express');
var router = express.Router();
// 对暴露的表进行导入，在{}写入要导入的需要导入的表名，然后在require中写上module.js的路径
var {userModel,doctorModel,titleModel}=require('../../module/module')
// 使用multiparty对获取前端上传的图片或者文件
var multiparty=require('multiparty')
// 导入node-xlsx对上传过来的文件中的内容进行读取
var xlsx=require('node-xlsx')
// 使用jsonwebtoken创建生成token
var jwt=require("jsonwebtoken")



/* GET home page. */
router.get('/',async(req,res)=>{
  // let data =await titleModel.create(req.body)
  res.send({
    code:200,
    // data
  })
})
router.post('/addT',async(req,res)=>{
  let data =await titleModel.create(req.body)
  res.send({
    code:200,
    data
  })
})
router.post('/upload',(req,res)=>{
  console.log(111);
  
  let form=new multiparty.Form() 
  
  form.uploadDir='upload'
  form.parse(req,(error,formdata,imgData)=>{
  //let path=fileData.file[0].path
  //let [{data}]=xlsx.parse(path)
  //data.shift()
  res.send({
    data:'http://localhost:3000/'+imgData.file[0].path
   })
  })
  
})
router.post('/listT',async(req,res)=>{
  console.log(123321);
  
  let list = await titleModel.find().populate('uid')
  res.send({
    code:200,
    list
  })
})

router.post('/listU',async(req,res)=>{
  
  let list = await userModel.find()
  res.send({
    code:200,
    list
  })
})
router.post('/DocList',async(req,res)=>{
  
  let list = await doctorModel.find()
  res.send({
    code:200,
    list
  })
})
router.post('/item',async(req,res)=>{
  console.log(req.body.tid);
  
  let list = await titleModel.find({_id:req.body.tid})
  let item = list[0]
  res.send({
    code:200,
    item
  })
})

module.exports = router;
