var express = require('express');
var router = express.Router();
// 对暴露的表进行导入，在{}写入要导入的需要导入的表名，然后在require中写上module.js的路径
var {goodstypeModel,goodsModel,cartModel,orderModel,userModel}=require('../../module/module')
// 使用multiparty对获取前端上传的图片或者文件
var multiparty=require('multiparty')
// 导入node-xlsx对上传过来的文件中的内容进行读取
var xlsx=require('node-xlsx')
// 使用jsonwebtoken创建生成token
var jwt=require("jsonwebtoken")
const alipaySdk =require('../../alipay.js')
const {AlipayFormData} = require('alipay-sdk')
router.get('/goods',async(req,res)=>{
  let typeid = req.query.type
  let val = req.query.val
  let query = {}; 
  if(typeid){
    query.type = typeid;  
  }else{
    query.isjx = 1;
  }
  if(val){
    console.log(val);
    query.name = { $regex: new RegExp(val) };
  }
  console.log(query);
  let goodsList = await goodsModel.find(query); 

  res.send({
    code:200,
    msg:'获取商品列表成功',
    data:goodsList
  })
})
router.get('/goodstype',async(req,res)=>{
  let goodstypeList= await goodstypeModel.find()
  res.send({
    code:200,
    msg:'获取商品类型列表成功',
    data:goodstypeList
  })
})

router.get('/getcart',async(req,res)=>{
  let uid = req.query.uid
  if(uid){
    let cartList= await cartModel.find({user_id:uid}).populate({path:'goodslist.goods_id',model:'goods'})
    res.send({
      code:200,
      msg:'获取购物车列表成功',
      data:cartList
    })
  }else{
    res.send({
      code:400,
      msg:'获取购物车列表失败'
    })
  }
  
})
router.post('/addcart',async(req,res)=>{
  let {goodsid,uid,num}=req.body
  console.log(goodsid,uid,num);
  let cartList= await cartModel.findOne({user_id:uid})
  let itemIndex = cartList.goodslist.findIndex(item => item.goods_id == goodsid)
  console.log(itemIndex);
  if(itemIndex==-1){
    cartList.goodslist.push({goods_id:goodsid,num:num})
  }else{
    cartList.goodslist[itemIndex].num+=num
  }
  await cartList.save()
  res.send({
    code:200,
    msg:'添加购物车成功',
  })
})
router.post('/delcart',async(req,res)=>{
  let {goodsid,uid}=req.body
  let cartList= await cartModel.findOne({user_id:uid})
  let itemIndex = cartList.goodslist.findIndex(item => item.goods_id == goodsid)
  cartList.goodslist.splice(itemIndex,1)
  await cartList.save()
  res.send({
    code:200,
    msg:'删除购物车成功',
  })
})
router.get('/getuser',async(req,res)=>{
  let uid = req.query.uid
  console.log(uid);
  if(uid){
    let user= await userModel.findOne({_id:uid})
    res.send({
      code:200,
      msg:'获取用户信息成功',
      data:user
    })
  }else{
    res.send({
      code:400,
      msg:'获取用户信息失败'
    })
  }
}
)
router.post('/api/payment',function(req, res, next){
let orderId = req.body.orderId;
let price = req.body.price;
let name = req.body.name;
console.log(orderId,price,name);
const bizContent = {
  out_trade_no: orderId,
  product_code: "FAST_INSTANT_TRADE_PAY",
  subject: name,
  total_amount: price,
  
}
const url = alipaySdk.pageExec('alipay.trade.page.pay', 'GET', {
  bizContent,returnUrl:'http://localhost:5173/payxxy'
})

res.send({
  code:200,
  msg:'获取支付链接成功',
  paymentUrl:url
})
})




module.exports = router;
