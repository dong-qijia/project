var express = require('express');
var router = express.Router();
// 对暴露的表进行导入，在{}写入要导入的需要导入的表名，然后在require中写上module.js的路径
var {userModel,userinfoModel,havmedModel,BloodModel,titleModel,goodsModel}=require('../../module/module')
// 使用multiparty对获取前端上传的图片或者文件
var multiparty=require('multiparty')
// 导入node-xlsx对上传过来的文件中的内容进行读取
var xlsx=require('node-xlsx')
// 使用jsonwebtoken创建生成token
var jwt=require("jsonwebtoken")



/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/addhavmed',async(req,res)=>{
  await havmedModel.create(req.query)
  res.send({
    code:200,
    res:'ok',
  })
})

router.get('/havmedlist',async(req,res)=>{
  let data = await havmedModel.find()
    res.send({
      code:200,
      res:'ok',
      list:data
    })  
})

router.get('/delhavmed',async(req,res)=>{
  let id = req.query.id
  await havmedModel.deleteOne({_id:id})
    res.send({
      code:200,
      res:'ok',
    })  
})

router.get('/getuser',async(req,res)=>{
  let id = req.query.id 
  let data = await userModel.find({_id:id})
    res.send({
      code:200,
      res:'ok',
      data:data
    })  
})

router.get('/addblood',async(req,res)=>{
  await BloodModel.create(req.query)
  res.send({
    code:200,
    res:'ok',
  })
})

router.get('/getblood',async(req,res)=>{
  let {years,mounth} = req.query 
  let arr = [] 
  if(years !== 'undefined'){
    arr.push({years:years})
  }
  if(mounth !== 'undefined'){
    arr.push({mounth:mounth})
  }
  let tiao = { $and: arr }
  console.log(arr.length);
  
  if(arr.length !== 0){
    let data = await BloodModel.find(tiao)
    res.send({
      code:200,
      res:'ok',
      data:data
    })  
  }
  if(arr.length == 0 ){
    let data = await BloodModel.find()
    res.send({
      code:201,
      res:'ok',
      data:data
    })
  }
})

router.get('/getuser',async(req,res)=>{
  let data = await BloodModel.find()
    res.send({
      code:200,
      res:'ok',
      data:data
    })  
})

router.get('/gettitle',async(req,res)=>{
  let uids = req.query.uid 
  let data = await titleModel.find({uid:uids})
    res.send({
      code:200,
      res:'ok',
      data:data
    })  
})

router.get('/getgoods',async(req,res)=>{
  let data = await goodsModel.find()
    res.send({
      code:200,
      res:'ok',
      data:data
    })  
})

router.get('/chengegoods',async(req,res)=>{
  let {id,types} = req.query
  await goodsModel.updateOne({_id:id},{types:types}) 
    res.send({
      code:200,
      res:'ok',
    })  
})



module.exports = router;
