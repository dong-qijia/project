// 导入暴露的文件db
let mongoose = require('./db')
// 设置数据库中表的结构，有哪些字段，每个字段的数据类型是什么
// 如果不需要使用多表记得删，字段或者表名不想用，改一下


//表结构创建-----------------------------------------------记得字段名打备注
//用户表
let userSchema = new mongoose.Schema({
    username: String,      //用户名
    password: String,  //密码
    phone:String,  //手机号
    identity:{
        type:Boolean,
        default:false
    },  //身份验证  true代表医生 false代表用户
    avatar:String,      //头像
    nickname:String,    //昵称
    gender:String,      //性别
    age:Number,         //年龄
    height:String,      //身高
    weight:String,      //体重
    allergy:String,     //过敏史
    disease:String,     //既往病史
    family:String,      //家族病史
})

//帖子表 
let titleSchema = new mongoose.Schema({
    title: String,      // 文章名
    content: String,  // 内容
    img:String,       //图片
    time:String,      //日期
    comment:Number,   //评论
    video:String, //视频
    uid: {             //uid是外键
        type: mongoose.Types.ObjectId,
        ref: "user",
    },
    doctor_id:{
        type: mongoose.Types.ObjectId,
        ref: "doctor"
    }
})
// 医生表
let doctorSchema = new mongoose.Schema({
    username: String,      //用户名
    phone: String,      //手机号
    password: String,  //密码
    identity:{
        type:Boolean,
        default:true
    },  //身份验证  true代表医生 false代表用户
    doctor_name:String,  //医生姓名
    doctor_title:String,  //医生职称
    doctor_department:String,  //医生所属医院
    Areas_of_expertise:String,  //医生擅长领域
    Biography:String,  //医生简介
    Professional_Experience:Boolean,//是否接诊 //true代表接诊 false代表空闲
    avatar:String,  //头像
    identity_card_true:String,  //身份证正面
    identity_card_false:String,  //身份证反面
    Qualifications:String,  //医生资质  
    Ask_for_a_fee:String,   //问诊价格
    Chat_lists:Array,  //聊天列表 
    bank:Boolean,  //是否开通银行卡
    price:Number,  //医生问诊价格
    authentication:{
        type:Boolean,
        default:true
    },  //是否认证
})


// 医生常用药品表
let Commonly_used_medicinesSchecma = new mongoose.Schema({
    doctor_id: {             //uid是外键
        type: mongoose.Types.ObjectId,
        ref: "doctor",
    },
    price: String,  //药品价格
    name: String,  //药品名称
    manufacturer: String,  //药品厂家
    specification: String,  //药品规格
    quantity: String,  //药品数量
    unit: String,  //药品单位
    usage: String,  //药品用法
    storage: String,  //药品储存
    indications: String,  //药品适应症
    Adverse_effects:String,  //药品不良反应
    precautions: String,  //药品注意事项
    shelf_life: String,  //药品保质期
    production_date: String,  //药品生产日期
    expiration_date: String,  //药品过期日期
    approval_number: String,  //药品批准文号
    registration_number: String,  //药品注册证号
    image: String,  //药品图片
    taboo: String,  //药品禁忌
})

// 医生常用处方表
let Commonly_prescribed_prescriptionSchema = new mongoose.Schema({
    doctor_id: String,
    diagnosis:String,  //诊断
    name:String,  //药品名称
    usage:String,  //药品用法
    suoshuyiyuan:String,  //所属医院
})


// 历史处方表
let List_of_historical_prescriptionSchema = new mongoose.Schema({
    doctor_id: String, //医生id
    username:String,  //患者姓名
    userage:Number,  //患者年龄
    usersex:String,  //患者性别
    diagnosis:String,  //诊断
    name:String,  //药品名称
    usage:String,  //药品用法
    suoshuyiyuan:String,  //所属医院
})




// 聊天表
let chatSchema = new mongoose.Schema({
    doctor_id: String, //医生id
    user_id: String, //用户id
    istype:{
        type:Boolean,
        default:true
    } , //咨询状态 true咨询中，false已结束
    content:String,  //聊天内容
    datetime:String,  //聊天时间
    msg_type:Boolean   //谁发送的消息 true医生，false用户
})



//添加的银行卡
let bankSchema = new mongoose.Schema({
    doctor_id: String, //医生id
    bankname: String,  //银行名称
    bankip: String,  //银行卡号
    name: String,  //持卡人姓名
    nameip: String,  //开户行地址
    ip: String,  //身份证号
    
})

// 用药提醒表
let havmedSchema = new mongoose.Schema({
    name: String,      //药品名称
    once: String,  //使用次数和时间
})

// 血糖列表
let BloodSchema = new mongoose.Schema({
    years: String,    //测量时间
    mounth: String,    //测量时间
    day: String,    //测量时间
    times: String,    //测量时间
    nums: String,  //血糖值
})



//测试用
let userinfoSchema = new mongoose.Schema({
    name: String
})

//商品表
let goodsSchema = new mongoose.Schema({
    name: String,      //商品名
    price: Number,   //价格
    discount: Number,   //折扣价
    img: String,       //图片
    soldnum: Number,    //已售
    parameter: String,      //参数
    desctitle: String,        //标题
    desc: String,        //详情
    descimg: [String],        //详情图片
    type: { type: mongoose.Schema.Types.ObjectId, ref: 'goodstype'},        //类型id
    isjx: Number,        //是否精选
    types:{             //订单类型
        type:String,
        default:'0'
    },      
    dan:String,         //单号
})
//商品类型表
let goodstypeSchema = new mongoose.Schema({
    title: String,      //类型名
    src: String      //图片
})
//购物车表
let cartSchema = new mongoose.Schema({
    user_id: { type: mongoose.Schema.Types.ObjectId, ref: 'user'}, //用户id
    goodslist:[{       //商品列表
        goods_id: { type: mongoose.Schema.Types.ObjectId, ref: 'goods'}, //商品id
        num: Number     //数量
    }] 
})

//健康知识列表
let jkzSchma=new mongoose.Schema({
    title:String,  //标题
    num:Number,  //浏览量
    img:String,  //图片
    content:String,  //内容
})

//并发症列表
let bfzSchema=new mongoose.Schema({
    title:String,  //标题
    content:String,  //内容,
    img:String,
    type:{
        type:Number,
        default:0
    },
    author:String,  //作者
    num:Number, //阅读量
})

let hotSchema=new mongoose.Schema({
    title:String,  //标题
    content:String,  //内容,
    img:String,
    author:String,  //作者
    num:Number, //阅读量
})

let videoSchema=new mongoose.Schema({
    title:String,  //标题
    src:String
})





//表创建----------------------------------------------------------
let userModel = mongoose.model('user', userSchema, 'user')//用户表
let titleModel = mongoose.model('title', titleSchema, 'title')
let userinfoModel = mongoose.model('userinfo', userinfoSchema, 'userinfo')
let goodsModel = mongoose.model('goods', goodsSchema, 'goods')
let havmedModel = mongoose.model('havmed', havmedSchema, 'havmed')
let BloodModel = mongoose.model('Blood', BloodSchema, 'Blood')
let goodstypeModel = mongoose.model('goodstype', goodstypeSchema, 'goodstype')
let doctorModel = mongoose.model('doctor', doctorSchema, 'doctor')
let Commonly_used_medicinesModel = mongoose.model('Commonly_used_medicines',Commonly_used_medicinesSchecma, 'Commonly_used_medicines')//常用药品
let Commonly_prescribed_prescriptionModel = mongoose.model('Commonly_prescribed_prescription', Commonly_prescribed_prescriptionSchema, 'Commonly_prescribed_prescription')//常用处方
let List_of_historical_prescriptionModel = mongoose.model('List_of_historical_prescription', List_of_historical_prescriptionSchema, 'List_of_historical_prescription')//历史处方
let chatModel = mongoose.model('chat', chatSchema, 'chat')
let bankMdoel = mongoose.model('bank', bankSchema, 'bank')
let cartModel = mongoose.model('cart', cartSchema, 'cart')
let jkzModel = mongoose.model('jkz', jkzSchma,'jkz')//健康知识
let bfzModel = mongoose.model('bfz', bfzSchema,'bfz') //并发症
let hotModel = mongoose.model('hot', hotSchema,'hot') //热点
let videoModel = mongoose.model('video', videoSchema,'video') //视频



module.exports = {
    userModel,
    userinfoModel,
    titleModel,
    goodsModel,
    havmedModel,
    goodstypeModel,
    havmedModel,
    doctorModel,
    Commonly_used_medicinesModel,
    Commonly_prescribed_prescriptionModel,
    List_of_historical_prescriptionModel,
    chatModel,
    BloodModel,
    cartModel,
    bankMdoel,
    jkzModel,
    bfzModel,
    hotModel,
    videoModel
}
